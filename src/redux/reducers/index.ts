import { combineReducers } from "redux";
import authReducer from "./authReducer";
import filterReducer from "./filterReducer";
import offerReducer from "./offerReducer";

export default combineReducers({
    auth: authReducer,
    filter: filterReducer,
    offers: offerReducer,
})