import {
    AuthActionTypes,
    FETCH_MY_OFFERS_FAIL,
    FETCH_MY_OFFERS_START, FETCH_MY_OFFERS_SUCCESS,
    SET_CURRENT_USER,
    SET_USER_LOADING
} from "../actions/types";
import {AuthStateInterface} from "../../interfaces/auth.interface";

const isEmpty = require("is-empty");

const initialAuthState: AuthStateInterface = {
    isAuthenticated: false,
    user: undefined,
    offers: [],
    loading: false,
}

export default function(state = initialAuthState, action: AuthActionTypes): AuthStateInterface {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload
            };
        case SET_USER_LOADING:
            return {
                ...state,
                loading: true
            };
        case FETCH_MY_OFFERS_START:
            return {
                ...state,
                loading: true
            }
        case FETCH_MY_OFFERS_SUCCESS:
            return {
                ...state,
                loading: false,
                offers: action.offers
            }
        case FETCH_MY_OFFERS_FAIL:
            return {
                ...state,
                loading: false,
            }
        default:
            return state;
    }
}