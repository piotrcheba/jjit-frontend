import {
    SELECT_FILTER,
    CLEAR_FILTER, FilterActionTypes,
} from "../actions/types";
import {updateObject} from '../../utils/updateObject';
import {defaultFilters} from "../../components/Filterbar/Filterbar.constants";
import {FilterStateInterface} from "../../interfaces/filter.interface";

const initialState: FilterStateInterface = {
    location: defaultFilters.location,
    tech: defaultFilters.tech,
    salary: defaultFilters.salary,
    seniority: defaultFilters.seniority,
};

const clearFilter = (state: FilterStateInterface, action: any): FilterStateInterface => {
    let updatedFilters: FilterStateInterface = {
        ...state
    };
    action.filters.forEach((arg: keyof FilterStateInterface) => {
        updatedFilters = updateObject(updatedFilters, {[arg]: defaultFilters[arg as keyof typeof defaultFilters]});
    })
    return updatedFilters;
}

const reducer = (state = initialState, action: FilterActionTypes): FilterStateInterface => {
    switch (action.type) {
        case SELECT_FILTER:
            return {
                ...state,
                [action.filter]: action.value
            }
        case CLEAR_FILTER: return clearFilter(state, action);
        default: return state;
    }
}

export default reducer;