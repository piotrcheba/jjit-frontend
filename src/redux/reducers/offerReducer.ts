import {
    INIT_OFFER,
    ADD_OFFER_START,
    ADD_OFFER_SUCCESS,
    ADD_OFFER_FAIL,
    FETCH_OFFERS_START,
    FETCH_OFFERS_SUCCESS,
    FETCH_OFFERS_FAIL,
    APPLY_OFFER_START,
    APPLY_OFFER_FAIL,
    APPLY_OFFER_SUCCESS,
    FETCH_SINGLE_OFFER_START,
    FETCH_SINGLE_OFFER_SUCCESS,
    FETCH_SINGLE_OFFER_FAIL,
    SORT_BY_LOWEST_SALARY,
    SORT_BY_HIGHEST_SALARY,
    SORT_BY_LATEST, OfferActionTypes
} from "../actions/types";
import {OfferStateInterface} from "../../interfaces/offer.interface";

const initialState: OfferStateInterface = {
    fetchLoading: false,
    loading: false,
    offers: [],
    activeOffer: undefined,
    error: {}
};

export default function(state = initialState, action: OfferActionTypes): OfferStateInterface {
    switch (action.type) {
        case INIT_OFFER:
            return {
                ...state,
                activeOffer: action.payload
            };
        case ADD_OFFER_START:
            return {
                ...state,
                loading: true,
            }
        case ADD_OFFER_SUCCESS:
            return {
                ...state,
                loading: false,
                activeOffer: undefined
            }
        case ADD_OFFER_FAIL:
            return {
                ...state,
                loading: false,
            }
        case APPLY_OFFER_START:
            return {
                ...state,
                loading: true
            }
        case APPLY_OFFER_SUCCESS:
            return {
                ...state,
                loading: false,
            }
        case APPLY_OFFER_FAIL:
            return {
                ...state,
                loading: false,
            }
        case FETCH_OFFERS_START:
            return {
                ...state,
                loading: true
            }
        case FETCH_OFFERS_SUCCESS:
            return {
                ...state,
                loading: false,
                offers: action.offers
            }
        case FETCH_OFFERS_FAIL:
            return {
                ...state,
                loading: false
            }
        case FETCH_SINGLE_OFFER_START:
            return {
                ...state,
                fetchLoading: true
            }
        case FETCH_SINGLE_OFFER_SUCCESS:
            return {
                ...state,
                activeOffer: action.offer,
                fetchLoading: false
            }
        case FETCH_SINGLE_OFFER_FAIL:
            return {
                ...state,
                error: action.error,
                fetchLoading: false
            }
        case SORT_BY_LOWEST_SALARY:
            return {
                ...state,
                offers: state.offers.sort((a: any, b: any) => {
                    return a.salaryTo - b.salaryTo
                })
            }
        case SORT_BY_HIGHEST_SALARY:
            return {
                ...state,
                offers: state.offers.sort((a: any, b: any) => {
                    return b.salaryTo - a.salaryTo
                })
            }
        case SORT_BY_LATEST:
            return {
                ...state,
                offers: state.offers.sort((a: any, b: any) => {
                    return Date.parse(b.date) - Date.parse(a.date);
                })
            }
        default:
            return state;
    }
}