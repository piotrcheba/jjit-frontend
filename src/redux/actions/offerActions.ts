import axios from 'axios';
import {
    ADD_OFFER_FAIL,
    ADD_OFFER_START,
    ADD_OFFER_SUCCESS,
    APPLY_OFFER_START,
    APPLY_OFFER_SUCCESS,
    APPLY_OFFER_FAIL,
    FETCH_OFFERS_FAIL,
    FETCH_OFFERS_START,
    FETCH_OFFERS_SUCCESS,
    INIT_OFFER,
    FETCH_SINGLE_OFFER_START,
    FETCH_SINGLE_OFFER_SUCCESS,
    FETCH_SINGLE_OFFER_FAIL, SORT_BY_LOWEST_SALARY, SORT_BY_HIGHEST_SALARY, SORT_BY_LATEST, OfferActionTypes
} from "./types";
import {FetchFilterInterface} from "../../interfaces/filter.interface";
import {apiConfig} from "../../config/api";
import {ApplicationInterface, FetchSingleOfferParams, OfferInterface} from "../../interfaces/offer.interface";
import {ThunkAction} from "redux-thunk";
import {AppDispatch, RootState} from "../store";
import {Action} from "redux";

const CLOUDINARY_UPLOAD_PRESET = 'jjit_companylogo';

// Init offer
export const initOffer = (offerData: OfferInterface): OfferActionTypes => {
    return {
        type: INIT_OFFER,
        payload: offerData
    }
}

export const addOfferStart = (): OfferActionTypes => {
    return {
        type: ADD_OFFER_START
    }
}

export const addOfferSuccess = (): OfferActionTypes => {
    return {
        type: ADD_OFFER_SUCCESS,
    }
}

export const addOfferFail = (error: any): OfferActionTypes => {
    return {
        type: ADD_OFFER_FAIL,
        error: error
    }
}

// Add offer
export const addOffer = (offerData: OfferInterface): ThunkAction<void, RootState, unknown, Action> => async (dispatch: AppDispatch) => {
    dispatch(addOfferStart());

    const formData = new FormData();
    formData.append('file', offerData.companyLogo);
    formData.append('upload_preset', CLOUDINARY_UPLOAD_PRESET);
    delete axios.defaults.headers.common["Authorization"]
    try {
        const res = await axios.post(apiConfig.CLOUDINARY_API_URL, formData);
        const uploadedImg = res.data.secure_url;
        const updatedForm = {
            ...offerData,
            companyLogo: uploadedImg
        };
        const dbRes = await axios.post('https://mern-jjit.herokuapp.com/api/offers/add', updatedForm);
        dispatch(addOfferSuccess())
    } catch (err) {
        dispatch(addOfferFail(err.message))
    }
}

export const applyOfferStart = (): OfferActionTypes => {
    return {
        type: APPLY_OFFER_START
    }
}

export const applyOfferSuccess = (): OfferActionTypes => {
    return {
        type: APPLY_OFFER_SUCCESS,
    }
}

export const applyOfferFail = (error: any): OfferActionTypes => {
    return {
        type: APPLY_OFFER_FAIL,
        error: error
    }
}

// Apply to offer
export const applyOffer = (id: string, applicationInfo: ApplicationInterface): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    axios
        .put(`https://mern-jjit.herokuapp.com/api/offers/apply/${id}`, applicationInfo)
        .then(res => {
            dispatch(applyOfferSuccess());
        })
        .catch(err => {
            dispatch(applyOfferFail(err.response.data));
        })
}

export const fetchOffersSuccess = (offers: OfferInterface[]): OfferActionTypes => {
    return {
        type: FETCH_OFFERS_SUCCESS,
        offers: offers
    }
}

export const fetchOffersFail = (error: any): OfferActionTypes => {
    return {
        type: FETCH_OFFERS_FAIL,
        error: error
    }
}

export const fetchOffersStart = (): OfferActionTypes => {
    return {
        type: FETCH_OFFERS_START
    }
}

export const fetchOffers = (filters: FetchFilterInterface): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    dispatch(fetchOffersStart());
    axios
        .post('https://mern-jjit.herokuapp.com/api/offers/filter', filters)
        .then(res => {
            const fetchedOffers = res.data.sort((a: any, b: any) => {
                return Date.parse(b.date) - Date.parse(a.date);
            })
            dispatch(fetchOffersSuccess(fetchedOffers));
        })
        .catch(err => {
            dispatch(fetchOffersFail(err))
        })
}

export const fetchSingleOfferStart = (): OfferActionTypes => {
    return {
        type: FETCH_SINGLE_OFFER_START
    }
}

export const fetchSingleOfferSuccess = (offer: OfferInterface): OfferActionTypes => {
    return {
        type: FETCH_SINGLE_OFFER_SUCCESS,
        offer: offer
    }
}

export const fetchSingleOfferFail = (error: any): OfferActionTypes => {
    return {
        type: FETCH_SINGLE_OFFER_FAIL,
        error: error
    }
}

export const fetchSingleOffer = (params: FetchSingleOfferParams): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    dispatch(fetchSingleOfferStart());
    axios
        .get(`https://mern-jjit.herokuapp.com/api/offers/${params.name}/${params.title}`)
        .then(res => {
            console.log('res', res)
            dispatch(fetchSingleOfferSuccess(res.data))
        })
        .catch(err => {
            console.log('err', err)
            dispatch(fetchSingleOfferFail(err.message))
        })
}

export const sortByLowestSalary = (): OfferActionTypes => {
    return {
        type: SORT_BY_LOWEST_SALARY
    }
}

export const sortByHighestSalary = (): OfferActionTypes => {
    return {
        type: SORT_BY_HIGHEST_SALARY
    }
}

export const sortByLatest = (): OfferActionTypes => {
    return {
        type: SORT_BY_LATEST
    }
}