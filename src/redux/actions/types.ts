import {OfferInterface} from "../../interfaces/offer.interface";
import {SelectFilterValue} from "../../interfaces/filter.interface";

export const SET_USER_LOADING = "SET_USER_LOADING";
export interface SetUserLoadingAction {
    type: typeof SET_USER_LOADING;
}

export const SET_CURRENT_USER = "SET_CURRENT_USER";
export interface SetCurrentUserAction {
    type: typeof SET_CURRENT_USER;
    payload: any;
}

export const INIT_OFFER = "INIT_OFFER";
export interface InitOfferAction {
    type: typeof INIT_OFFER;
    payload: OfferInterface;
}

export const ADD_OFFER_START = "ADD_OFFER_START";
export interface AddOfferStartAction {
    type: typeof ADD_OFFER_START;
}

export const ADD_OFFER_SUCCESS = "ADD_OFFER_SUCCESS";
export interface AddOfferSuccessAction {
    type: typeof ADD_OFFER_SUCCESS;
}

export const ADD_OFFER_FAIL = "ADD_OFFER_FAIL";
export interface AddOfferFailAction {
    type: typeof ADD_OFFER_FAIL;
    error: any;
}

export const APPLY_OFFER_START = "APPLY_OFFER_START";
export interface ApplyOfferStartAction {
    type: typeof APPLY_OFFER_START;
}

export const APPLY_OFFER_SUCCESS = "APPLY_OFFER_SUCCESS";
export interface ApplyOfferSuccessAction {
    type: typeof APPLY_OFFER_SUCCESS;
}

export const APPLY_OFFER_FAIL = "APPLY_OFFER_FAIL";
export interface ApplyOfferFailAction {
    type: typeof APPLY_OFFER_FAIL;
    error: any;
}

export const FETCH_OFFERS_START = "FETCH_OFFERS_START";
export interface FetchOffersStartAction {
    type: typeof FETCH_OFFERS_START;
}

export const FETCH_OFFERS_SUCCESS = "FETCH_OFFERS_SUCCESS";
export interface FetchOffersSuccessAction {
    type: typeof FETCH_OFFERS_SUCCESS;
    offers: OfferInterface[];
}

export const FETCH_OFFERS_FAIL = "FETCH_OFFERS_FAIL";
export interface FetchOffersFailAction {
    type: typeof FETCH_OFFERS_FAIL;
    error: any;
}

export const FETCH_MY_OFFERS_START = "FETCH_MY_OFFERS_START";
export interface FetchMyOffersStartAction {
    type: typeof FETCH_MY_OFFERS_START;
}

export const FETCH_MY_OFFERS_SUCCESS = "FETCH_MY_OFFERS_SUCCESS";
export interface FetchMyOffersSuccessAction {
    type: typeof FETCH_MY_OFFERS_SUCCESS;
    offers: OfferInterface[];
}

export const FETCH_MY_OFFERS_FAIL = "FETCH_MY_OFFERS_FAIL";
export interface FetchMyOffersFailAction {
    type: typeof FETCH_MY_OFFERS_FAIL;
    error: any;
}

export const FETCH_SINGLE_OFFER_START = "FETCH_SINGLE_OFFER_START";
export interface FetchSingleOfferStartAction {
    type: typeof FETCH_SINGLE_OFFER_START;
}

export const FETCH_SINGLE_OFFER_SUCCESS = "FETCH_SINGLE_OFFER_SUCCESS";
export interface FetchSingleOfferSuccessAction {
    type: typeof FETCH_SINGLE_OFFER_SUCCESS;
    offer: OfferInterface;
}

export const FETCH_SINGLE_OFFER_FAIL = "FETCH_SINGLE_OFFER_FAIL";
export interface FetchSingleOfferFailAction {
    type: typeof FETCH_SINGLE_OFFER_FAIL;
    error: any;
}

export const SORT_BY_LOWEST_SALARY = "SORT_BY_LOWEST_SALARY";
export interface SortByLowestSalaryAction {
    type: typeof SORT_BY_LOWEST_SALARY;
}

export const SORT_BY_HIGHEST_SALARY = "SORT_BY_HIGHEST_SALARY";
export interface SortByHighestSalaryAction {
    type: typeof SORT_BY_HIGHEST_SALARY;
}

export const SORT_BY_LATEST = "SORT_BY_LATEST";
export interface SortByLatestAction {
    type: typeof SORT_BY_LATEST;
}

export const SELECT_FILTER = 'SELECT_FILTER';
export interface SelectFilterAction {
    type: typeof SELECT_FILTER;
    filter: string;
    value: SelectFilterValue;
}

export const CLEAR_FILTER = 'CLEAR_FILTER';
export interface ClearFilterAction {
    type: typeof CLEAR_FILTER;
    filters: string[];
}

export type AuthActionTypes =
    | SetUserLoadingAction
    | SetCurrentUserAction
    | FetchMyOffersStartAction
    | FetchMyOffersSuccessAction
    | FetchMyOffersFailAction

export type FilterActionTypes =
    | SelectFilterAction
    | ClearFilterAction

export type OfferActionTypes =
    | InitOfferAction
    | AddOfferStartAction
    | AddOfferSuccessAction
    | AddOfferFailAction
    | ApplyOfferStartAction
    | ApplyOfferSuccessAction
    | ApplyOfferFailAction
    | FetchOffersStartAction
    | FetchOffersSuccessAction
    | FetchOffersFailAction
    | FetchSingleOfferStartAction
    | FetchSingleOfferSuccessAction
    | FetchSingleOfferFailAction
    | SortByLowestSalaryAction
    | SortByHighestSalaryAction
    | SortByLatestAction