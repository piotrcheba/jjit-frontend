import {
    FilterActionTypes,
    SELECT_FILTER,
    CLEAR_FILTER
} from "./types";
import {SelectFilterValue} from "../../interfaces/filter.interface";

export function selectFilter(filter: string, value: SelectFilterValue): FilterActionTypes {
    return {
        type: SELECT_FILTER,
        filter: filter,
        value: value
    }
}

export function clearFilter(filters: string[]): FilterActionTypes {
    return {
        type: CLEAR_FILTER,
        filters: filters
    }
}