import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

import {
    AuthActionTypes,
    FETCH_MY_OFFERS_FAIL,
    FETCH_MY_OFFERS_START,
    FETCH_MY_OFFERS_SUCCESS,
    SET_CURRENT_USER,
    SET_USER_LOADING
} from "./types";
import {AppDispatch, RootState} from "../store";
import {ThunkAction} from "redux-thunk";
import {Action} from "redux";
import {OfferInterface} from "../../interfaces/offer.interface";
import {LoginUserInterface, RegisterUserInterface} from "../../interfaces/auth.interface";

// Register User
export const registerUser = (userData: RegisterUserInterface): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    axios
        .post('https://mern-jjit.herokuapp.com/api/users/register', userData)
        .catch(err =>
            console.log(err)
        );
};

// Login - get user token
export const loginUser = (userData: LoginUserInterface): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    axios
        .post('https://mern-jjit.herokuapp.com/api/users/login', userData)
        .then(res => {
            // Set token to localStorage
            const { token } = res.data;
            localStorage.setItem('jwtToken', token);
            // Set token to Auth header
            setAuthToken(token);
            // Decode token to get user data
            const decoded = jwt_decode(token);
            // Set current user
            dispatch(setCurrentUser(decoded));
        })
        .catch(err =>
            console.log(err)
        );
};

// EmpsLogin - token stored in localstorage
export const verifyAuth = (): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    const token = localStorage.getItem('jwtToken');
    if (token) {
        setAuthToken(token);
        const decoded: any = jwt_decode(token);
        if (decoded.exp > Date.now() / 1000) {
            dispatch(setCurrentUser(decoded));
        }
    } else return null;
}

// Set logged in user
export const setCurrentUser = (decoded: any): AuthActionTypes => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    };
};

// User loading
export const setUserLoading = (): AuthActionTypes => {
    return {
        type: SET_USER_LOADING
    };
};

// Edit user
export const updateUser = (id: string, data: any): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    axios
        .patch(`https://mern-jjit.herokuapp.com/api/users/${id}`, data)
        .then(res => {
            // Set token to localStorage
            const { token } = res.data;
            localStorage.setItem('jwtToken', token);
            // Set token to Auth header
            setAuthToken(token);
            // Decode token to get user data
            const decoded = jwt_decode(token);
            // Set current user
            dispatch(setCurrentUser(decoded));
        })
        .catch(err =>
            console.log(err)
        );
}

// Log user out
export const logoutUser = (): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    // Remove token from local storage
    localStorage.removeItem('jwtToken');
    // Remove auth header for future requests
    setAuthToken(false);
    // Set current user to empty object {} which will set isAuthenticated to false
    dispatch(setCurrentUser({}))
}

// Delete user
export const deleteUser = (id: string): ThunkAction<void, RootState, unknown, Action> => (dispatch: AppDispatch) => {
    axios
        .delete(`https://mern-jjit.herokuapp.com/api/users/${id}`)
        .then(res => {
            console.log(res);
            // Remove token from local storage
            localStorage.removeItem('jwtToken');
            // Remove auth header for future requests
            setAuthToken(false);
            // Set current user to empty object {} which will set isAuthenticated to false
            dispatch(setCurrentUser({}))
        })
        .catch(err =>
            console.log(err)
        );
}

export const fetchMyOffersStart = (): AuthActionTypes => {
    return {
        type: FETCH_MY_OFFERS_START
    }
}

export const fetchMyOffersSuccess = (fetchedOffers: OfferInterface[]): AuthActionTypes => {
    return {
        type: FETCH_MY_OFFERS_SUCCESS,
        offers: fetchedOffers
    }
}

export const fetchMyOffersFail = (error: any): AuthActionTypes => {
    return {
        type: FETCH_MY_OFFERS_FAIL,
        error: error
    }
}

// Fetch my offers
export const fetchMyOffers = (id: string): ThunkAction<void, RootState, unknown, Action> => async (dispatch: AppDispatch) => {
    dispatch(fetchMyOffersStart());
    axios
        .get(`https://mern-jjit.herokuapp.com/api/offers/?id=${id}`)
        .then(res => {
            const fetchedOffers = res.data.sort((a: any, b: any) => {
                return Date.parse(b.date) - Date.parse(a.date);
            })
            dispatch(fetchMyOffersSuccess(fetchedOffers))
        })
        .catch(err => {
            dispatch(fetchMyOffersFail(err.message))
        })
}