import React, {ReactElement} from 'react';
import {styles} from './Verify.styles';
import OfferHeader from "../../OfferHeader/OfferHeader";
import OfferSection from "../../OfferSection/OfferSection";
import MapWrapper from "../Create/MapWrapper/MapWrapper";
import {useSelector} from "react-redux";
import Skill from '../../Skill/Skill';
import parse from 'html-react-parser';
import {VerifyProps} from './Verify.interface';
import {RootState} from "../../../redux/store";
import {OfferInterface, SkillInterface} from "../../../interfaces/offer.interface";

export default function Verify({next}: VerifyProps): ReactElement {
    const offer = useSelector((state: RootState): OfferInterface | undefined => state.offers.activeOffer)

    return (
        <styles.Wrapper>
            <styles.OfferSummary>
                <OfferHeader/>
                <styles.SectionWrapper>
                    <OfferSection
                        title="Tech stack"
                    >
                        <styles.SkillsContainer>
                            {offer?.skills.map((el: SkillInterface): ReactElement => (
                                <Skill
                                    label={el.name}
                                    level={el.level}
                                />
                            ))}
                        </styles.SkillsContainer>
                    </OfferSection>
                </styles.SectionWrapper>
                <styles.SectionWrapper>
                    <OfferSection
                        title="Description"
                    >
                        <styles.DescriptionContainer>
                            {parse(offer!.jobDesc)}
                        </styles.DescriptionContainer>
                    </OfferSection>
                </styles.SectionWrapper>
                <styles.MapContainer>
                    <MapWrapper icon={offer!.mainTech} geoLoc={offer!.geoLoc}/>
                </styles.MapContainer>
            </styles.OfferSummary>
            <styles.NextBtn
                data-testid="verify-nextbutton"
                onClick={next}
            >
                Post offer
            </styles.NextBtn>
        </styles.Wrapper>
    )
};