import styled from "styled-components";

export const styles = {
    Wrapper: styled.div`
        max-width: 720px;
        margin: auto;
        padding: 30px 0 0 0;
    `,
    OfferSummary: styled.div`
        margin: 25px 0 0 0;
    `,
    SectionWrapper: styled.div`
        background-color: #fff;
        position: relative;
        border-radius: 5px;
        overflow: hidden;
        box-shadow: 0 1px 3px 1px rgba(0,0,0,0.08), 0 1px 2px rgba(0,0,0,0.08);
        margin-top: 40px;
    `,
    SkillsContainer: styled.div`
        display: flex;
        flex-wrap: wrap;
        margin: 0 0 -20px 0;
    `,
    DescriptionContainer: styled.div`
        line-height: 1.5;
    `,
    MapContainer: styled.div`
        margin: 20px 0 20px 0;
        width: 100%;
        height: 300px;
        position: relative;
    `,
    NextBtn: styled.button`
        height: 36px;
        border: none;
        line-height: 36px;
        padding: 0 2rem;
        text-transform: uppercase;
        font-size: 1rem;
        outline: 0;
        text-align: center;
        letter-spacing: .5px;
        color: #fff;
        text-decoration: none;
        position: relative;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        user-select: none;
        vertical-align: middle;
        z-index: 1;
        transition: .3s ease-out;
        margin: 0;
        font-weight: 400;
        border-radius: 20px;
        background-color: #ff4081;
        &:hover {
            box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14), 0 1px 7px 0 rgba(0,0,0,0.12), 0 3px 1px -1px rgba(0,0,0,0.2);
            background-color: #ff5a92;
        }
    `,
}