import styled from "styled-components";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

export const styles = {
    MainGrid: styled.div`
        max-width: 900px;
        padding: 20px;
        margin: 20px auto 20px auto;
        background-color: #fff;
        position: relative;
        border-radius: 5px;
        box-shadow: 0 2px 18px 10px rgba(225, 232, 240, 0.7);
        overflow: hidden;
    `,
    BackBtn: styled.div`
        position: absolute;
        right: 30px;
        font-weight: 400;
        cursor: pointer;
    `,
    StyledIcon: styled(ArrowBackIcon)`
        vertical-align: middle;
    `,
};

export const muiStyles = {
    stepper: {
        backgroundColor: 'transparent',
        margin: '30px auto',
        padding: 0,
        maxWidth: '600px',
    }
}