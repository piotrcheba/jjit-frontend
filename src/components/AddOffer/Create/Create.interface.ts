export interface CreateProps {
    next: () => void;
}

export interface CreateTechItemInterface {
    type: string;
    id: string;
    label: string;
}