import styled from "styled-components";

export const styles = {
    Wrapper: styled.div`
        padding: 16px;
    `,
    FilePlaceholder: styled.div`
        width: 100%;
        height: 100%;
        color: red;
        background-color: red;
        border-radius: 5px;
    `,
    UploadLogoContainer: styled.div`
        text-align: center;
        cursor: pointer;
        position: relative;
        width: 100%;
        &:hover {
            color: #64407a;
        }
        & svg {
            display: block;
            font-size: 60px;
            fill: rgb(55, 71, 79);
            &:hover {
                fill: #64407a;
            }
        }
    `,
    SectionHeader: styled.div`
        font-weight: bold;
        font-size: 1.2em;
        line-height: 40px;
        color: #666;
    `,
    MapContainer: styled.div`
        width: 100%;
        height: 300px;
        position: relative;
        margin-bottom: 15px;
    `,
    NextBtn: styled.button`
        height: 36px;
        border: none;
        line-height: 36px;
        padding: 0 2rem;
        text-transform: uppercase;
        font-size: 1rem;
        outline: 0;
        text-align: center;
        letter-spacing: .5px;
        color: #fff;
        text-decoration: none;
        position: relative;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        user-select: none;
        vertical-align: middle;
        z-index: 1;
        transition: .3s ease-out;
        margin-top: 2rem;
        font-weight: 400;
        border-radius: 20px;
        background-color: #ff4081;
        &:disabled {
            background-color: rgba(0, 0, 0, 0.12);
            box-shadow: none;
            color: rgba(0, 0, 0, 0.26);
            &:hover {
                background-color: rgba(0, 0, 0, 0.12);
                box-shadow: none;
            }
        }
        &:hover {
            box-shadow: 0 3px 3px 0 rgba(0,0,0,0.14), 0 1px 7px 0 rgba(0,0,0,0.12), 0 3px 1px -1px rgba(0,0,0,0.2);
            background-color: #ff5a92;
        }
    `,
    TechContainer: styled.div`
        margin: 25px 0 0 0;
    `,
    TechLabel: styled.span`
        display: block;
        text-align: center;
    `,
    TechList: styled.div`
        margin: 8px 0 10px 0;
        padding: 5px;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    `,
    ImgInput: styled.input`
        display: none;
    `,
    ThumbContainer: styled.div`
        position: relative;
        display: inline-block;
        & i {
            cursor: pointer;
            position: absolute;
            right: -6px;
            top: -6px;
            color: #64407a;
            font-size: 15px;
            background: #fff;
            border-radius: 50%;
            border: 1px solid #64407a;
            opacity: 0;
            z-index: 10;
            &:hover {
                opacity: 1;
                background: #64407a;
                color: #fff;
                transition: .2s linear all;
            }
        }
    `,
    Thumb: styled.img`
        max-width: 200px;
        max-height: 100px;
        cursor: pointer;
        &:hover {
            & + i {
                opacity: 1;
            }
        }
    `,
    SkillsContainer: styled.div`
        display: flex;
        padding: 20px 0 0 0;
        flex-wrap: wrap;
        margin: 0 0 -20px 0;
    `
}