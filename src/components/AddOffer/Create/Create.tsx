import React, {ReactElement, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {initOffer} from "../../../redux/actions/offerActions";
import {Editor} from '@tinymce/tinymce-react';
import Geocode from 'react-geocode';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Autocomplete, {AutocompleteRenderInputParams} from "@material-ui/lab/Autocomplete";
import Skill from '../../Skill/Skill';
import TechButton from "../../Filterbar/TechButton/TechButton";
import MapWrapper from "./MapWrapper/MapWrapper";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {styles} from './Create.styles';
import {addOfferValidator} from '../addOfferValidator';
import {checkValidity} from "../../../utils/checkValidity";
import {AddOfferValidatorOptionInterface, CheckboxOnly, InputOnly} from "../addOfferValidator.interface";
import {OfferInterface, SkillInterface} from '../../../interfaces/offer.interface'
import {techItems, labels} from './Create.constants';
import {apiConfig} from "../../../config/api";
import {RootState} from "../../../redux/store";
import {UserInterface} from "../../../interfaces/auth.interface";
import {CreateProps, CreateTechItemInterface} from "./Create.interface";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& .MuiTextField-root': {
                display: 'flex',
                alignItems: 'center',
                position: 'relative',
                marginTop: '.5rem',
                marginLeft: 'auto',
            },
            '& .MuiFormLabel-root:not(.Mui-focused)': {
                fontSize: '0.9rem',
            },
            '& .MuiInputBase-input': {
                height: '1.3rem',
                fontSize: '0.9rem',
                margin: '0 0 15px 0',
                padding: '0',
                color: 'inherit',
                lineHeight: 'normal',
            },
            '& .MuiSelect-select': {
                marginTop: '16px',
                color: '#9e9e9e',
            },
            '& .MuiFormHelperText-root': {
                alignSelf: 'flex-start',
                fontSize: '0.7rem',
                lineHeight: 1,
            }
        }
    }))

export default function Create({next}: CreateProps): ReactElement {
    const classes = useStyles();

    const offer = useSelector((state: RootState): OfferInterface | undefined => state.offers.activeOffer);
    const user = useSelector((state: RootState): UserInterface | undefined => state.auth.user)

    const [offerForm, setOfferForm] = useState(addOfferValidator(offer))
    const [formValidator, setFormValidator] = useState<boolean | undefined>(false);
    const [activeButton, setActiveButton] = useState(offer?.mainTech || 'unknown');
    const [geoLoc, setGeoLoc] = useState(offer?.geoLoc || {
        lat: 51.921253,
        lng: 19.145759
    });
    const [skills, setSkills] = useState<SkillInterface[]>(offer?.skills || []);

    const dispatch = useDispatch();

    const inputStreetRef = useRef<HTMLInputElement>();
    const inputCityRef = useRef<HTMLInputElement>();

    const fetchGeoLoc = (location: string): void => {
        Geocode.fromAddress(location).then(
            (response: any) => {
                const coords = response.results[0].geometry.location;
                setGeoLoc({lat: coords.lat, lng: coords.lng});
            },
            (error: any) => {
                console.error(error);
            }
        );
    }

    useEffect((): () => void => {
        const city = offerForm.officeCity.value;
        const street = offerForm.officeStreet.value;
        const timer = setTimeout(() => {
            if (street === inputStreetRef.current?.value || city === inputCityRef.current?.value) {
                fetchGeoLoc(`${street}, ${city}`)
            }
        }, 500);
        return () => {
            clearTimeout(timer);
        };
    }, [offerForm.officeStreet.value, offerForm.officeCity.value, inputStreetRef, inputCityRef])

    useEffect((): void => {
        if (skills.length === 0) {
            setFormValidator(false)
        }
    }, [skills.length, formValidator])

    const inputChangedHandler = (value: string, inputIdentifier: keyof InputOnly): void => {
        const updatedOfferForm = {
            ...offerForm,
            [inputIdentifier]: {
                ...offerForm[inputIdentifier],
                value: value,
                valid: checkValidity(value, offerForm[inputIdentifier].validation),
                touched: true
            }
        }

        let formIsValid: boolean = true;
        for (let inputIdentifier in updatedOfferForm) {
            // @ts-ignore
            formIsValid = updatedOfferForm[inputIdentifier].valid && formIsValid;
        }

        setFormValidator(formIsValid);
        setOfferForm(updatedOfferForm);
    }

    const checkboxChangedHandler = (value: boolean, inputIdentifier: keyof CheckboxOnly): void => {
        const updatedOfferForm = {
            ...offerForm
        }

        const updatedFormElement = {
            ...updatedOfferForm[inputIdentifier as keyof typeof offerForm]
        }

        updatedFormElement.value = value;

        // @ts-ignore
        updatedOfferForm[inputIdentifier] = updatedFormElement;

        setOfferForm(updatedOfferForm);
    };

    const skillLevelChangedHandler = (value: number, skillIdentifier: string): void => {
        const skillToUpdate = skills.find((el: SkillInterface): boolean => el.name === skillIdentifier);
        const indexToUpdate = skills.findIndex((el: SkillInterface): boolean => el.name === skillIdentifier);

        const updatedSkillElement = {
            ...skillToUpdate,
            level: value,
        };

        const skillsToUpdate = [...skills]

        //@ts-ignore
        skillsToUpdate[indexToUpdate] = updatedSkillElement;

        setSkills(skillsToUpdate);
    }

    const imgBlob = (e: React.ChangeEvent<HTMLInputElement>): void => {
        if (!e.target.files) return;
        let file = e.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function() {
            const base64data = reader.result;
            console.log(base64data)
            // @ts-ignore
            inputChangedHandler(base64data, 'companyLogo');
        }
    }

    const imgCleaner = (): void => {
        const updatedOfferForm = {
            ...offerForm
        }

        const updatedFormElement = {
            ...updatedOfferForm.companyLogo
        }

        updatedFormElement.value = '';
        updatedFormElement.valid = false;

        // @ts-ignore
        updatedOfferForm.companyLogo = updatedFormElement;

        let formIsValid: boolean = true;
        for (let inputIdentifier in updatedOfferForm) {
            // @ts-ignore
            formIsValid = updatedOfferForm[inputIdentifier].valid && formIsValid;
        }

        setOfferForm(updatedOfferForm);
        setFormValidator(formIsValid);
    }

    const submitOfferHandler = (event: React.FormEvent<HTMLFormElement>): void => {
        event.preventDefault();

        const formData: {[index: string]:any} = {};
        for (let key in offerForm) {
            formData[key as keyof typeof formData] = offerForm[key as keyof typeof offerForm].value;
        }

        const offer: any = {
            ...formData,
            geoLoc: geoLoc,
            skills: skills.sort((a, b) =>  b.level - a.level),
            createdBy: user!.id
        };

        dispatch(initOffer(offer));
        next();
    }

    const objSkills = labels.map((el: string): SkillInterface => {
        return {
            name: el,
            level: 1
        }
    })

    const skillRemover = (skillIdentifier: string): void => {
        const indexToRemove = skills.findIndex(el => el.name === skillIdentifier);

        const skillsToUpdate = [...skills];

        skillsToUpdate.splice(indexToRemove, 1);

        setSkills(skillsToUpdate);
    }

    return (
        <form onSubmit={submitOfferHandler} className={classes.root} autoComplete="off" data-testid="create-form">
            <styles.Wrapper>
                <Grid container justify='center' xs={12} spacing={2}>
                    <Grid item xs={4} style={{display: 'flex'}} justify='center' alignItems='center'>
                        {offerForm.companyLogo.value === '' ?
                            <label>
                                <styles.UploadLogoContainer>
                                    <i className="material-icons" style={{fontSize: '60px', display: 'block'}}>add_a_photo</i>
                                    <span>
                                        Upload logo *
                                    </span>
                                </styles.UploadLogoContainer>
                                <styles.ImgInput
                                    data-testid="create-imginput"
                                    id="file-upload"
                                    type="file"
                                    accept="image/png, image/jpg"
                                    onChange={imgBlob}
                                />
                            </label>
                            :
                            <styles.ThumbContainer data-testid="create-thumbcontainer">
                                <styles.Thumb data-testid="create-thumb" src={offerForm.companyLogo.value} />
                                <i
                                    className="material-icons"
                                    onClick={imgCleaner}
                                >
                                    close
                                </i>
                            </styles.ThumbContainer>
                        }
                    </Grid>
                    <Grid container item xs={4}>
                        <Grid item xs={12}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.companyName.valid && offerForm.companyName.touched}
                                helperText={!offerForm.companyName.valid && offerForm.companyName.touched ? 'Company name is required' : ' '}
                                label={offerForm.companyName.label}
                                value={offerForm.companyName.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'companyName')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.companyWebsite.valid && offerForm.companyWebsite.touched}
                                helperText={!offerForm.companyWebsite.valid && offerForm.companyWebsite.touched ? 'Company website is required' : ' '}
                                label={offerForm.companyWebsite.label}
                                value={offerForm.companyWebsite.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'companyWebsite')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.companySize.valid && offerForm.companySize.touched}
                                helperText={!offerForm.companySize.valid && offerForm.companySize.touched ? 'Company size is required' : ' '}
                                label={offerForm.companySize.label}
                                value={offerForm.companySize.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'companySize')}
                            />
                        </Grid>
                    </Grid>
                    <Grid container item xs={4} alignItems='flex-end' alignContent='flex-end'>
                        <Grid item xs={12}>
                            <TextField
                                select
                                color="secondary"
                                fullWidth
                                helperText=" "
                                SelectProps={{
                                    native: true,
                                }}
                                value={offerForm.companyType.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'companyType')}
                            >
                                {offerForm.companyType.options.map((option: AddOfferValidatorOptionInterface): ReactElement => (
                                    <option key={option.value} value={option.value} disabled={option.disabled}>
                                        {option.displayValue}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                select
                                color="secondary"
                                fullWidth
                                helperText=" "
                                SelectProps={{
                                    native: true,
                                }}
                                value={offerForm.companyIndustry.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'companyIndustry')}
                            >
                                {offerForm.companyIndustry.options.map((option: AddOfferValidatorOptionInterface): ReactElement => (
                                    <option key={option.value} value={option.value} disabled={option.disabled}>
                                        {option.displayValue}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid container item xs={4}>
                        <Grid item xs={12}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.title.valid && offerForm.title.touched}
                                helperText={!offerForm.title.valid && offerForm.title.touched ? 'Title is required' : ' '}
                                label={offerForm.title.label}
                                value={offerForm.title.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'title')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.salaryFrom.valid && offerForm.salaryFrom.touched}
                                helperText={!offerForm.salaryFrom.valid && offerForm.salaryFrom.touched ? 'Field is required' : ' '}
                                type="number"
                                label={offerForm.salaryFrom.label}
                                value={offerForm.salaryFrom.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'salaryFrom')}
                            />
                        </Grid>
                    </Grid>
                    <Grid container item xs={4}>
                        <Grid item xs={12}>
                            <TextField
                                select
                                color="secondary"
                                fullWidth
                                helperText=" "
                                SelectProps={{
                                    native: true,
                                }}
                                value={offerForm.expLevel.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'expLevel')}
                            >
                                {offerForm.expLevel.options.map((option: AddOfferValidatorOptionInterface): ReactElement => (
                                    <option key={option.value} value={option.value} disabled={option.disabled}>
                                        {option.displayValue}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.salaryTo.valid && offerForm.salaryTo.touched}
                                helperText={!offerForm.salaryTo.valid && offerForm.salaryTo.touched ? 'Field is required' : ' '}
                                type="number"
                                label={offerForm.salaryTo.label}
                                value={offerForm.salaryTo.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'salaryTo')}
                            />
                        </Grid>
                    </Grid>
                    <Grid container item xs={4}>
                        <Grid item xs={12}>
                            <TextField
                                select
                                color="secondary"
                                fullWidth
                                helperText=" "
                                SelectProps={{
                                    native: true,
                                }}
                                value={offerForm.empType.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'empType')}
                            >
                                {offerForm.empType.options.map((option: AddOfferValidatorOptionInterface): ReactElement => (
                                    <option key={option.value} value={option.value} disabled={option.disabled}>
                                        {option.displayValue}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                select
                                color="secondary"
                                fullWidth
                                helperText=" "
                                SelectProps={{
                                    native: true,
                                }}
                                value={offerForm.currency.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'currency')}
                            >
                                {offerForm.currency.options.map((option: AddOfferValidatorOptionInterface): ReactElement => (
                                    <option key={option.value} value={option.value} disabled={option.disabled}>
                                        {option.displayValue}
                                    </option>
                                ))}
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    color="primary"
                                    checked={offerForm.onlineInterview.value}
                                    onChange={(event: React.ChangeEvent<HTMLInputElement>): void => checkboxChangedHandler(event.target.checked, 'onlineInterview')}
                                    name="onlineInterview"
                                />
                            }
                            label={offerForm.onlineInterview.label}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <styles.SectionHeader>
                            Tech stack
                        </styles.SectionHeader>
                        <Grid item xs={4}>
                            <Autocomplete
                                multiple
                                value={skills}
                                onChange={(event: React.ChangeEvent<{}>, newValue): void => {
                                    let formIsValid = true;

                                    for (let key in offerForm) {
                                        // @ts-ignore
                                        formIsValid = offerForm[key].valid && formIsValid
                                    }

                                    setFormValidator(formIsValid);
                                    setSkills(newValue)
                                }}
                                disablePortal
                                renderTags={() => null}
                                noOptionsText="No labels"
                                options={objSkills}
                                getOptionLabel={(option: SkillInterface): string => option.name}
                                renderInput={(params: AutocompleteRenderInputParams): ReactElement => (
                                    <TextField
                                        fullWidth
                                        error={skills.length === 0}
                                        helperText={skills.length === 0 ? "Atleast one is required" : " "}
                                        color="secondary"
                                        ref={params.InputProps.ref}
                                        inputProps={params.inputProps}
                                        placeholder="Select technology"
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <styles.SkillsContainer>
                                {skills.map((el: SkillInterface): ReactElement => (
                                    <Skill
                                        editable
                                        label={el.name}
                                        level={el.level}
                                        skillRemover={skillRemover}
                                        skillChange={skillLevelChangedHandler}
                                    />
                                ))}
                            </styles.SkillsContainer>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <styles.SectionHeader>
                            Job description
                        </styles.SectionHeader>
                        <Editor
                            apiKey={apiConfig.EDITOR_API_KEY}
                            init={{
                                plugins: 'lists',
                                menubar: false,
                                toolbar: 'undo redo | bold italic | bullist numlist'
                            }}
                            onEditorChange={(content: string): void => inputChangedHandler(content, 'jobDesc')}
                            value={offerForm.jobDesc.value}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <styles.SectionHeader>
                            Choose your location
                        </styles.SectionHeader>
                    </Grid>
                    <Grid container item xs={12} spacing={3} alignItems="flex-end">
                        <Grid item xs={4}>
                            <TextField
                                color='secondary'
                                fullWidth
                                placeholder="Warszawa"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                inputRef={inputCityRef}
                                label={offerForm.officeCity.label}
                                value={offerForm.officeCity.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'officeCity')}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                color='secondary'
                                fullWidth
                                placeholder="Grunwaldzka 34"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                inputRef={inputStreetRef}
                                label={offerForm.officeStreet.label}
                                value={offerForm.officeStreet.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'officeStreet')}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        color="primary"
                                        checked={offerForm.fullyRemote.value}
                                        onChange={(event: React.ChangeEvent<HTMLInputElement>): void => checkboxChangedHandler(event.target.checked, 'fullyRemote')}
                                        name="fullyRemote"
                                    />
                                }
                                label={offerForm.fullyRemote.label}
                            />
                        </Grid>
                    </Grid>
                    <styles.MapContainer>
                        <MapWrapper icon={activeButton} geoLoc={geoLoc} />
                    </styles.MapContainer>
                    <styles.TechContainer>
                        <styles.TechLabel>
                            Choose main technology
                        </styles.TechLabel>
                        <styles.TechList>
                            {techItems.map((item: CreateTechItemInterface): ReactElement => (
                                <TechButton
                                    {...item}
                                    key={item.id}
                                    className={activeButton !== 'unknown' && activeButton !== item.id ? 'disabled' : undefined}
                                    onClick={(): void => {
                                        setActiveButton(item.id);
                                        inputChangedHandler(item.id, 'mainTech');
                                    }}
                                />
                            ))}
                        </styles.TechList>
                    </styles.TechContainer>
                    <Grid container item xs={12}>
                        <Grid item xs={12}>
                            <styles.SectionHeader>
                                How to apply
                            </styles.SectionHeader>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                color='secondary'
                                fullWidth
                                error={!offerForm.contact.valid && offerForm.contact.touched}
                                helperText={!offerForm.contact.valid && offerForm.contact.touched ? 'Contact is required' : ' '}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                label={offerForm.contact.label}
                                value={offerForm.contact.value}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>): void => inputChangedHandler(event.target.value, 'contact')}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <styles.NextBtn
                    type="submit"
                    disabled={!formValidator}
                    data-testid="create-nextbutton"
                >
                    Next Step
                </styles.NextBtn>
            </styles.Wrapper>
        </form>
    )
};

Geocode.setApiKey(apiConfig.GEOCODE_API_KEY);