import React, {ReactElement} from 'react';
import {GoogleApiWrapper, Map, Marker} from "google-maps-react";
import {apiConfig} from "../../../../config/api";
import {MapWrapperProps} from "./MapWrapper.interface";

const mapStyles = {
    width: '100%',
    height: '100%',
};

const MapWrapper = React.memo(({google, geoLoc, icon}: MapWrapperProps): ReactElement => {
    return (
        <Map
            google={google}
            zoom={6}
            // @ts-ignore
            style={mapStyles}
            initialCenter={{
                lat: 51.921253,
                lng: 19.145759
            }}
            mapTypeControl={false}
            fullscreenControl={false}
            maxZoom={12}
            bounds={new google.maps.LatLngBounds().extend({lat: geoLoc.lat, lng: geoLoc.lng})}
        >
            <Marker
                icon={{
                    url: require(`../../../../assets/markers/png/${icon}.png`),
                    scaledSize: new google.maps.Size(40,40),
                    anchor: new google.maps.Point(20, 20)}}
                position={{lat: geoLoc.lat, lng: geoLoc.lng}}

            />
        </Map>
    )
});

export default GoogleApiWrapper({
    apiKey: apiConfig.GOOGLE_API_KEY
})(MapWrapper);