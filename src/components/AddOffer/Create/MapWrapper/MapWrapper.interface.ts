import {GoogleAPI} from "google-maps-react";

export interface MapWrapperProps {
    google: GoogleAPI;
    geoLoc: {
        lat: number,
        lng: number
    };
    icon: string;
}