import React, {ReactElement, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addOffer} from "../../../redux/actions/offerActions";
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from "@material-ui/core/Typography";
import {Link} from "@material-ui/core";
import {styles} from './Publish.styles';
import {RootState} from "../../../redux/store";
import {OfferStateInterface} from "../../../interfaces/offer.interface";

const MsgSuccess = (): ReactElement => (
    <>
        <styles.Icon/>
        <styles.Text data-testid='publish-offeraddedtext'>
            Thank you! Your offer has been added!
        </styles.Text>
        <Typography
            variant="subtitle2"
            gutterBottom
        >
            <Link href='/' color='primary' underline="hover">
                Go back to homepage
            </Link>
        </Typography>
    </>
)

export default function Publish(): ReactElement {
    const offers = useSelector((state: RootState): OfferStateInterface => state.offers);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(addOffer(offers.activeOffer!));
    }, [])

    return (
        <styles.Wrapper>
            {offers.loading ? <CircularProgress size={80} data-testid='publish-spinner'/> : <MsgSuccess />}
        </styles.Wrapper>
    )
};