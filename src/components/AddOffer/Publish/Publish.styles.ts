import styled from 'styled-components';
import CheckCircleOutlinedIcon from '@material-ui/icons/CheckCircleOutlined';

export const styles = {
    Wrapper: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin: 50px auto;
    `,
    Icon: styled(CheckCircleOutlinedIcon)`
        width: 100px;
        height: 100px;
        font-size: 80px;
        fill: rgb(255, 64, 129);
    `,
    Text: styled.span`
        padding-top: 10px;
        color: #2E384D;
        font-size: 22px;
        font-family: Open Sans, sans-serif;
        font-weight: 300;
        line-height: 1.167;
        margin-bottom: 10px;
    `
}

export const muiStyles = {
    header: {
        color: '#2E384D',
        fontSize: '22px',
        fontFamily: 'Open Sans, sans-serif',
        fontWeight: 300,
        lineHeight: 1.167,
        marginBottom: '20px',
    }
}