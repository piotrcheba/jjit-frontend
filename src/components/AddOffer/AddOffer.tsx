import React, {ReactElement, useState} from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Create from './Create/Create';
import Verify from "./Verify/Verify";
import Publish from './Publish/Publish';
import { makeStyles } from "@material-ui/core/styles";
import {styles, muiStyles} from './AddOffer.styles';
import {AddOfferProps} from "./AddOffer.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function AddOffer({history}: AddOfferProps): ReactElement {
    const classes = useStyles();

    const [activeStep, setActiveStep] = useState<number>(0);

    const steps = ['Create', 'Verify', 'Publish'];

    const handleNext = (): void => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = (): void => {
        if (activeStep === 0) {
            history.push('/');
        }
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    function getStepContent(step: number): ReactElement | null {
        switch (step) {
            case 0:
                return <Create next={handleNext}/>;
            case 1:
                return <Verify next={handleNext}/>;
            case 2:
                return <Publish/>
            default:
                return null;
        }
    }

    return (
        <>
            <Stepper className={classes.stepper} activeStep={activeStep}>
                {steps.map((label: string): ReactElement => (
                    <Step key={label}>
                        <StepLabel data-testid={label}>
                            {label}
                        </StepLabel>
                    </Step>
                ))}
            </Stepper>
            <styles.MainGrid>
                {activeStep === 2 ? null :
                    <styles.BackBtn
                        onClick={handleBack}
                        data-testid="addoffer-button"
                    >
                        <styles.StyledIcon/>
                        <span>Back</span>
                    </styles.BackBtn>
                }
                {getStepContent(activeStep)}
            </styles.MainGrid>
        </>
    )
};