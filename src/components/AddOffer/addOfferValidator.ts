import {AddOfferValidatorInterface} from "./addOfferValidator.interface";
import {OfferInterface} from "../../interfaces/offer.interface";

export const addOfferValidator = (offer: OfferInterface | undefined): AddOfferValidatorInterface => ({
    companyLogo: {
        value: offer?.companyLogo || '',
        validation: {
            required: true,
        },
        valid: !!offer?.companyLogo,
        touched: !!offer?.companyLogo
    },
    companyName: {
        value: offer?.companyName || '',
        validation: {
            required: true,
            maxLength: 40,
        },
        label: 'Short company name *',
        valid: !!offer?.companyName,
        touched: !!offer?.companyName
    },
    companyWebsite: {
        value: offer?.companyWebsite || '',
        validation: {
            required: true,
        },
        label: 'Company website *',
        valid: !!offer?.companyWebsite,
        touched: !!offer?.companyWebsite
    },
    companySize: {
        value: offer?.companySize || '',
        label: 'Company size *',
        validation: {
            required: true,
            isNumeric: true,
        },
        valid: !!offer?.companySize,
        touched: !!offer?.companySize
    },
    companyType: {
        options: [
            {value: '', displayValue: 'Choose company type', disabled: true},
            {value: 'Startup', displayValue: 'Startup'},
            {value: 'Software House', displayValue: 'Software House'},
            {value: 'E-commerce', displayValue: 'E-commerce'},
            {value: 'Corporation', displayValue: 'Corporation'},
            {value: 'Other', displayValue: 'Other'},
        ],
        value: offer?.companyType || '',
        validation: {
            required: true,
        },
        valid: !!offer?.companyType,
        touched: !!offer?.companyType
    },
    companyIndustry: {
        options: [
            {value: '', displayValue: 'Choose company industry', disabled: true},
            {value: 'Fintech', displayValue: 'Fintech'},
            {value: 'Blockchain', displayValue: 'Blockchain'},
            {value: 'E-commerce', displayValue: 'E-commerce'},
            {value: 'Medicine', displayValue: 'Medicine'},
            {value: 'Military', displayValue: 'Military'},
            {value: 'Martech', displayValue: 'Martech'},
            {value: 'IoT', displayValue: 'IoT'},
            {value: 'Logistic', displayValue: 'Logistic'},
            {value: 'Beauty', displayValue: 'Beauty'},
            {value: 'Travel', displayValue: 'Travel'},
            {value: 'Other', displayValue: 'Other'},
        ],
        value: offer?.companyIndustry || '',
        validation: {
            required: true,
        },
        valid: !!offer?.companyIndustry,
        touched: !!offer?.companyIndustry
    },
    title: {
        value: offer?.title || '',
        label: 'Title *',
        validation: {
            required: true,
            maxLength: 40,
        },
        valid: !!offer?.title,
        touched: !!offer?.title
    },
    expLevel: {
        options: [
            {value: '', displayValue: 'Choose an experience level', disabled: true},
            {value: 'Junior', displayValue: 'Junior'},
            {value: 'Mid', displayValue: 'Mid'},
            {value: 'Senior', displayValue: 'Senior'},
        ],
        value: offer?.expLevel || '',
        validation: {
            required: true,
        },
        valid: !!offer?.expLevel,
        touched: !!offer?.expLevel
    },
    empType: {
        options: [
            {value: '', displayValue: 'Choose an employment type', disabled: true},
            {value: 'B2B', displayValue: 'B2B'},
            {value: 'Permanent', displayValue: 'Permanent'},
            {value: 'Mandate contract', displayValue: 'Mandate contract'},
        ],
        value: offer?.empType || '',
        validation: {
            required: true,
        },
        valid: !!offer?.empType,
        touched: !!offer?.empType
    },
    salaryFrom: {
        value: offer?.salaryFrom || '',
        label: 'Monthly salary from (invoice net)',
        validation: {
            required: true,
            minLength: 4,
        },
        valid: !!offer?.salaryFrom,
        touched: !!offer?.salaryFrom
    },
    salaryTo: {
        value: offer?.salaryTo || '',
        label: 'Monthly salary to (invoice net)',
        validation: {
            required: true,
            minLength: 4,
        },
        valid: !!offer?.salaryTo,
        touched: !!offer?.salaryTo
    },
    currency: {
        options: [
            {value: '', displayValue: 'Choose currency', disabled: true},
            {value: 'PLN', displayValue: 'PLN'},
            {value: 'EUR', displayValue: 'EUR'},
            {value: 'USD', displayValue: 'USD'},
            {value: 'GBP', displayValue: 'GBP'},
            {value: 'CHF', displayValue: 'CHF'},
        ],
        value: offer?.currency || '',
        validation: {
            required: true,
        },
        valid: !!offer?.currency,
        touched: !!offer?.currency
    },
    onlineInterview: {
        value: offer?.onlineInterview || false,
        label: 'Online interview available',
        valid: true
    },
    jobDesc: {
        value: offer?.jobDesc || '',
        validation: {
            required: true,
        },
        valid: !!offer?.jobDesc,
        touched: !!offer?.jobDesc
    },
    officeCity: {
        value: offer?.officeCity || '',
        label: 'Office city *',
        validation: {
            required: true,
        },
        valid: !!offer?.officeCity,
        touched: !!offer?.officeCity
    },
    officeStreet: {
        value: offer?.officeStreet || '',
        label: 'Office street / Business center *',
        validation: {
            required: true,
        },
        valid: !!offer?.officeStreet,
        touched: !!offer?.officeStreet
    },
    fullyRemote: {
        value: offer?.fullyRemote || false,
        label: 'Fully remote',
        valid: true
    },
    mainTech: {
        value: offer?.mainTech || '',
        validation: {
            required: true,
        },
        valid: !!offer?.mainTech,
        touched: !!offer?.mainTech,
    },
    contact: {
        value: offer?.contact || '',
        label: 'Enter apply email or paste link *',
        validation: {
            required: true,
        },
        valid: !!offer?.contact,
        touched: !!offer?.contact
    },
})