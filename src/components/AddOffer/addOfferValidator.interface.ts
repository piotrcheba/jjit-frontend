export interface AddOfferValidatorOptionInterface {
    value: string;
    displayValue: string;
    disabled?: boolean;
}

export interface AddOfferValidatorInterface {
    companyLogo: {
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    companyName: {
        value: string,
        validation: {
            required: boolean,
            maxLength: number,
        },
        label: string,
        valid: boolean,
        touched: boolean
    },
    companyWebsite: {
        value: string,
        validation: {
            required: boolean,
        },
        label: string,
        valid: boolean,
        touched: boolean
    },
    companySize: {
        value: string,
        label: string,
        validation: {
            required: boolean,
            isNumeric: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    companyType: {
        options: AddOfferValidatorOptionInterface[],
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    companyIndustry: {
        options: AddOfferValidatorOptionInterface[],
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    title: {
        value: string,
        label: string,
        validation: {
            required: boolean,
            maxLength: number,
        },
        valid: boolean,
        touched: boolean
    },
    expLevel: {
        options: AddOfferValidatorOptionInterface[],
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    empType: {
        options: AddOfferValidatorOptionInterface[],
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    salaryFrom: {
        value: string | number,
        label: string
        validation: {
            required: boolean,
            minLength: number,
        },
        valid: boolean,
        touched: boolean
    },
    salaryTo: {
        value: string | number,
        label: string,
        validation: {
            required: boolean,
            minLength: number,
        },
        valid: boolean,
        touched: boolean
    },
    currency: {
        options: AddOfferValidatorOptionInterface[],
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    onlineInterview: {
        value: boolean,
        label: string,
        valid: true
    },
    jobDesc: {
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    officeCity: {
        value: string,
        label: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    officeStreet: {
        value: string,
        label: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
    fullyRemote: {
        value: boolean,
        label: string,
        valid: true
    },
    mainTech: {
        value: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean,
    },
    contact: {
        value: string,
        label: string,
        validation: {
            required: boolean,
        },
        valid: boolean,
        touched: boolean
    },
}

export type InputOnly = Omit<AddOfferValidatorInterface, "onlineInterview" | "fullyRemote" | "skills">
export type CheckboxOnly = Pick<AddOfferValidatorInterface, "onlineInterview" | "fullyRemote">