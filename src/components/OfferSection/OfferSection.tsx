import React, {ReactElement} from 'react';
import {styles} from './OfferSection.styles';
import {OfferSectionProps} from "./OfferSection.interface";

export default function OfferSection({title, children}: OfferSectionProps): ReactElement {
    return (
        <>
            <styles.Title data-testid='offersection-title'>
                {title}
            </styles.Title>
            <styles.Content skillSection={title === 'Tech stack'}>
                {children}
            </styles.Content>
        </>
    )
};