import styled from "styled-components";

export const styles = {
    Title: styled.div`
        background-color: #fff;
        padding: 12px 20px;
        border-bottom: 2px solid #f3f6f8;
        font-weight: 600;
        color: rgb(55, 71, 79);
        font-size: 18px;
        position: static;
    `,

    Content: styled.div`
        color: rgb(55, 71, 79);
        padding: ${(props: {skillSection: boolean}) => props.skillSection ? '20px 10px' : '20px'};
        position: relative;
    `,
}