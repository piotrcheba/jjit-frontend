import {ReactElement} from "react";

export interface OfferSectionProps {
    title: string;
    children: JSX.Element | JSX.Element[];
}