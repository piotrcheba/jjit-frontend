import {FilterStateInterface} from "../../interfaces/filter.interface";
import {FilterTechItemInterface, LocationItemInterface, SeniorityItemInterface} from "./Filterbar.interface";

export const defaultFilters: FilterStateInterface = {
    location: 'all',
    tech: 'all',
    salary: [0, 50000],
    seniority: 'all',
}

export const topLocations: LocationItemInterface[] = [
    {id: 'warszawa', displayValue: 'Warszawa'},
    {id: 'krakow', displayValue: 'Kraków'},
    {id: 'wroclaw', displayValue: 'Wrocław'},
    {id: 'poznan', displayValue: 'Poznań'},
    {id: 'trojmiasto', displayValue: 'Trójmiasto'},
]

export const otherLocations: LocationItemInterface[] = [
    {id: 'bialystok', displayValue: 'Białystok'},
    {id: 'bielsko-biala', displayValue: 'Bielsko-Biała'},
    {id: 'bydgoszcz', displayValue: 'Bydgoszcz'},
    {id: 'czestochowa', displayValue: 'Częstochowa'},
    {id: 'gliwice', displayValue: 'Gliwice'},
    {id: 'katowice', displayValue: 'Katowice'},
    {id: 'kielce', displayValue: 'Kielce'},
    {id: 'lublin', displayValue: 'Lublin'},
    {id: 'lodz', displayValue: 'Łódź'},
    {id: 'olsztyn', displayValue: 'Olsztyn'},
    {id: 'opole', displayValue: 'Opole'},
    {id: 'torun', displayValue: 'Toruń'},
    {id: 'rzeszow', displayValue: 'Rzeszów'},
    {id: 'szczecin', displayValue: 'Szczecin'},
    {id: 'zielona_gora', displayValue: 'Zielona Góra'},
];

export const seniorityItems: SeniorityItemInterface[] = [
    { id: 'all', displayValue: 'All'},
    { id: 'junior', displayValue: 'Junior'},
    { id: 'mid', displayValue: 'Mid'},
    { id: 'senior', displayValue: 'Senior'}
];

export const filterTechItems: FilterTechItemInterface[] = [
    {type: 'textBtn', id: 'all', label: 'All'},
    {type: 'filterBtn', id: 'javascript', label: 'JS'},
    {type: 'filterBtn', id: 'html', label: 'HTML'},
    {type: 'filterBtn', id: 'php', label: 'PHP'},
    {type: 'filterBtn', id: 'ruby', label: 'Ruby'},
    {type: 'filterBtn', id: 'python', label: 'Python'},
    {type: 'filterBtn', id: 'java', label: 'Java'},
    {type: 'filterBtn', id: 'net', label: '.Net'},
    {type: 'filterBtn', id: 'scala', label: 'Scala'},
    {type: 'filterBtn', id: 'c', label: 'C'},
    {type: 'filterBtn', id: 'mobile', label: 'Mobile'},
    {type: 'filterBtn', id: 'testing', label: 'Testing'},
    {type: 'filterBtn', id: 'devops', label: 'DevOps'},
    {type: 'filterBtn', id: 'ux', label: 'UX/UI'},
    {type: 'filterBtn', id: 'pm', label: 'PM'},
    {type: 'filterBtn', id: 'game', label: 'Game'},
    {type: 'filterBtn', id: 'analytics', label: 'Analytics'},
    {type: 'filterBtn', id: 'security', label: 'Security'},
    {type: 'filterBtn', id: 'data', label: 'Data'},
    {type: 'filterBtn', id: 'go', label: 'Go'},
    {type: 'filterBtn', id: 'sap', label: 'SAP'},
    {type: 'filterBtn', id: 'support', label: 'Support'},
    {type: 'filterBtn', id: 'other', label: 'Other'},
];