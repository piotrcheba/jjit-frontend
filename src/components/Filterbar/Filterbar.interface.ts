export interface FilterTechItemInterface {
    type: string;
    id: string;
    label: string;
}

export interface SeniorityItemInterface {
    id: string;
    displayValue: string;
}

export interface LocationItemInterface {
    id: string;
    displayValue: string;
}