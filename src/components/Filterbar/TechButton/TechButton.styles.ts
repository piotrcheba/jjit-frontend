import styled from "styled-components";

export const styles = {
    TextBtnWrapper: styled.div`
        display: flex;
    `,
    TextBtnInner: styled.div`
        width: 51px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        margin-left: 8px;
    `,
    IconBtnWrapper: styled.div`
        display: flex;
        min-width: 51px;
        max-width: 51px;
    `,
    IconBtnInner: styled.div`
        display: flex;
        flex-grow: 1;
        min-width: 0px;
    `,
    MenuBtn: styled.div`
        display: flex;
        justify-content: center;
        align-items: center;
        flex-basis: 35px;
        height: 35px;
    `,
    LinkWrapper: styled.div`
        display: flex;
        flex-grow: 1;
        justify-content: center;
        align-items: flex-start;
        min-width: 0px;
    `,
    IconLink: styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        min-width: 0px;
        text-decoration: none;
        border-radius: 50%;
        cursor: pointer;
    `,
    MenuLinkBtn: styled.div`
        display: flex;
        align-items: center;
        min-width: 150px;
        font-size: 14px;
        color: rgb(55, 71, 79);
        padding: 7px 14px;
        text-decoration: none;
        cursor: pointer;
    `,
    IconDiv: styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        min-width: 0px;
        text-decoration: none;
    `,
    IconWrapper: styled.div`
        display: flex;
        align-items: center;
        justify-content: center;
        box-shadow: none;
        text-transform: none;
        width: 35px;
        height: 35px;
        line-height: 35px;
        position: relative;
        background: ${(props: {id: string}) =>
            props.id === 'javascript' && 'linear-gradient(-90deg, rgb(255, 199, 6), rgb(255, 175, 0))' ||
            props.id === 'html' && 'linear-gradient(-90deg, rgb(255, 125, 80), rgb(253, 93, 33))' ||
            props.id === 'php' && 'linear-gradient(-90deg, rgb(65, 173, 250), rgb(21, 124, 252))' ||
            props.id === 'ruby' && 'linear-gradient(-90deg, rgb(239, 83, 80), rgb(244, 67, 54))' ||
            props.id === 'python' && 'linear-gradient(-90deg, rgb(31, 94, 182), rgb(31, 123, 196))' ||
            props.id === 'java' && 'linear-gradient(-90deg, rgb(250, 101, 107), rgb(249, 89, 122))' ||
            props.id === 'net' && 'linear-gradient(-90deg, rgb(103, 37, 114), rgb(150, 70, 163))' ||
            props.id === 'scala' && 'linear-gradient(-90deg, rgb(248, 100, 104), rgb(241, 70, 76))' ||
            props.id === 'c' && 'linear-gradient(-90deg, rgb(47, 207, 187), rgb(55, 221, 156))' ||
            props.id === 'mobile' && 'linear-gradient(-90deg, rgb(224, 79, 134), rgb(186, 74, 141))' ||
            props.id === 'testing' && 'linear-gradient(-90deg, rgb(0, 150, 136), rgb(0, 121, 107))' ||
            props.id === 'devops' && 'linear-gradient(-90deg, rgb(82, 102, 225), rgb(129, 102, 224))' ||
            props.id === 'ux' && 'linear-gradient(-90deg, rgb(255, 183, 77), rgb(255, 152, 0))' ||
            props.id === 'pm' && 'linear-gradient(-90deg, rgb(128, 203, 196), rgb(77, 182, 172))' ||
            props.id === 'game' && 'linear-gradient(-90deg, rgb(255, 64, 129), rgb(236, 64, 122))' ||
            props.id === 'analytics' && 'linear-gradient(-90deg, rgb(59, 89, 152), rgb(112, 140, 199))' ||
            props.id === 'security' && 'linear-gradient(-90deg, rgb(83, 109, 254), rgb(13, 71, 161))' ||
            props.id === 'data' && 'linear-gradient(-90deg, rgb(137, 219, 84), rgb(101, 175, 53))' ||
            props.id === 'go' && 'linear-gradient(-90deg, rgb(106, 215, 229), rgb(106, 139, 229))' ||
            props.id === 'sap' && 'linear-gradient(-90deg, rgb(2, 175, 235), rgb(27, 104, 194))' ||
            props.id === 'support' && 'linear-gradient(-90deg, rgb(212, 104, 222), rgb(82, 77, 225))' ||
            props.id === 'other' && 'linear-gradient(-90deg, rgb(236, 76, 182), rgb(212, 75, 213))'
        };
        border-radius: 30px;
        transition: all 300ms ease 0s;
        svg {
            fill: white;
            width: ${(props: {id: string}) =>
                props.id === 'javascript' && '18px' ||
                props.id === 'html' && '19px' ||
                props.id === 'php' && '26px' ||
                props.id === 'ruby' && '23px' ||
                props.id === 'python' && '20px' ||
                props.id === 'java' && '20px' ||
                props.id === 'net' && '25px' ||
                props.id === 'scala' && '15px' ||
                props.id === 'c' && '22px' ||
                props.id === 'mobile' && '14px' ||
                props.id === 'testing' && '20px' ||
                props.id === 'devops' && '22px' ||
                props.id === 'ux' && '19px' ||
                props.id === 'pm' && '18px' ||
                props.id === 'game' && '25px' ||
                props.id === 'analytics' && '23px' ||
                props.id === 'security' && '26px' ||
                props.id === 'data' && '16px' ||
                props.id === 'go' && '24px' ||
                props.id === 'sap' && '24px' ||
                props.id === 'support' && '24px' ||
                props.id === 'other' && '23px'
            };
        };
        &&.disabled {
            background: linear-gradient(to right, #9193AA, #7D82A8);
        }
        &:hover {
            box-shadow: rgb(228, 232, 240) 0px 0px 0px 5px;
            color: white;
            fill: white;
        }
    `,
    BtnLabel: styled.span`
        color: rgb(117, 117, 117);
        font-size: 11px;
        line-height: 15px;
        text-align: center;
        margin-top: 3px;
    `,
    MenuBtnLabel: styled.div`
        margin-left: 10px;
    `,
    PopperItem: styled.div`
        display: flex;
        align-items: center;
        flex-flow: row nowrap;
    `,
    PopperIcon: styled.div`
        display: flex;
        align-items: center;
        justify-content: center;
        height: 30px;
        width: 30px;
        margin-right: 10px;
        background-color: rgb(191, 197, 210);
        border-radius: 30px;
    `,
    PopperLabel: styled.div`
        display: flex;
        justify-content: flex-start;
        align-items: flex-start;
        flex-flow: column nowrap;
    `,
    PopperLabelSpan: styled.span`
        font-weight: 400;
    `
}

export const muiStyles = {
    textBtn: {
        textTransform: 'none',
        width: '35px',
        minWidth: '35px',
        height: '35px',
        lineHeight: '35px',
        color: 'white',
        borderRadius: '50%',
        padding: '0px',
        background: 'linear-gradient(-90deg, rgb(186, 104, 200), rgb(156, 39, 176))',
        '&.disabled': {
            background: 'linear-gradient(to right, #9193AA, #7D82A8)',
        },
        '&:hover': {
            boxShadow: 'rgb(228, 232, 240) 0px 0px 0px 5px',
            color: 'white',
            fill: 'white',
        }
    },
    menuBtn: {
        color: 'rgb(55, 71, 79)',
    },
    chip: {
        fontSize: '8px',
        height: '16px',
        borderRadius: '4px',
    }
}