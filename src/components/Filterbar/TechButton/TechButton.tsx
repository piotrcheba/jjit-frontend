import React, {ReactElement} from 'react';
import {styles, muiStyles} from './TechButton.styles';
import Icon from "../../Icon/Icon";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton"
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import {makeStyles} from "@material-ui/core/styles";
import {TechButtonProps} from "./TechButton.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function TechButton({type, label, id, clicked, className, onClick}: TechButtonProps): ReactElement | null {
    const classes = useStyles();

    switch (type) {
        case ('textBtn'):
            return (
                <styles.TextBtnWrapper onClick={onClick}>
                    <styles.TextBtnInner>
                        <Button
                            className={[classes.textBtn, className].join(' ')}
                        >
                            {label}
                        </Button>
                        <styles.BtnLabel>
                            {label}
                        </styles.BtnLabel>
                    </styles.TextBtnInner>
                </styles.TextBtnWrapper>
            );
        case ('filterBtn'):
            return (
                <styles.IconBtnWrapper onClick={onClick}>
                    <styles.IconBtnInner>
                        <styles.LinkWrapper>
                            <styles.IconLink>
                                <styles.IconWrapper id={id!} className={className!}>
                                    <Icon type={id!}/>
                                </styles.IconWrapper>
                                <styles.BtnLabel>
                                    {label}
                                </styles.BtnLabel>
                            </styles.IconLink>
                        </styles.LinkWrapper>
                    </styles.IconBtnInner>
                </styles.IconBtnWrapper>
            );
        case ('menuBtn'):
            return (
                <styles.MenuLinkBtn>
                    <styles.IconWrapper id={id!} className={className!}>
                        <Icon type={id!}/>
                    </styles.IconWrapper>
                    <styles.MenuBtnLabel>
                        {label}
                    </styles.MenuBtnLabel>
                </styles.MenuLinkBtn>
            )
        case ('menuIcon'):
            return (
                <styles.IconBtnWrapper data-testid='filterbar-menubtn'>
                    <styles.MenuBtn>
                        <IconButton onClick={clicked} className={classes.menuBtn} size="small">
                            <MoreHorizIcon/>
                        </IconButton>
                    </styles.MenuBtn>
                </styles.IconBtnWrapper>
            )
        case ('form'):
            return (
                <styles.IconBtnWrapper onClick={onClick}>
                    <styles.IconBtnInner>
                        <styles.LinkWrapper>
                            <styles.IconDiv>
                                <styles.IconWrapper id={id!} className={className!}>
                                    <Icon type={id!}/>
                                </styles.IconWrapper>
                                <styles.BtnLabel>
                                    {label}
                                </styles.BtnLabel>
                            </styles.IconDiv>
                        </styles.LinkWrapper>
                    </styles.IconBtnInner>
                </styles.IconBtnWrapper>
            );
        default:
            return null;
    }
};