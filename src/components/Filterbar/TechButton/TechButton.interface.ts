import React from "react";

export interface TechButtonProps {
    type: string;
    label?: string;
    id?: string;
    className?: string | null;
    clicked?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onClick?: () => void;
}