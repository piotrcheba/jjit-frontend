import React, {ReactElement, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {selectFilter} from "../../redux/actions/filterActions";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import TuneIcon from '@material-ui/icons/Tune';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import TechButton from './TechButton/TechButton';
import {styles, muiStyles} from './Filterbar.styles';
import {filterTechItems} from './Filterbar.constants';
import LocationPopover from "./LocationPopover/LocationPopover";
import MoreFiltersDialog from "./MoreFiltersDialog/MoreFiltersDialog";
import {RootState} from "../../redux/store";
import {FilterStateInterface} from "../../interfaces/filter.interface";
import {FilterTechItemInterface} from "./Filterbar.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function Filterbar(): ReactElement {
    const filter = useSelector((state: RootState): FilterStateInterface => state.filter);

    const dispatch = useDispatch();

    const classes = useStyles();

    const [showPopover, togglePopover] = useState(false);
    const [showDialog, toggleDialog] = useState(false);
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
    const [displayFilter, setDisplayFilter] = useState('');

    const closePopoverHandler = (): void => togglePopover(false);

    const closeDialogHandler = (): void => toggleDialog(false);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (): void => {
        setAnchorEl(null);
    }

    return(
        <>
            <styles.StyledWrapper>
                <styles.BaseFilter>
                    <styles.BaseFilter>
                        <styles.Buttons>
                            <Button
                                data-testid='filterbar-locpopbtn'
                                variant='outlined'
                                className={filter.location === 'all' ? classes.locationBtn : classes.locationBtnActive}
                                endIcon={<ExpandMoreIcon className={showPopover ? classes.animatedIconEnd : classes.animatedIconStart}/>}
                                onClick={(): void => togglePopover(true)}
                            >
                                {filter.location === 'all' ? 'Location' : displayFilter}
                            </Button>
                            <styles.Techbar>
                                {filterTechItems.map((item: FilterTechItemInterface): ReactElement => (
                                    <TechButton
                                        key={item.id}
                                        {...item}
                                        className={filter.tech !== 'all' && filter.tech !== item.id ? 'disabled' : null}
                                        onClick={(): void => {
                                            dispatch(selectFilter('tech', item.id));
                                        }}
                                    />
                                ))}
                                <TechButton
                                    type="menuIcon"
                                    clicked={handleClick}
                                />
                            </styles.Techbar>
                        </styles.Buttons>
                        <styles.MoreFilters>
                            <Button
                                data-testid='filterbar-morebtn'
                                variant='outlined'
                                className={classes.moreBtn}
                                startIcon={<TuneIcon/>}
                                endIcon={<ExpandMoreIcon className={showDialog ? classes.animatedIconEnd : classes.animatedIconStart}/>}
                                onClick={(): void => toggleDialog(true)}
                            >
                                More filters
                            </Button>
                        </styles.MoreFilters>
                    </styles.BaseFilter>
                </styles.BaseFilter>
            </styles.StyledWrapper>
            <LocationPopover
                showPopover={showPopover}
                closePopover={closePopoverHandler}
                setDisplayFilter={setDisplayFilter}
            />
            <MoreFiltersDialog
                showDialog={showDialog}
                closeDialog={closeDialogHandler}
            />
            <Menu
                data-testid='filterbar-menu'
                anchorEl={anchorEl}
                anchorOrigin={{vertical: "bottom", horizontal: "center"}}
                transformOrigin={{vertical: "top", horizontal: "center"}}
                open={Boolean(anchorEl)}
                onClose={handleClose}
                className={classes.menu}
            >
                {filterTechItems.slice(8, 23).map((item: FilterTechItemInterface): ReactElement => (
                    <MenuItem
                        key={item.id}
                        onClick={(): void => {
                            dispatch(selectFilter('tech', item.id))
                            handleClose();
                        }}
                        className={classes.menuItem}
                    >
                        <TechButton
                            type="menuBtn"
                            id={item.id}
                            label={item.label}
                            className={filter.tech !== 'all' && filter.tech !== item.id ? 'disabled' : undefined}
                        />
                    </MenuItem>
                ))}
            </Menu>
        </>
    );
};