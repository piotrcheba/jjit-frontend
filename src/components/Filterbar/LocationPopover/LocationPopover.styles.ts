import styled from "styled-components";

export const styles = {
    LocPopoverWrapper: styled.div`
        max-width: 574px;
        padding: 24px;
    `,
    LocPopoverInner: styled.div`
        position: relative;
    `,
    SectionWrapper: styled.div`
        display: flex;
        flex-wrap: wrap;
        margin: -6px;
    `,
    SectionTitle: styled.div`
        font-size: 16px;
        font-weight: 600;
        color: rgb(119, 119, 119);
        margin: 24px 0px 12px;
    `,
    ClearBtnWrapper: styled.div`
        display: flex;
        justify-content: space-between;
    `,
}

export const muiStyles = {
    icon: {
        fill: 'rgb(153, 161, 171)',
    },
    iconBtn: {
        width: '36px',
        height: '36px',
        position: 'absolute',
        top: '6px',
        right: '0px',
        padding: '0px',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(153, 161, 171)',
        borderImage: 'initial',
    },
    popoverBtn: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        borderRadius: '18px',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
        margin: '6px',
        '&:hover': {
            backgroundColor: 'rgb(245, 245, 245)',
        },
    },
    popoverBtnActive: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(255, 64, 129)',
        borderRadius: '18px',
        background: 'rgba(255, 64, 129, 0.08)',
        borderColor: 'rgb(255, 64, 129)',
        margin: '6px',
        '&:hover': {
            backgroundColor: 'initial',
        },
    },
    locDivider: {
        margin: '20px 0px',
    },
    clearBtn: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        borderRadius: '18px',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
    },
}