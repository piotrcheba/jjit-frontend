export interface LocationPopoverProps {
    showPopover: boolean;
    closePopover: () => void;
    setDisplayFilter: React.Dispatch<React.SetStateAction<string>>;
}