import React, {ReactElement} from 'react';
import {styles, muiStyles} from "./LocationPopover.styles";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Button from "@material-ui/core/Button";
import {clearFilter, selectFilter} from "../../../redux/actions/filterActions";
import {otherLocations, topLocations} from "../Filterbar.constants";
import Divider from "@material-ui/core/Divider";
import Popover from "@material-ui/core/Popover";
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {LocationPopoverProps} from "./LocationPopover.interface";
import {RootState} from "../../../redux/store";
import {FilterStateInterface} from "../../../interfaces/filter.interface";
import {LocationItemInterface} from "../Filterbar.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function LocationPopover({showPopover, closePopover, setDisplayFilter}: LocationPopoverProps): ReactElement {
    const filter = useSelector((state: RootState): FilterStateInterface => state.filter);

    const dispatch = useDispatch();

    const classes = useStyles();

    return (
        <Popover
            data-testid='locationpopover-popover'
            open={showPopover}
            onClose={closePopover}
            anchorReference="anchorPosition"
            anchorPosition={{ top: 117, left: 16 }}
            transformOrigin={{ vertical: 'top', horizontal: 'left'}}
        >
            <styles.LocPopoverWrapper>
                <styles.LocPopoverInner>
                    <IconButton
                        className={classes.iconBtn}
                        onClick={closePopover}
                    >
                        <CloseIcon className={classes.icon}/>
                    </IconButton>
                    <div>
                        <styles.SectionWrapper>
                            <Button
                                variant='outlined'
                                className={classes.popoverBtn}
                                onClick={(): void => {
                                    dispatch(selectFilter('location', 'all'));
                                    setDisplayFilter('Remote');
                                    setTimeout(closePopover, 200);
                                }}
                            >
                                Remote
                            </Button>
                        </styles.SectionWrapper>
                    </div>
                    <div>
                        <styles.SectionTitle>
                            Top Locations
                        </styles.SectionTitle>
                        <styles.SectionWrapper>
                            {topLocations.map((item: LocationItemInterface): ReactElement => (
                                <Button
                                    key={item.id}
                                    variant='outlined'
                                    className={filter.location === item.id ? classes.popoverBtnActive : classes.popoverBtn}
                                    onClick={(): void => {
                                        dispatch(selectFilter('location', item.id));
                                        setDisplayFilter(item.displayValue);
                                        setTimeout(closePopover, 200);
                                    }}
                                >
                                    {item.displayValue}
                                </Button>
                            ))}
                        </styles.SectionWrapper>
                    </div>
                    <div>
                        <styles.SectionTitle>
                            Other Locations
                        </styles.SectionTitle>
                        <styles.SectionWrapper>
                            {otherLocations.map((item: LocationItemInterface): ReactElement => (
                                <Button
                                    key={item.id}
                                    variant='outlined'
                                    className={filter.location === item.id ? classes.popoverBtnActive : classes.popoverBtn}
                                    onClick={(): void => {
                                        dispatch(selectFilter('location', item.id))
                                        setDisplayFilter(item.displayValue);
                                        setTimeout(closePopover, 200);
                                    }}
                                >
                                    {item.displayValue}
                                </Button>
                            ))}
                        </styles.SectionWrapper>
                    </div>
                    <Divider
                        orientation='horizontal'
                        className={classes.locDivider}
                    />
                    <styles.ClearBtnWrapper>
                        <Button
                            variant='outlined'
                            className={classes.clearBtn}
                            onClick={(): void => {
                                dispatch(clearFilter(['location']))
                                setTimeout(closePopover, 200);
                            }}
                        >
                            Clear filter
                        </Button>
                    </styles.ClearBtnWrapper>
                </styles.LocPopoverInner>
            </styles.LocPopoverWrapper>
        </Popover>
    )
}