import styled from "styled-components";

export const styles = {
    StyledWrapper: styled.div`
        background-color: white;
        display: flex;
        min-height: 68px;
        flex-direction: column;
        justify-content: center;
        padding: 14px 15px 10px;
    `,
    BaseFilter: styled.div`
        display: flex;
        opacity: 1;
        pointer-events: initial;
        padding-left: 4px;
        align-items: flex-start;
        justify-content: space-between;
        z-index: unset;
        transition: opacity 300ms ease 0s;
        overflow: hidden;
        flex: 1 1 0%;
        flex-flow: row nowrap;
    `,
    Buttons: styled.div`
        display: flex;
        opacity: 1;
        pointer-events: initial;
        padding-left: 4px;
        align-items: flex-start;
        justify-content: space-between;
        z-index: unset;
        transition: opacity 300ms ease 0s;
        overflow: hidden;
        flex: 1 1 0%;
        flex-flow: row nowrap;
    `,
    Search: styled.div`
        display: flex;
        align-items: center;
        width: 100%;
        flex-flow: row nowrap;
    `,
    Techbar: styled.div`
        display: flex;
        flex-grow: 1;
        flex-wrap: nowrap;
        align-items: stretch;
        overflow: hidden;
        padding: 6px 0px;
        @media (max-width: 1036px) {
            div:nth-child(n+9):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1087px) {
            div:nth-child(n+10):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1138px) {
            div:nth-child(n+11):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1189px) {
            div:nth-child(n+12):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1240px) {
            div:nth-child(n+13):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1291px) {
            div:nth-child(n+14):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1342px) {
            div:nth-child(n+15):nth-child(-n+21) {
                display: none;
            }
        }
        @media (max-width: 1393px) {
            div:nth-child(n+16):nth-child(-n+21) {
                display: none;
            }
        }
        @media (max-width: 1444px) {
            div:nth-child(n+17):nth-child(-n+21) {
                display: none;
            }
        }
        @media (max-width: 1495px) {
            div:nth-child(n+18):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1546px) {
            div:nth-child(n+19):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1597px) {
            div:nth-child(n+20):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1648px) {
            div:nth-child(n+21):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1699px) {
            div:nth-child(n+22):nth-child(-n+23) {
                display: none;
            }
        }
        @media (max-width: 1750px) {
            div:nth-child(23) {
                display: none;
            }
        }
        @media (min-width: 1751px) {
            div:nth-child(24) {
                display: none;
            }
        }
    `,
    MoreFilters: styled.div`
        display: flex;
        align-items: flex-start;
        margin-left: auto;
        margin-top: 6px;
    `,
}

export const muiStyles = {
    locationBtn: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        minWidth: '148px',
        justifyContent: 'space-between',
        marginTop: '6px',
        borderRadius: '18px',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
    },
    locationBtnActive: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(255, 64, 129)',
        minWidth: '148px',
        justifyContent: 'space-between',
        marginTop: '6px',
        borderRadius: '18px',
        background: 'rgba(255, 64, 129, 0.08)',
        borderColor: 'rgb(255, 64, 129)',
        '&:hover': {
            backgroundColor: 'initial',
        },
    },
    moreBtn: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        marginLeft: '10px',
        whiteSpace: 'nowrap',
        borderRadius: '18px',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
    },
    moreBtnActive: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        marginLeft: '10px',
        whiteSpace: 'nowrap',
        borderRadius: '18px',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
    },
    icon: {
        fill: 'rgb(153, 161, 171)',
    },
    animatedIconStart: {
        transform: 'rotate(0deg)',
        transition: 'transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
    },
    animatedIconEnd: {
        transform: 'rotate(180deg)',
        transition: 'transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
    },

    // dialogShowBtn: {
    //     textTransform: 'none',
    //     fontWeight: 600,
    //     color: 'white',
    //     whiteSpace: 'nowrap',
    //     textOverflow: 'ellipsis',
    //     background: 'rgb(255, 64, 129)',
    //     borderColor: 'rgb(255, 64, 129)',
    //     padding: '7px 28px',
    //     borderRadius: '20px',
    //     overflow: 'hidden',
    //     '&:hover': {
    //         background: 'rgb(178, 44, 90)',
    //         borderColor: 'rgb(178, 44, 90)',
    //     }
    // },
    tag: {
        height: '30px',
        backgroundColor: 'white',
        display: 'flex',
        alignItems: 'center',
        fontFamily: 'Open Sans',
        fontSize: '12px',
        fontWeight: 600,
        marginLeft: '5px',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(228, 232, 240)',
        borderImage: 'initial',
        borderRadius: '15px',
        flexFlow: 'row nowrap',
        padding: '0 10px',
        '& .MuiChip-root': {
            fontSize: '8px',
            height: '16px',
            borderRadius: '4px',
        },
        '& .MuiButtonBase-root': {
            marginLeft: '5px',
        }
    },
    search: {
        backgroundColor: 'rgb(243, 246, 248)',
        boxShadow: 'none',
        padding: '3px 10px',
        borderWidth: 'initial',
        borderStyle: 'none',
        borderColor: 'initial',
        borderImage: 'initial',
        transition: 'background-color 300ms ease 0s',
        borderRadius: '20px',
        '&:hover': {
            backgroundColor: 'rgb(228, 232, 240)',
        },
        '& input': {
            paddingLeft: '5px',
            width: '0',
            minWidth: '30px',
        }
    },
    searchIcon: {
        '& .disabled': {
            color: 'rgb(117, 117, 117)'
        }
    },
    menu: {
        '& li': {
            '@media (min-width: 1037px)': {
                '&:nth-child(1)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1088px)': {
                '&:nth-child(-n+2)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1139px)': {
                '&:nth-child(-n+3)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1190px)': {
                '&:nth-child(-n+4)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1241px)': {
                '&:nth-child(-n+5)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1292px)': {
                '&:nth-child(-n+6)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1343px)': {
                '&:nth-child(-n+7)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1394px)': {
                '&:nth-child(-n+8)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1445px)': {
                '&:nth-child(-n+9)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1496px)': {
                '&:nth-child(-n+10)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1547px)': {
                '&:nth-child(-n+11)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1598px)': {
                '&:nth-child(-n+12)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1649px)': {
                '&:nth-child(-n+13)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1700px)': {
                '&:nth-child(-n+14)': {
                    display: 'none',
                },
            },
            '@media (min-width: 1751px)': {
                '&:nth-child(-n+15)': {
                    display: 'none',
                },
            },
        },
    },
    menuItem: {
        padding: '0',
    },
    popper: {
        '& .MuiAutocomplete-paper': {
            backgroundColor: 'rgb(255, 255, 255)',
            boxShadow: 'rgba(32, 33, 36, 0.28) 0px 3px 3px 0px',
            margin: '0px',
            borderRadius: '0px 0px 20px 20px',
            borderTop: 'none rgb(228, 232, 240)',
            borderColor: 'rgb(228, 232, 240)',
            '& ::-webkit-scrollbar': {
                width: '8px',
                padding: '2px'
            },
            '& ::-webkit-scrollbar-thumb': {
                height: '50px',
                background: 'rgb(193, 193, 193)',
                borderRadius: '10px',
                padding: '2px',
            },
            '& ::-webkit-scrollbar-track': {
                width: '8px',
                borderRadius: '4px',
                padding: '2px',
                background: 'rgb(255, 255, 255)',
            }
        }
    },
    tagBtn: {
        marginLeft: '5px',
    }
}