import React, {ReactElement} from 'react';
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogContent from "@material-ui/core/DialogContent";
import Slider from "@material-ui/core/Slider";
import Divider from "@material-ui/core/Divider";
import {seniorityItems} from "../Filterbar.constants";
import Button from "@material-ui/core/Button";
import {clearFilter, selectFilter} from "../../../redux/actions/filterActions";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import {styles, muiStyles} from './MoreFiltersDialog.styles';
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {MoreFiltersDialogProps} from "./MoreFiltersDialog.interface";
import {SeniorityItemInterface} from "../Filterbar.interface";
import {RootState} from "../../../redux/store";
import {FilterStateInterface} from "../../../interfaces/filter.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function MoreFiltersDialog({showDialog, closeDialog}: MoreFiltersDialogProps): ReactElement {
    const filter = useSelector((state: RootState): FilterStateInterface => state.filter);

    const dispatch = useDispatch();

    const classes = useStyles();

    const salaryChangedHandler = (event: React.ChangeEvent<{}>, newValue: number | number[]): void => {
        dispatch(selectFilter('salary', newValue));
    }

    return (
        <Dialog
            data-testid='morefiltersdialog-dialog'
            open={showDialog}
            onBackdropClick={closeDialog}
            fullWidth={true}
        >
            <DialogTitle className={classes.dialogTitle} disableTypography={true}>
                <styles.StyledDialogTitle>
                    More filters
                </styles.StyledDialogTitle>
                <IconButton
                    className={classes.dialogBtn}
                    onClick={closeDialog}
                    edge='end'
                >
                    <CloseIcon className={classes.icon}/>
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <styles.DialogContentTitle>
                    Salary expectations?
                </styles.DialogContentTitle>
                <div>
                    <Slider
                        color='secondary'
                        value={filter.salary}
                        onChange={salaryChangedHandler}
                        min={0}
                        max={50000}
                        step={1000}
                    />
                    <styles.SliderLabels>
                        <styles.SliderDesc>
                            <styles.SliderTitle>
                                Min. amount
                            </styles.SliderTitle>
                            <styles.SliderAmount>
                                {filter.salary[0]} PLN
                            </styles.SliderAmount>
                        </styles.SliderDesc>
                        <styles.SliderRange>—</styles.SliderRange>
                        <styles.SliderDesc>
                            <styles.SliderTitle>
                                Max. amount
                            </styles.SliderTitle>
                            <styles.SliderAmount>
                                {filter.salary[1]} PLN
                            </styles.SliderAmount>
                        </styles.SliderDesc>
                    </styles.SliderLabels>
                </div>
                <Divider
                    orientation='horizontal'
                    className={classes.moreDivider}
                />
                <styles.DialogContentTitle>
                    Seniority
                </styles.DialogContentTitle>
                <div>
                    <styles.SeniorityControls>
                        {seniorityItems.map((item: SeniorityItemInterface): ReactElement => (
                            <Button
                                key={item.id}
                                variant='outlined'
                                className={filter.seniority === item.id ? classes.seniorityBtnActive : classes.seniorityBtn}
                                onClick={(): void => {
                                    dispatch(selectFilter('seniority', item.id))
                                }}
                            >
                                {item.displayValue}
                            </Button>
                        ))}
                    </styles.SeniorityControls>
                </div>
                <Divider
                    orientation='horizontal'
                    className={classes.moreDivider}
                />
            </DialogContent>
            <DialogActions className={classes.dialogActions}>
                <Button
                    variant='outlined'
                    className={classes.dialogClearBtn}
                    onClick={(): void => {
                        dispatch(clearFilter(['salary', 'seniority']));
                        setTimeout(closeDialog, 200);
                    }}
                >
                    Clear filters
                </Button>
            </DialogActions>
        </Dialog>
    )
}