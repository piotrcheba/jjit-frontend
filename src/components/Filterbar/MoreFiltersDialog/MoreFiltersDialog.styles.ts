import styled from "styled-components";

export const styles = {
    StyledDialogTitle: styled.div`
        font-size: 18px;
        color: rgb(119, 119, 119);
    `,
    DialogContentTitle: styled.div`
        margin-bottom: 16px;
        font-size: 16px;
        line-height: 22px;
        font-weight: 700;
        color: rgb(119, 119, 119);
    `,
    SliderLabels: styled.div`
        display: flex;
        margin-top: 4px;
    `,
    SliderDesc: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        height: 48px;
        flex: 1 1 0%;
        padding: 2px 24px 0px;
        border-radius: 24px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(228, 232, 240);
        border-image: initial;
    `,
    SliderTitle: styled.div`
        font-size: 12px;
        line-height: 16px;
        font-weight: 600;
        color: rgb(119, 119, 119);
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `,
    SliderAmount: styled.div`
        font-size: 14px;
        line-height: 16px;
        color: rgb(119, 119, 119);
    `,
    SliderRange: styled.div`
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 6px;
    `,
    SeniorityControls: styled.div`
        margin: -6px;
    `,
}

export const muiStyles = {
    icon: {
        fill: 'rgb(153, 161, 171)',
    },
    dialogTitle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    dialogBtn: {
        width: '36px',
        height: '36px',
        padding: '0px',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(153, 161, 171)',
        borderImage: 'initial',
    },
    moreDivider: {
        margin: '24px 0px 0px',
        '&:not(:last-of-type)': {
            marginBottom: '24px',
        }
    },
    seniorityBtn: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        borderRadius: '18px',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
        margin: '6px',
        '@media (min-width: 1025px)': {
            width: '112px',
        }
    },
    seniorityBtnActive: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(255, 64, 129)',
        borderRadius: '18px',
        background: 'rgba(255, 64, 129, 0.08)',
        borderColor: 'rgb(255, 64, 129)',
        margin: '6px',
        '@media (min-width: 1025px)': {
            width: '112px',
        },
    },
    dialogActions: {
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '16px 24px 24px',
    },
    dialogClearBtn: {
        textTransform: 'none',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        background: 'white',
        borderColor: 'rgb(228, 232, 240)',
        padding: '7px 28px',
        borderRadius: '20px',
        overflow: 'hidden',
    },
}