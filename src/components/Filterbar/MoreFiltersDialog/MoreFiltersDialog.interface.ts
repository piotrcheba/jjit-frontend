export interface MoreFiltersDialogProps {
    showDialog: boolean;
    closeDialog: () => void;
}