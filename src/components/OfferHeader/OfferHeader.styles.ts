import styled from "styled-components";
import bg from "../../assets/header_bg.png";

export const styles = {
    Wrapper: styled.div`
        height: 235px;
        position: relative;
        margin: 0 0 80px 0;
    `,
    Background: styled.div`
        background: url(${bg}) center center no-repeat, ${(props: {color: string}) => 
            props.color === 'javascript' && 'linear-gradient(30deg, rgb(255, 199, 6), rgb(255, 175, 0))' ||
            props.color === 'html' && 'linear-gradient(30deg, rgb(255, 125, 80), rgb(253, 93, 33))' ||
            props.color === 'php' && 'linear-gradient(30deg, rgb(65, 173, 250), rgb(21, 124, 252))' ||
            props.color === 'ruby' && 'linear-gradient(30deg, rgb(239, 83, 80), rgb(244, 67, 54))' ||
            props.color === 'python' && 'linear-gradient(30deg, rgb(31, 94, 182), rgb(31, 123, 196))' ||
            props.color === 'java' && 'linear-gradient(30deg, rgb(250, 101, 107), rgb(249, 89, 122))' ||
            props.color === 'net' && 'linear-gradient(30deg, rgb(103, 37, 114), rgb(150, 70, 163))' ||
            props.color === 'scala' && 'linear-gradient(30deg, rgb(248, 100, 104), rgb(241, 70, 76))' ||
            props.color === 'c' && 'linear-gradient(30deg, rgb(47, 207, 187), rgb(55, 221, 156))' ||
            props.color === 'mobile' && 'linear-gradient(30deg, rgb(224, 79, 134), rgb(186, 74, 141))' ||
            props.color === 'testing' && 'linear-gradient(30deg, rgb(0, 150, 136), rgb(0, 121, 107))' ||
            props.color === 'devops' && 'linear-gradient(30deg, rgb(82, 102, 225), rgb(129, 102, 224))' ||
            props.color === 'ux' && 'linear-gradient(30deg, rgb(255, 183, 77), rgb(255, 152, 0))' ||
            props.color === 'pm' && 'linear-gradient(30deg, rgb(128, 203, 196), rgb(77, 182, 172))' ||
            props.color === 'game' && 'linear-gradient(30deg, rgb(255, 64, 129), rgb(236, 64, 122))' ||
            props.color === 'analytics' && 'linear-gradient(30deg, rgb(59, 89, 152), rgb(112, 140, 199))' ||
            props.color === 'security' && 'linear-gradient(30deg, rgb(83, 109, 254), rgb(13, 71, 161))' ||
            props.color === 'data' && 'linear-gradient(30deg, rgb(137, 219, 84), rgb(101, 175, 53))' ||
            props.color === 'go' && 'linear-gradient(30deg, rgb(106, 215, 229), rgb(106, 139, 229))' ||
            props.color === 'sap' && 'linear-gradient(30deg, rgb(2, 175, 235), rgb(27, 104, 194))' ||
            props.color === 'support' && 'linear-gradient(30deg, rgb(212, 104, 222), rgb(82, 77, 225))' ||
            props.color === 'other' && 'linear-gradient(30deg, rgb(236, 76, 182), rgb(212, 75, 213))'};
        background-size: cover;
        height: 100%;
        border-radius: 0 0 4px 4px;
        padding: 20px 20px 0 20px;
    `,
    HeaderRow: styled.div`
        display: flex;
        align-items: center;
        color: #fff;
    `,
    WhiteCircle: styled.div`
        flex-shrink: 0;
        width: 147px;
        height: 147px;
        background-color: rgba(255, 255, 255, 0.3);
        background-clip: padding-box;
        position: relative;
        border-radius: 50%;
        border: 10px solid rgba(255, 255, 255, 0.15);
    `,
    LogoContainer: styled.div`
        width: 107px;
        height: 107px;
        border-radius: 50%;
        background-color: #fff;
        transform: translate(-50%, -50%);
        position: absolute;
        top: 50%;
        left: 50%;
    `,
    Logo: styled.img`
        max-width: 75px;
        max-height: 70px;
        transform: translate(-50%, -50%);
        position: absolute;
        top: 50%;
        left: 50%;
    `,
    EssentialInfo: styled.div`
        margin: 0 0 0 25px;
    `,
    SalaryRow: styled.div`
        font-size: 18px;
        line-height: 24px;
    `,
    TitleWrapper: styled.div`
        display: flex;
        align-items: center;
        flex-wrap: wrap;
    `,
    Title: styled.span`
        font-size: 22px;
        font-weight: 600;
        margin-right: 8px;
    `,
    OnlineInterview: styled.span`
        display: block;
        text-align: center;
        font-size: 10px;
        line-height: 14px;
        background-color: rgb(225, 245, 254);
        color: rgb(66, 165, 245);
        margin: 0px;
        padding: 4px 12px;
        border-radius: 4px;
        @media (min-width: 1025px) {
            display: inline-block;
            height: 21px;
            line-height: 21px;
            white-space: nowrap;
            padding: 0px 12px;
        }
    `,
    Remote: styled.span`
        display: inline-block;
        height: 21px;
        font-size: 10px;
        line-height: 21px;
        white-space: nowrap;
        background-color: rgb(255, 245, 248);
        color: rgb(255, 64, 129);
        margin: 0px;
        padding: 0px 12px;
        border-radius: 11px;
    `,
    Address: styled.div`
        font-size: 18px;
    `,
    Tiles: styled.div`
        display: flex;
        justify-content: unset;
        padding: 0 20px;
        width: 100%;
        left: 0;
        bottom: 0;
        position: absolute;
        transform: translateY(50%);
        z-index: 10;
    `,
}

export const muiStyles = {
    iconBtn: {
        position: 'absolute',
        color: 'rgb(255, 255, 255)',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        width: '28px',
        height: '28px',
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: '10px',
        left: '10px',
        padding: '2px',
        borderRadius: '4px',
        transition: 'all 300ms ease-out 0s',
        '&:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.35)',
        }
    },
}