import React, {ReactElement} from 'react';
import {styles, muiStyles} from './OfferHeader.styles';
import Tile from "./Tile/Tile";
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from 'react-router-dom';
import {useSelector} from "react-redux";
import {OfferHeaderProps} from "./OfferHeader.interface";
import {RootState} from "../../redux/store";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function OfferHeader({headerCtrls}: OfferHeaderProps): ReactElement {
    const activeOffer = useSelector((state: RootState) => state.offers.activeOffer);

    const classes = useStyles();

    let history = useHistory();

    const backBtn: ReactElement = (
        <IconButton
            data-testid='offerheader-backbtn'
            className={classes.iconBtn}
            onClick={() => history.goBack()}
        >
            <ArrowBackIcon/>
        </IconButton>
    );

    const offerAdded = () => {
        const dateAdded = Date.parse(activeOffer?.date!)

        const diffInDays = (Date.now() - dateAdded) / 86400000

        if (diffInDays < 1) {
            return 'New'
        } else {
            return `${Math.floor(diffInDays)}d ago`
        }
    }

    const tiles = [
        {type: 'companyName', title: 'Company name', link: activeOffer?.companyWebsite, value: activeOffer?.companyName },
        {type: 'companySize', title: 'Company size', value: activeOffer?.companySize },
        {type: 'empType', title: 'EMP. type', value: activeOffer?.empType },
        {type: 'expLvl', title: 'EXP. lvl', value: activeOffer?.expLevel },
        {type: 'dateAdded', title: 'Added', value: headerCtrls ? offerAdded() : 'New' },
    ];

    return (
        <styles.Wrapper>
            <styles.Background color={activeOffer?.mainTech!}>
                {headerCtrls ? backBtn : null}
                <styles.HeaderRow>
                    <styles.WhiteCircle>
                        <styles.LogoContainer>
                            <styles.Logo src={activeOffer?.companyLogo}/>
                        </styles.LogoContainer>
                    </styles.WhiteCircle>
                    <styles.EssentialInfo>
                        <styles.SalaryRow>
                            <span>
                                {activeOffer?.salaryFrom} - {activeOffer?.salaryTo} {activeOffer?.currency} net/month
                            </span>
                        </styles.SalaryRow>
                        <styles.TitleWrapper>
                            <styles.Title>
                                {activeOffer?.title}
                            </styles.Title>
                            {activeOffer?.fullyRemote ? (
                                <styles.Remote data-testid='offerheader-remotechip'>
                                    Remote
                                </styles.Remote>
                                )
                                :
                                activeOffer?.onlineInterview ?
                                <styles.OnlineInterview data-testid='offerheader-onlinechip'>
                                    Online Interview
                                </styles.OnlineInterview>
                            : null}
                        </styles.TitleWrapper>
                        <styles.Address>
                            {activeOffer?.officeStreet}, {activeOffer?.officeCity}
                        </styles.Address>
                    </styles.EssentialInfo>
                </styles.HeaderRow>
            </styles.Background>
            <styles.Tiles>
                {tiles.map(item => (
                    <Tile
                        key={item.title}
                        type={item.type}
                        title={item.title}
                        value={item.value!}
                        link={item.link}
                    />
                ))}
            </styles.Tiles>
        </styles.Wrapper>
    )
};