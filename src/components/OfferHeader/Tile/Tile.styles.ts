import styled from "styled-components";

export const styles = {
    Container: styled.div`
        border-radius: 4px;
        background-color: #fff;
        position: relative;
        text-align: center;
        padding: 20px 5px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.08), 0 1px 5px 0 rgba(0, 0, 0, 0.06);
        max-width: calc(20% - 5px);
        flex: 1;
        margin: 0 0 0 10px;
        &:first-child {
            margin: 0;
        }
    `,
    IconWrapper: styled.div`
        position: absolute;
        left: 50%;
        top: 0;
        transform: translate(-50%, -50%);
        background: #fff;
        padding: 8px;
        line-height: 0.8;
        border-radius: 50%;
        box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.08), 0 1px 5px 0 rgba(0, 0, 0, 0.06);
        color: ${(props: {type: string}) => (
            props.type === 'companyName' && '#ff5252' ||
            props.type === 'companySize' && '#fb8c00' ||
            props.type === 'empType' && '#ab47bc' ||
            props.type === 'expLvl' && '#66bb6a' ||
            props.type === 'dateAdded' && '#448aff'
        )};
    `,
    TileTitle: styled.div`
        white-space: nowrap;
        color: #99a1ab;
        font-size: 11px;
    `,
    StyledLink: styled.a`
        text-decoration: none;
        color: #039be5;
    `,
}