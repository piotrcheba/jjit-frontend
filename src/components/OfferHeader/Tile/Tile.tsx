import React, {ReactElement} from 'react';
import {styles} from './Tile.styles';
import BusinessOutlinedIcon from "@material-ui/icons/BusinessOutlined";
import PeopleIcon from '@material-ui/icons/People';
import DescriptionIcon from '@material-ui/icons/Description';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import {TileProps} from "./Tile.interface";

export default function Tile({type, title, link, value}: TileProps): ReactElement | null {
    let icon;
    let valueElement;

    switch (type) {
        case ('companyName'):
            icon = <BusinessOutlinedIcon/>;
            valueElement = (
                <styles.StyledLink
                    data-testid='tile-link'
                    href={'http://' + link}
                    target="_blank"
                >
                    {value}
                </styles.StyledLink>
            );
            break;
        case ('companySize'):
            icon = <PeopleIcon/>
            valueElement = value;
            break;
        case ('empType'):
            icon = <DescriptionIcon/>
            valueElement = value;
            break;
        case ('expLvl'):
            icon = <TrendingUpIcon/>
            valueElement = value;
            break;
        case ('dateAdded'):
            icon = <TimelapseIcon/>
            valueElement = value;
            break;
        default:
            return null;
    }

    return (
        <styles.Container>
            <styles.IconWrapper type={type}>
                {icon}
            </styles.IconWrapper>
            <div className='value' data-testid='tile-value'>
                {valueElement}
            </div>
            <styles.TileTitle data-testid='tile-title'>
                {title}
            </styles.TileTitle>
        </styles.Container>
    )
};