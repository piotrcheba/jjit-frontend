export interface TileProps {
    type: string;
    title: string;
    value: string;
    link?: string;
}