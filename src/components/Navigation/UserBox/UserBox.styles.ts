import styled from "styled-components";

export const styles = {
    Wrapper: styled.div`
        display: flex;
        cursor: pointer;
        align-self: stretch;
        align-items: center;
        border-color: rgb(229, 234, 243);
        border-style: solid;
        border-width: 0px 1px;
        &:hover {
            background: rgb(245, 245, 245);
        }
    `,

    Content: styled.div`
        display: flex;
        align-items: center;
        max-width: 200px;
        font-size: 14px;
        color: rgb(153, 161, 171);
        padding: 0px 14px;
    `,

    AvatarBox: styled.div`
        box-shadow: rgba(0, 0, 0, 0.08) 0px 6px 13px 0px;
        color: white;
        position: relative;
        text-align: center;
        flex-shrink: 0;
        display: flex;
        align-items: center;
        width: 40px;
        height: 40px;
        border-width: 3px;
        border-style: solid
        border-color: white;
        border-image: initial;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        background: ${(props: {img: string}) => `url(${props.img})`};
        background-size: 50px;
        background-repeat: no-repeat;
    `,

    Username: styled.div`
        display: inline-block;
        text-overflow: ellipsis;
        margin-left: 7px;
        overflow: hidden;
    `,
}

export const muiStyles = {
    pop: {
        position: 'absolute',
        top: '0px',
        left: '0px',
        '& > .MuiPopover-paper': {
            width: '250px',
            marginRight: '-15px',
            marginTop: '40px',
            position: 'relative',
            background: 'white',
            padding: '15px 15px 8px',
        },
    },
    btn: {
        color: 'rgb(55, 71, 79)',
        display: 'block',
        marginBottom: '7px',
        textTransform: 'none',
        width: '100%',
        fontWeight: 600,
        textAlign: 'left',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(229, 234, 243)',
        borderImage: 'initial',
        background: 'white',
        borderRadius: '5px',
        padding: '11px 22px',
    },
    icon: {
        width: '48px',
        height: '48px',
        marginRight: '30px',
        verticalAlign: 'middle',
        color: 'rgb(255, 64, 129)',
        padding: '12px',
        borderRadius: '50%',
        background: 'rgb(255, 227, 237)',
    }
}