import React, {ReactElement, useState} from 'react';
import {styles, muiStyles} from './UserBox.styles';
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import DescriptionIcon from '@material-ui/icons/Description';
import SettingsOutlinedIcon from "@material-ui/icons/SettingsOutlined";
import PowerSettingsNewOutlinedIcon from "@material-ui/icons/PowerSettingsNewOutlined";
import Popover from "@material-ui/core/Popover";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../../redux/actions/authActions";
import {RootState} from "../../../redux/store";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function UserBox(): ReactElement {
    const classes = useStyles();

    const auth = useSelector((state: RootState) => state.auth);

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const handleClick = (event: React.MouseEvent<HTMLDivElement>): void => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (): void => {
        setAnchorEl(null);
    }

    const logOutHandler = (): void => {
        dispatch(logoutUser());
        handleClose();
    }

    const dispatch = useDispatch();

    return (
        <>
            <styles.Wrapper data-testid='userbox-wrapper' onClick={handleClick}>
                <styles.Content>
                    <styles.AvatarBox img={auth.user?.avatarUrl!}/>
                    <styles.Username data-testid='userbox-usernamecontainer'>
                        {auth.user?.name}
                    </styles.Username>
                    <ExpandMoreIcon/>
                </styles.Content>
            </styles.Wrapper>
            <div data-testid='userbox-backdrop'>
                <Popover
                    data-testid='userbox-popover'
                    className={classes.pop}
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    elevation={1}
                >
                    <Button
                        component={Link}
                        to="/user_panel/profile"
                        className={classes.btn}
                        onClick={handleClose}
                    >
                        <PersonOutlineIcon className={classes.icon}/>
                        My profile
                    </Button>
                    {auth.isAuthenticated && auth.user?.accType === 'emp' ? (
                        <Button
                            data-testid='userbox-myoffersbtn'
                            component={Link}
                            to="/user_panel/offers"
                            className={classes.btn}
                            onClick={handleClose}
                        >
                            <DescriptionIcon className={classes.icon}/>
                            My offers
                        </Button>
                    ) : null}
                    {auth.isAuthenticated && auth.user?.accType === 'admin' ? (
                        <Button
                            component={Link}
                            to="/user_panel/alloffers"
                            className={classes.btn}
                            onClick={handleClose}
                        >
                            <DescriptionIcon className={classes.icon}/>
                            All offers
                        </Button>
                    ) : null}
                    {/*<Button*/}
                    {/*    component={Link}*/}
                    {/*    to="/user_panel/settings"*/}
                    {/*    className={classes.btn}*/}
                    {/*    onClick={handleClose}*/}
                    {/*>*/}
                    {/*    <SettingsOutlinedIcon className={classes.icon}/>*/}
                    {/*    Settings*/}
                    {/*</Button>*/}
                    <Button
                        component={Link}
                        to="/"
                        className={classes.btn}
                        onClick={logOutHandler}
                    >
                        <PowerSettingsNewOutlinedIcon className={classes.icon}/>
                        Log out
                    </Button>
                </Popover>
            </div>
        </>
    )
}