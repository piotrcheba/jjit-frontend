import React, {ReactElement} from 'react';
import {styles, muiStyles} from './SideDrawer.styles';
import {listItems} from './SideDrawer.constants';
import {makeStyles} from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Logo from "../../Logo/Logo";
import {SideDrawerProps} from "./SideDrawer.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function SideDrawer({showDrawer, closeHandler}: SideDrawerProps): ReactElement {
    const classes = useStyles();

    return (
        <Drawer
            data-testid="sidedrawer-drawer"
            anchor='right'
            open={showDrawer}
            onClose={closeHandler}
        >
            <styles.LogoWrapper>
                <Logo/>
            </styles.LogoWrapper>
            <List className={classes.list}>
                {listItems.map((item, index) => (
                    <ListItem className={classes.item} button component='a' key={index}>
                        <ListItemIcon>
                            <i className="material-icons" data-testid={`sidedrawer-icon-${index}`}>
                                {item.icon}
                            </i>
                        </ListItemIcon>
                        <ListItemText data-testid={`sidedrawer-label-${index}`}>
                            {item.label}
                        </ListItemText>
                    </ListItem>
                ))}
            </List>
        </Drawer>
    )
};