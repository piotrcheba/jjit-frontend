export interface SideDrawerProps {
    showDrawer: boolean;
    closeHandler: () => void;
}