export const listItems = [
    {label: 'Employer Panel', icon: 'work'},
    {label: 'Event', icon: 'mic'},
    {label: 'About us', icon: 'group'},
    {label: 'RSS', icon: 'rss_feed'},
    {label: 'Terms', icon: 'picture_as_pdf'},
    {label: 'Policy', icon: 'picture_as_pdf'},
];