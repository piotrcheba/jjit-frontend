import styled from "styled-components";

export const styles = {
    LogoWrapper: styled.div`
        width: 80%;
        text-align: center;
        line-height: 56px;
        border-bottom: 1px solid rgba(180, 180, 180, 0.3);
        padding: 6px 6px 5px;
        margin: 0px auto;
        & svg {
            height: 32px;
            width: 120px;
            vertical-align: middle;
            fill: rgb(55, 71, 79);
            margin: 0px 25px 2px 32px;
        }
    `
}

export const muiStyles = {
    list: {
        width: '300px',
        paddingTop: '10px',
    },
    item: {
        height: '56px',
        paddingLeft: '32px',
        color: 'rgb(55, 71, 79)',
        "& svg": {
            fill: 'rgb(55, 71, 79)',
        },
    }
}