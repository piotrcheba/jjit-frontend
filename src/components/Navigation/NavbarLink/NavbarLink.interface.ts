export interface NavbarLinkProps {
    url: string;
    label: string;
}