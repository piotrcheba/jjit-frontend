import styled from "styled-components";

export const styles = {
    StyledSpan: styled.span`
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `
}

export const muiStyles = {
    button: {
        color: 'rgb(153, 161, 171)',
        fontWeight: 600,
        textTransform: 'none',
        padding: 0,
        margin: '0 6px',
        "&.active": {
            color: 'rgb(255, 64, 129)',
        },
        "&:hover": {
            color: 'rgb(244, 143, 177)',
            background: 'none',
            textDecoration: 'none',
        },
    },
    span: {
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
    }
}