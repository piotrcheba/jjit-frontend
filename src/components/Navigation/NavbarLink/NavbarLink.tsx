import React, {ReactElement} from 'react';
import {styles, muiStyles} from './NavbarLink.styles';
import {NavLink} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {NavbarLinkProps} from "./NavbarLink.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function NavbarLink({url, label}: NavbarLinkProps): ReactElement {
    const classes = useStyles();

    return (
        <Button
            data-testid="navbarlink-btn"
            className={classes.button}
            component={NavLink}
            to={url}
            exact
        >
            <styles.StyledSpan data-testid="navbarlink-span">
                {label}
            </styles.StyledSpan>
        </Button>
    )
};