export const navItems = [
    {
        label: 'Offers',
        url: '/'
    },
    {
        label: 'Brands',
        url: '/brands'
    },
    {
        label: 'Geek',
        url: '/jgk'
    },
    {
        label: 'Matchmaking',
        url: '/matchmaking',
    },
    {
        label: 'Select',
        url: '/outplacement-select'
    }
];