export interface NavbarProps {
    toggleDrawer: React.Dispatch<React.SetStateAction<boolean>>;
}