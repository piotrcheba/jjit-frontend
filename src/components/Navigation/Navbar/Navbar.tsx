import React, {ReactElement, useState} from 'react';
import {styles, muiStyles} from './Navbar.styles';
import {navItems} from './Navbar.constants';
import NavbarLink from "../NavbarLink/NavbarLink";
import NavbarBtn from "../NavbarBtn/NavbarBtn";
import UserBox from '../UserBox/UserBox';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import Logo from "../../Logo/Logo";
import {makeStyles} from "@material-ui/core/styles";
import FaceIcon from '@material-ui/icons/Face';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {NavbarProps} from "./Navbar.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function Navbar({toggleDrawer}: NavbarProps): ReactElement {
    const auth = useSelector((state: any) => state.auth);

    const [showPopover, togglePopover] = useState(false);

    const classes = useStyles();

    const closePopHandler = (): void => togglePopover(false);

    return (
        <>
            <styles.Wrapper>
                <styles.LinkLogo to="/">
                    <Logo/>
                </styles.LinkLogo>
                <styles.Subtitle>
                    #1 Job Board for IT industry in Poland
                </styles.Subtitle>
                <styles.NavContainer>
                    {navItems.map(item => (
                        <NavbarLink
                            url={item.url}
                            label={item.label}
                            key={item.label}
                        />
                    ))}
                </styles.NavContainer>
                <styles.BtnContainer>
                    {auth.isAuthenticated ?
                        auth.user.accType === 'emp' ?
                            <>
                                <NavbarBtn
                                    url='/addoffer'
                                    type='post'
                                    label='Post a Job'
                                />
                                <UserBox />
                            </> : <UserBox />
                            :
                            <NavbarBtn
                                type='sign'
                                label='Sign in'
                                clicked={() => togglePopover(true)}
                            />
                    }
                    <NavbarBtn
                        type='drawer'
                        clicked={() => toggleDrawer(true)}
                    />
                </styles.BtnContainer>
            </styles.Wrapper>
            <Popover
                open={showPopover}
                onClose={closePopHandler}
                className={classes.pop}
                anchorReference="anchorPosition"
                anchorPosition={{ top: 54, left: 1520 }}
                transformOrigin={{ vertical: 'top', horizontal: 'left'}}
            >
                <Button
                    component={Link}
                    to="/devs"
                    className={classes.btn}
                    onClick={closePopHandler}
                >
                    <FaceIcon className={classes.faceIcon}/>
                    Sign in as Developer
                </Button>
                <Button
                    component={Link}
                    to="/emps"
                    className={classes.btn}
                    onClick={closePopHandler}
                >
                    <WorkOutlineIcon className={classes.workIcon}/>
                    Sign in to Employer Panel
                </Button>
            </Popover>
        </>
    );
};