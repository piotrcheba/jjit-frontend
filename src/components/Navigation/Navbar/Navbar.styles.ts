import styled from "styled-components";
import {Link} from "react-router-dom";

export const styles = {
    Wrapper: styled.div`
        position: sticky;
        top: 0;
        z-index: 1100;
        display: flex;
        min-height: 68px;
        align-items: center;
        justify-content: space-between;
        flex: 0 0 68px;
        background: #fff;
        border-bottom: 1px solid rgb(229, 234, 243);
        @media (max-width: 1024px) {
            min-height: 68px;
            flex: 0 0 68px;
        }
    `,
    LinkLogo: styled(Link)`
        display: inline-block;
        width: 120px;
        float: left;
        margin: 7px 15px 0 25px;
        & svg {
            fill: rgb(55, 71, 79);
        };
        @media (max-width: 1024px) {
            margin-left: 15px;
            width: 120px;
            height: 27px;
        }
    `,
    Subtitle: styled.span`
        color: rgb(153, 161, 171);
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        @media (max-width: 1024px) {
            display: none;
        }
    `,
    NavContainer: styled.div`
        display: flex;
        justify-content: flex-end;
        flex: 1 1 0%;
        @media (max-width: 1024px) {
            display: none;
        }
    `,
    BtnContainer: styled.div`
        margin-right: 24px;
        display: flex;
        align-items: center;
        align-self: stretch;
        @media (max-width: 1024px) {
            margin-right: 15px;
        }
    `,
}

export const muiStyles = {
    pop: {
        '& .MuiPopover-paper': {
            margin: '40px 0px 0px 15px',
            padding: '15px 15px 8px',
            background: 'white',
        }
    },
    btn: {
        color: 'rgb(55, 71, 79)',
        display: 'block',
        marginBottom: '7px',
        textTransform: 'none',
        width: '100%',
        fontWeight: 600,
        textAlign: 'left',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(229, 234, 243)',
        borderImage: 'initial',
        background: 'white',
        borderRadius: '5px',
        padding: '11px 22px',
    },
    faceIcon: {
        width: '48px',
        height: '48px',
        marginRight: '30px',
        verticalAlign: 'middle',
        color: 'rgb(255, 64, 129)',
        padding: '12px',
        borderRadius: '50%',
        background: 'rgb(255, 227, 237)',
    },
    workIcon: {
        color: 'rgb(163, 53, 181)',
        backgroundColor: 'rgb(242, 225, 244)',
        width: '48px',
        height: '48px',
        marginRight: '30px',
        verticalAlign: 'middle',
        padding: '12px',
        borderRadius: '50%',
    }
}