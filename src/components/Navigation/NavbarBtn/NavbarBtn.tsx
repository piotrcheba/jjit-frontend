import React, {ReactElement} from 'react';
import {muiStyles} from './NavbarBtn.styles';
import {makeStyles} from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab"
import Button from "@material-ui/core/Button";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MenuIcon from '@material-ui/icons/Menu';
import {NavbarBtnProps} from "./NavbarBtn.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function NavbarBtn({type, clicked, label, url}: NavbarBtnProps): ReactElement | null {
    const classes = useStyles();

    switch (type) {
        case 'post':
            return (
                <Fab
                    data-testid='navbarbtn-post'
                    variant='extended'
                    className={classes.post}
                    size='medium'
                    href={url}
                >
                    {label}
                </Fab>
            );
        case 'sign':
            return (
                <Fab
                    data-testid='navbarbtn-sign'
                    variant='extended'
                    className={classes.sign}
                    size='medium'
                    onClick={clicked}
                >
                    {label}
                    <ExpandMoreIcon className={classes.icon}/>
                </Fab>
            );
        case 'drawer':
            return (
                <Button className={classes.iconBtn} onClick={clicked}>
                    <MenuIcon/>
                </Button>
            );
        default:
            return null;
    }
};