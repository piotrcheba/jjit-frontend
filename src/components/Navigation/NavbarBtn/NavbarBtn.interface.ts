export interface NavbarBtnProps {
    type: string;
    label?: string;
    url?: string;
    clicked?: () => void;
}