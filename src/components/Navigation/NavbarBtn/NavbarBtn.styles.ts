export const muiStyles = {
    post: {
        '&&': {
            letterSpacing: '0.5px',
            fontWeight: 400,
            height: '38px',
            color: 'rgb(117, 117, 117)',
            whiteSpace: 'nowrap',
            margin: '0 12px 0 6px',
            padding: '0 16px',
            borderWidth: '1px',
            borderStyle: 'solid',
            borderColor: 'rgb(228, 232, 240)',
            borderImage: 'initial',
            background: '#fff',
        },
        '&&:hover': {
            background: 'rgb(245, 245, 245)',
        }
    },
    sign: {
        marginRight: '12px',
        letterSpacing: '0.5px',
        fontWeight: 400,
        height: '38px',
        color: '#fff',
        whiteSpace: 'nowrap',
        padding: '0 16px',
        background: 'rgb(255, 64, 129)',
        '&:hover': {
            backgroundColor: 'rgb(178, 44, 90)'
        }
    },
    iconBtn: {
        color: 'rgb(153, 161, 171)',
        marginLeft: '15px',
        minWidth: '0px !important',
        padding: '0px',
        "&:hover": {
            color: 'rgb(244, 143, 177)',
            background: 'none',
        }
    },
    icon: {
        marginRight: '-10px',
    },
}