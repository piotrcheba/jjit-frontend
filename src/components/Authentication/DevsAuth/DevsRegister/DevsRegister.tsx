import React, {ReactElement, useState} from 'react';
import {useDispatch} from "react-redux";
import {registerUser} from "../../../../redux/actions/authActions";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import {makeStyles} from "@material-ui/core/styles";
import {styles, muiStyles} from "./DevsRegister.styles";
import {checkValidity} from "../../../../utils/checkValidity";
import {RegisterFormInterface} from "../../../../interfaces/authForm.interface";
import {DevsRegisterProps} from "./DevsRegister.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function DevsRegister({history}: DevsRegisterProps): ReactElement {
    const classes = useStyles();

    const [user, setUser] = useState<RegisterFormInterface>({
        name: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        },
        surname: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        },
        email: {
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        },
        password2: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        }
    });

    const [formIsValid, setFormIsValid] = useState<boolean>(false);

    const dispatch = useDispatch();

    const inputChangedHandler = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, inputIdentifier: keyof typeof user): void => {
        const updatedForm = {
            ...user,
            [inputIdentifier]: {
                ...user[inputIdentifier],
                value: event.target.value,
                valid: checkValidity(event.target.value, user[inputIdentifier].validation),
                touched: true
            }
        }

        let formIsValid = true;

        for (let key in updatedForm) {
            // @ts-ignore
            formIsValid = updatedForm[key].valid && formIsValid
        }

        setFormIsValid(formIsValid);
        setUser(updatedForm);
    }

    const onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        const userData = {
            accType: 'dev',
            name: user.name.value,
            surname: user.surname.value,
            email: user.email.value,
            password: user.password.value,
            password2: user.password2.value
        }

        dispatch(registerUser(userData));
        history.push('/devs');
    }

    return (
        <>
            <Typography
                variant='h1'
                className={classes.header}
            >
                Register
            </Typography>
            <styles.Form noValidate onSubmit={onSubmit}>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devsregister-nameinput'}}
                            fullWidth
                            label="Name"
                            variant="outlined"
                            type="text"
                            error={!user.name.valid && user.name.touched}
                            helperText={!user.name.valid && user.name.touched ? "Name is required" : " "}
                            id="name"
                            onChange={e => inputChangedHandler(e, 'name')}
                            value={user.name.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devsregister-surnameinput'}}
                            fullWidth
                            label="Surname"
                            variant="outlined"
                            type="text"
                            error={!user.surname.valid && user.surname.touched}
                            helperText={!user.surname.valid && user.surname.touched ? "Surname is required" : " "}
                            id="surname"
                            onChange={e => inputChangedHandler(e, 'surname')}
                            value={user.surname.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devsregister-emailinput'}}
                            fullWidth
                            label="Email"
                            variant="outlined"
                            type="email"
                            error={!user.email.valid && user.email.touched}
                            helperText={!user.email.valid && user.email.touched ? "Incorrect email" : " "}
                            id="email"
                            onChange={e => inputChangedHandler(e, 'email')}
                            value={user.email.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devsregister-pwinput'}}
                            fullWidth
                            label="Password (min. 6 characters)"
                            variant="outlined"
                            type="password"
                            error={!user.password.valid && user.password.touched}
                            helperText={!user.password.valid && user.password.touched ? "Password is required & has to be min. 6 characters" : " "}
                            id="password"
                            onChange={e => inputChangedHandler(e, 'password')}
                            value={user.password.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devsregister-pw2input'}}
                            fullWidth
                            label="Confirm password"
                            variant="outlined"
                            type="password"
                            error={!user.password2.valid && user.password2.touched}
                            helperText={!user.password2.valid && user.password2.touched ? "Field is required" : " "}
                            id="password2"
                            onChange={e => inputChangedHandler(e, 'password2')}
                            value={user.password2.value}
                        />
                    </Grid>
                </Grid>
                <Fab
                    data-testid='devsregister-submitbtn'
                    className={classes.btn}
                    disabled={!formIsValid}
                    color="primary"
                    variant="extended"
                    type="submit"
                >
                    Sign up
                </Fab>
                <Typography
                    variant="subtitle2"
                    gutterBottom
                >
                    {"Already registered? "}
                    <Link href='/devs' color='primary' underline="hover">
                        Sign in
                    </Link>
                </Typography>
            </styles.Form>
        </>
    )
}