import styled from "styled-components";

export const styles = {
    Form: styled.form`
        min-width: 365px;
    `
}

export const muiStyles = {
    header: {
        color: '#2E384D',
        fontSize: '34px',
        fontFamily: 'Open Sans, sans-serif',
        fontWeight: 300,
        lineHeight: 1.167,
        marginBottom: '20px',
    },
    btn: {
        width: '70%',
        marginTop: '15px',
        marginBottom: '36px',
    },
    icon: {
        position: 'relative',
        top: '-12px',
        display: 'block',
        margin: '0px auto',
    },
    btnLink: {
        boxSizing: 'border-box',
        width: '100%',
        fontSize: '12px',
        color: 'rgb(117, 117, 117)',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(228, 232, 240)',
        borderImage: 'initial',
        background: 'white',
        '& svg': {
            marginRight: '8px',
        }
    },
}