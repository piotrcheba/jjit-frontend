import styled from "styled-components";
import img from "../../../assets/signin.svg";

export const styles = {
    Wrapper: styled.div`
        display: flex;
        flex: 1 1 0%;
        height: calc(100% - 68px);
    `,
    ContentContainer: styled.div`
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        justify-content: center;
        flex: 0 0 50%;
        background: white;
    `,
    ContentWrapper: styled.div`
        max-width: 568px;
        text-align: center;
        padding: 0 84px;
    `,
    Img: styled.div`
        display: flex;
        position: relative;
        align-items: center;
        flex: 0 0 50%;
        background: linear-gradient(123.06deg, rgb(70, 51 ,174) 0%, rgb(157, 166, 217) 100%, rgb(159, 168, 218) 100%);
        
        &::after {
            content: "";
            position: absolute;
            width: 100%;
            height: 100%;
            background-image: url(${img});
            background-repeat: no-repeat;
        }
    `,
}