import React, {ReactElement, useState} from 'react';
import {useDispatch} from "react-redux";
import {loginUser} from "../../../../redux/actions/authActions";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import Icon from "../../../Icon/Icon";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Link from "@material-ui/core/Link";
import {makeStyles} from "@material-ui/core/styles";
import {styles, muiStyles} from "./DevsLogin.styles";
import {buttons} from "./DevsLogin.constants";
import {checkValidity} from "../../../../utils/checkValidity";
import {DevsLoginProps} from "./DevsLogin.interface";
import {LoginFormInterface} from "../../../../interfaces/authForm.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function DevsLogin({history}: DevsLoginProps): ReactElement {
    const classes = useStyles();

    const [credentials, setCredentials] = useState<LoginFormInterface>({
        email: {
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password: {
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        }
    });

    const [formIsValid, setFormIsValid] = useState(false);

    const dispatch = useDispatch();

    const inputChangedHandler = (event: React.ChangeEvent<HTMLInputElement>, inputIdentifier: keyof typeof credentials): void => {
        const updatedForm = {
            ...credentials,
            [inputIdentifier]: {
                ...credentials[inputIdentifier],
                value: event.target.value,
                valid: checkValidity(event.target.value, credentials[inputIdentifier].validation),
                touched: true
            }
        }

        let formIsValid = true;

        for (let key in updatedForm) {
            // @ts-ignore
            formIsValid = updatedForm[key].valid && formIsValid
        }

        setFormIsValid(formIsValid);
        setCredentials(updatedForm);
    }

    const onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        const userData = {
            email: credentials.email.value,
            password: credentials.password.value
        }

        dispatch(loginUser(userData));
        history.push('/');
    }

    return (
        <>
            <Typography
                variant='h1'
                className={classes.header}
            >
                Get started for free
            </Typography>
            <Grid container spacing={2}>
                {buttons.map(item => (
                    <Grid item xs={12} md={6} key={item.icon}>
                        <Fab
                            component="a"
                            className={classes.btnLink}
                            variant="extended"
                            href={item.link}
                        >
                            <Icon type={item.icon}/>
                            {item.label}
                        </Fab>
                    </Grid>
                ))}
            </Grid>
            <styles.BoldDivider>
                <span>
                    Or
                </span>
            </styles.BoldDivider>
            <form noValidate onSubmit={onSubmit}>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={1}>
                        <PermIdentityIcon className={classes.icon}/>
                    </Grid>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devslogin-emailinput'}}
                            fullWidth
                            label="Email"
                            variant="outlined"
                            type="email"
                            error={!credentials.email.valid && credentials.email.touched}
                            helperText={!credentials.email.valid && credentials.email.touched ? "Incorrect email" : " "}
                            id="email"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'email')}
                            value={credentials.email.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={1}>
                        <LockOutlinedIcon className={classes.icon}/>
                    </Grid>
                    <Grid item xs={true}>
                        <TextField
                            inputProps={{'data-testid': 'devslogin-pwinput'}}
                            fullWidth
                            label="Password"
                            variant="outlined"
                            type="password"
                            error={!credentials.password.valid && credentials.password.touched}
                            helperText={!credentials.password.valid && credentials.password.touched ? "Field is required" : " "}
                            id="password"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'password')}
                            value={credentials.password.value}

                        />
                    </Grid>
                </Grid>
                <Fab
                    data-testid='devslogin-btn'
                    className={classes.btn}
                    disabled={!formIsValid}
                    color="primary"
                    variant="extended"
                    type="submit"
                >
                    Sign in
                </Fab>
                <Typography
                    variant="subtitle2"
                    gutterBottom
                >
                    {"New account? "}
                    <Link href='/devs/register' color='primary' underline="hover">
                        Register
                    </Link>
                </Typography>
            </form>
        </>
    )
}