import styled from 'styled-components';

export const styles = {
    BoldDivider: styled.div`
        height: 2px;
        text-align: center;
        background: rgb(228, 232, 240);
        margin: 20px 0px 36px;
        & span {
            color: rgb(119, 119, 119);
            font-size: 14px;
            position: relative;
            top: -12px;
            background: white;
            padding: 0px 20px;
        }
    `,
    ThinDivivder: styled.div`
        height: 1px;
        width: 60%;
        text-align: center;
        background: rgb(231, 233, 239);
        margin: 15px 0px 15px 20%;
    `,
}

export const muiStyles = {
    header: {
        color: '#2E384D',
        fontSize: '34px',
        fontFamily: 'Open Sans, sans-serif',
        fontWeight: 300,
        lineHeight: 1.167,
        marginBottom: '20px',
    },
    btn: {
        width: '70%',
        marginTop: '15px',
        marginBottom: '36px',
    },
    icon: {
        position: 'relative',
        color: 'rgb(153, 161, 171)',
        display: 'block',
        margin: '0px auto 12px',
    },
    btnLink: {
        boxSizing: 'border-box',
        width: '100%',
        fontSize: '12px',
        color: 'rgb(117, 117, 117)',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(228, 232, 240)',
        borderImage: 'initial',
        background: 'white',
        '& svg': {
            marginRight: '8px',
        }
    },
}