export const buttons = [
    {label: 'Sign in with Google', icon: 'google', link: '/'},
    {label: 'Sign in with Github', icon: 'github', link: '/'},
    {label: 'Sign in with Linkedin', icon: 'linkedin', link: '/'},
    {label: 'Sign in with Facebook', icon: 'facebook', link: '/'},
]