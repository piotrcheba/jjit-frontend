import React, {ReactElement, useEffect} from 'react';
import {Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import DevsLogin from "./DevsLogin/DevsLogin";
import DevsRegister from "./DevsRegister/DevsRegister";
import {styles} from './DevsAuth.styles';
import {DevsAuthProps} from "./DevsAuth.interface";
import {RootState} from "../../../redux/store";

export default function DevsAuth({history}: DevsAuthProps): ReactElement {
    const isAuthenticated = useSelector((state: RootState): boolean => state.auth.isAuthenticated);

    useEffect((): void => {
        if (isAuthenticated) {
            history.push('/');
        }
    }, [isAuthenticated])

    return (
        <styles.Wrapper>
            <styles.ContentContainer>
                <styles.ContentWrapper>
                    <Switch>
                        <Route
                            path="/devs/register"
                            component={DevsRegister}
                        />
                        <Route
                            path="/devs"
                            component={DevsLogin}
                            exact
                        />
                    </Switch>
                </styles.ContentWrapper>
            </styles.ContentContainer>
            <styles.Img/>
        </styles.Wrapper>
    )
};