import React, {ReactElement} from 'react';
import {styles, muiStyles} from './Settings.styles';
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteUser} from "../../../../redux/actions/authActions";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function Settings(): ReactElement {
    const classes = useStyles();

    const auth = useSelector((state: any) => state.auth);

    const dispatch = useDispatch();

    return (
        <>
            <Typography
                className={classes.header}
                variant="h1"
            >
                Settings
            </Typography>
            <form autoComplete="off">
                <Paper className={classes.section}>
                    <styles.ChangePwContent>
                        <Typography
                            variant="h2"
                        >
                            Set up new password
                        </Typography>
                        <styles.StyledParagraph>
                            Follow the instructions below
                        </styles.StyledParagraph>
                        <Grid container spacing={2} alignItems="center" justify="center">
                            <Grid item xs={1}>
                                <LockOutlinedIcon/>
                            </Grid>
                            <Grid item xs={true}>
                                <TextField
                                    fullWidth
                                    type="password"
                                    name="oldPassword"
                                    label="Old password"
                                    variant="outlined"
                                    helperText=" "
                                    required
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={2} alignItems="center" justify="center">
                            <Grid item xs={1}>
                                <LockOutlinedIcon/>
                            </Grid>
                            <Grid item xs={true}>
                                <TextField
                                    fullWidth
                                    type="password"
                                    name="newPassword"
                                    label="New password"
                                    variant="outlined"
                                    helperText=" "
                                    required
                                />
                            </Grid>
                        </Grid>
                    </styles.ChangePwContent>
                </Paper>
                <Paper className={classes.section}>
                    <Typography
                        variant="h2"
                    >
                        Delete Account
                    </Typography>
                    <styles.DeleteAccContent>
                        <p>
                            Delete your account and account data
                        </p>
                        <Fab
                            component={Link}
                            to='/'
                            className={classes.btn}
                            color="primary"
                            size="medium"
                            variant="extended"
                            onClick={() => dispatch(deleteUser(auth.user.id))}
                        >
                            Delete my account
                        </Fab>
                    </styles.DeleteAccContent>
                </Paper>
                <styles.UpdateBtn>
                    <Fab
                        color="primary"
                        size="medium"
                        variant="extended"
                    >
                        Update profile
                    </Fab>
                </styles.UpdateBtn>
            </form>
        </>
    )
}