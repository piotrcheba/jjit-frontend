import styled from "styled-components";

export const styles = {
    ChangePwContent: styled.div`
        max-width: 557px;
    `,
    StyledParagraph: styled.p`
        margin-bottom: 40px;
    `,
    DeleteAccContent: styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 50px;
    `,
    UpdateBtn: styled.div`
        marginBottom: '20px',
    `,
}

export const muiStyles = {
    header: {
        color: 'rgb(55, 71, 79)',
        margin: '30px 0px',
    },
    section: {
        marginBottom: '20px',
        padding: '20px',
        background: 'white',
    },
    btn: {
        '&&': {
            letterSpacing: '0.5px',
            fontWeight: 400,
            height: '38px',
            color: 'rgb(117, 117, 117)',
            whiteSpace: 'nowrap',
            margin: '0px 12px 0px 6px',
            padding: '0px 16px',
            borderWidth: '1px',
            borderStyle: 'solid',
            borderColor: 'rgb(228, 232, 240)',
            borderImage: 'initial',
            background: 'white',
        },
        '&&:hover': {
            color: 'rgb(117, 117, 117)',
            background: 'rgb(245, 245, 245)',
        }
    }
}