import React, {ReactElement, useState} from 'react';
import {styles, muiStyles} from './MyProfile.styles';
import {expYears} from './MyProfile.constants';
import Typography from "@material-ui/core/Typography";
import {Grid, Paper} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import linkedin from "../../../../assets/linkedin-logo.svg";
import Button from "@material-ui/core/Button";
import github from "../../../../assets/github.svg";
import CreateOutlinedIcon from '@material-ui/icons/CreateOutlined';
import Fab from "@material-ui/core/Fab";
import {makeStyles} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import axios from 'axios';
import {updateUser} from "../../../../redux/actions/authActions";
import CVDropzone from "./CVDropzone/CVDropzone";
import PreviewOffers from "./PreviewOffers/PreviewOffers";
import {apiConfig} from "../../../../config/api";
import {RootState} from "../../../../redux/store";
import {UserInterface} from "../../../../interfaces/auth.interface";

const CLOUDINARY_UPLOAD_AVATAR_PRESET = 'jjit_avatar';
const CLOUDINARY_UPLOAD_CV_PRESET = 'jjit_cv';

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function MyProfile(): ReactElement {
    const classes = useStyles();

    const user = useSelector((state: RootState): UserInterface | undefined => state.auth.user);
    const dispatch = useDispatch();

    const [editUser, setEditUser] = useState({
        name: user?.name,
        surname: user?.surname,
        email: user?.email,
        avatarUrl: user?.avatarUrl,
        city: user?.city,
        description: user?.description,
        liProfile: user?.liProfile,
        githubProfile: user?.githubProfile,
        cv: user?.cv,
        expYears: user?.expYears
    })

    const [tempAvatar, setTempAvatar] = useState('');
    const [tempFile, setTempFile] = useState<any>(user?.cv || '');

    const inputChangedHandler = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, inputIdentifier: keyof typeof editUser): void => {
        const updatedForm = {
            ...editUser,
            [inputIdentifier]: event.target.value
        }

        setEditUser(updatedForm);
    }

    const handleExpChange = (years: string): void => {
        const updatedForm = {
            ...editUser,
            expYears: years
        }

        setEditUser(updatedForm);
    }

    const onChangeImage = (e: React.ChangeEvent<HTMLInputElement>): void => {
        if (!e.target.files) return;
        let file = e.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function() {
            const base64data = reader.result;
            // @ts-ignore
            setTempAvatar(base64data);
        }
    };

    const onSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
        e.preventDefault();

        const id = user?.id;

        const tempImg = tempAvatar;
        const tempCv = tempFile[0];

        let avatarUrl = '';
        let cv = {};

        // Upload avatar to cloudinary if exist
        if (tempImg) {
            const formDataAvatar = new FormData();
            formDataAvatar.append('file', tempImg);
            formDataAvatar.append('upload_preset', CLOUDINARY_UPLOAD_AVATAR_PRESET);
            delete axios.defaults.headers.common["Authorization"]
            const res = await axios.post(apiConfig.CLOUDINARY_API_URL, formDataAvatar);
            avatarUrl = res.data.secure_url;
        }

        // Upload cv to cloudinary if exist
        if (tempCv) {
            const formDataCv = new FormData();
            // @ts-ignore
            formDataCv.append('file', tempCv);
            formDataCv.append('upload_preset', CLOUDINARY_UPLOAD_CV_PRESET);
            delete axios.defaults.headers.common["Authorization"]
            const res = await axios.post(apiConfig.CLOUDINARY_API_URL, formDataCv);
            cv = {
                name: res.data.original_filename,
                size: res.data.bytes,
                url: res.data.secure_url
            };
        }

        let valuesToUpdate = {}

        for (let key in editUser) {
            // @ts-ignore
            if (editUser[key] !== user[key]) {
                valuesToUpdate = {
                    ...valuesToUpdate,
                    // @ts-ignore
                    [key]: editUser[key]
                }
            }
        }

        const dataToUpdate = {
            ...valuesToUpdate,
            avatarUrl: avatarUrl,
            cv: cv
        }

        dispatch(updateUser(id!, dataToUpdate));
    }

    let devInputs = null;
    let devPanels = null;

    if (user?.accType === 'emp') {
        devPanels = <PreviewOffers/>;
    }

    if (user?.accType === 'dev') {
        devInputs = <>
            <TextField
                fullWidth
                label="City"
                variant="outlined"
                value={editUser.city}
                helperText=" "
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'city')}
            />
            <TextField
                fullWidth
                multiline
                name="description"
                rows={11}
                label="Introduce yourself"
                variant="outlined"
                value={editUser.description}
                helperText=" "
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'description')}
            />
        </>;
        devPanels = <>
            <Paper className={classes.section}>
                <Typography
                    variant="h3"
                >
                    UPLOAD CV
                </Typography>
                <styles.Description>
                    Paste your Linkedin page as CV
                </styles.Description>
                <Grid container className={classes.grid} spacing={2} alignItems="center">
                    <Grid item className={classes.gridItem} xs={1}>
                        <img src={linkedin} alt="" className={classes.icon}/>
                    </Grid>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            name="linkedinUrl"
                            label="Linkedin"
                            variant="outlined"
                            value={editUser.liProfile}
                            helperText=" "
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'liProfile')}
                        />
                    </Grid>
                </Grid>
                <p>
                    <b>Or add as attachment </b>
                </p>
                <CVDropzone
                    file={tempFile}
                    setTempFile={setTempFile}
                />
            </Paper>
            <Paper className={classes.section}>
                <Typography
                    variant="h3"
                >
                    YEARS OF EXPERIENCE
                </Typography>
                <styles.Description>
                    How many years of experience do you have?
                </styles.Description>
                <Grid container spacing={1}>
                    {expYears.map((item: string): ReactElement => (
                        <Grid item xs={true}>
                            <Button
                                className={editUser.expYears === item ? classes.expBtnActive : classes.expBtn}
                                onClick={() => handleExpChange(item)}
                            >
                                <strong>{item}</strong>
                                years
                            </Button>
                        </Grid>
                    ))}
                </Grid>
            </Paper>
            <Paper className={classes.section}>
                <Typography
                    variant="h3"
                >
                    SOCIAL LINKS
                </Typography>
                <Grid container className={classes.socialSection} spacing={2} alignItems="center" justify="center">
                    <Grid item className={classes.gridItem} xs={1}>
                        <img
                            src={github}
                            alt=""
                            className={classes.icon}
                        />
                    </Grid>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            name="githubUrl"
                            label="Github"
                            variant="outlined"
                            value={editUser.githubProfile}
                            helperText=" "
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'githubProfile')}
                        />
                    </Grid>
                </Grid>
            </Paper>
        </>
    }

    return (
        <>
            <Typography
                className={classes.header}
                variant="h1"
            >
                My profile
                <Typography
                    className={classes.desc}
                    variant="body1"
                >
                    Complete your profile & apply with just one click!
                </Typography>
            </Typography>
            <form autoComplete="off" encType="multipart/form-data" onSubmit={onSubmit}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={6}>
                        <Paper className={classes.section}>
                            <Typography
                                variant="h3"
                            >
                                USER DETAILS
                            </Typography>
                            <styles.AvatarUploadWrapper>
                                <styles.AvatarUpload img={tempAvatar ? tempAvatar : editUser.avatarUrl!}>
                                    <label>
                                        <styles.IconWrapper>
                                            <CreateOutlinedIcon className={classes.imgIcon}/>
                                        </styles.IconWrapper>
                                        <styles.ImgInput
                                            id="file-upload"
                                            type="file"
                                            accept="image/jpg, image/png"
                                            onChange={onChangeImage}
                                        />
                                    </label>
                                </styles.AvatarUpload>
                            </styles.AvatarUploadWrapper>
                            <TextField
                                fullWidth
                                label="Name"
                                variant="outlined"
                                value={editUser.name}
                                helperText=" "
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'name')}
                            />
                            <TextField
                                fullWidth
                                label="Surname"
                                variant="outlined"
                                value={editUser.surname}
                                helperText=" "
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'surname')}
                            />
                            <TextField
                                fullWidth
                                disabled
                                label="Email"
                                variant="outlined"
                                value={editUser.email}
                                helperText=" "
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'email')}
                            />
                            {devInputs}
                        </Paper>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        {devPanels}
                    </Grid>
                </Grid>
                <Fab
                    color="primary"
                    size="medium"
                    variant="extended"
                    type="submit"
                >
                    Update profile
                </Fab>
            </form>
        </>
    )
};