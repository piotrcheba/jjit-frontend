import styled from "styled-components";
import SentimentDissatisfiedOutlinedIcon from '@material-ui/icons/SentimentDissatisfiedOutlined';

export const styles = {
    Wrapper: styled.div`
        display: flex;
        align-items: center;
        flex-direction: column;
        max-height: 237px;
        width: 100%;
        margin-top: 20px;
    `,
    Icon: styled(SentimentDissatisfiedOutlinedIcon)`
        width: 100px;
        height: 100px;
        font-size: 80px;
        fill: rgb(255, 64, 129);
    `,
    Text: styled.span`
        padding-top: 10px;
        color: #2E384D;
        font-size: 22px;
        font-family: Open Sans, sans-serif;
        font-weight: 300;
        line-height: 1.167;
        margin-bottom: 10px;
    `
}

export const muiStyles = {
    section: {
        marginBottom: '22px',
        padding: '20px',
        background: 'white',
    },
}