import React, {ReactElement} from 'react';
import Typography from "@material-ui/core/Typography";
import {styles, muiStyles} from "./PreviewOffers.styles";
import {Link, Paper} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useSelector} from "react-redux";
import PreviewOffer from "./PreviewOffer/PreviewOffer";
import {RootState} from "../../../../../redux/store";
import {OfferInterface} from "../../../../../interfaces/offer.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

const NoOffersMsg = (): ReactElement => (
    <>
        <styles.Icon/>
        <styles.Text>
            You have no active offers
        </styles.Text>
        <Typography
            variant="subtitle2"
            gutterBottom
        >
            <Link href='/addoffer' color='primary' underline="hover">
                Add one now!
            </Link>
        </Typography>
    </>
)

export default function PreviewOffers(): ReactElement {
    const classes = useStyles();

    const offers = useSelector((state: RootState): OfferInterface[] => state.auth.offers);

    return (
        <Paper className={classes.section}>
            <Typography
                variant="h3"
            >
                PREVIEW OFFERS
            </Typography>
            <styles.Wrapper>
                {offers.length > 0 ?
                    offers.map((el: OfferInterface): ReactElement => (
                        <PreviewOffer
                            mainTech={el.mainTech}
                            title={el.title}
                            salaryFrom={el.salaryFrom}
                            salaryTo={el.salaryTo}
                            currency={el.currency}
                        />
                    )) : <NoOffersMsg />
                }
            </styles.Wrapper>
        </Paper>
    )
}