export interface PreviewOfferProps {
    mainTech: string;
    title: string;
    salaryFrom: number;
    salaryTo: number;
    currency: string;
}