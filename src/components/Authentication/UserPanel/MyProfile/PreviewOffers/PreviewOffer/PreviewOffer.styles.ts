import styled from "styled-components";

export const styles = {
    Offer: styled.div`
        display: flex;
        width: 100%;
        box-shadow: rgba(0, 0, 0, 0.05) 0 2px 2px 0, rgba(0, 0, 0, 0.04) 0 1px 5px 0;
        height: 30px;
        background-color: white;
        color: rgba(0, 0, 0, 0.87);
        border-radius: 6px;
        margin: 0 10px 10px;
        padding: 0;
        overflow: hidden;
        text-decoration: none;
        &:hover {
        box-shadow: rgba(0, 0, 0, 0.07) 0 4px 4px 0, rgba(0, 0, 0, 0.06) 0 3px 7px 0;
        transition: box-shadow 0.9s cubic-bezier(0.25, 0.8, 0.25, 1) 0s;
    `,
    Colorbar: styled.div`
        flex-basis: 5px;
        flex-shrink: 0;
        background: ${(props: {color: string}) =>
        props.color === 'javascript' && 'rgb(255, 199, 6)' ||
        props.color === 'html' && 'rgb(255, 125, 80)' ||
        props.color === 'php' && 'rgb(65, 173, 250)' ||
        props.color === 'ruby' && 'rgb(239, 83, 80)' ||
        props.color === 'python' && 'rgb(31, 94, 182)' ||
        props.color === 'java' && 'rgb(250, 101, 107)' ||
        props.color === 'net' && 'rgb(103, 37, 114)' ||
        props.color === 'scala' && 'rgb(248, 100, 104)' ||
        props.color === 'c' && 'rgb(47, 207, 187)' ||
        props.color === 'mobile' && 'rgb(224, 79, 134)' ||
        props.color === 'testing' && 'rgb(0, 150, 136)' ||
        props.color === 'devops' && 'rgb(82, 102, 225)' ||
        props.color === 'ux' && 'rgb(255, 183, 77)' ||
        props.color === 'pm' && 'rgb(128, 203, 196)' ||
        props.color === 'game' && 'rgb(255, 64, 129)' ||
        props.color === 'analytics' && 'rgb(59, 89, 152)' ||
        props.color === 'security' && 'rgb(83, 109, 254)' ||
        props.color === 'data' && 'rgb(137, 219, 84)' ||
        props.color === 'go' && 'rgb(106, 215, 229)' ||
        props.color === 'sap' && 'rgb(2, 175, 235)' ||
        props.color === 'support' && 'rgb(212, 104, 222)' ||
        props.color === 'other' && 'rgb(236, 76, 182)'
    };
    `,
    Title: styled.div`
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
        flex-grow: 1;
        min-width: 0;
        padding-left: 12px;
    `,
    TitleSpan: styled.span`
        color: rgb(55, 71, 79);
        font-size: 13px;
        line-height: 23px;
        white-space: nowrap;
        font-weight: 600;
        align-self: center;
        text-overflow: ellipsis;
        overflow: hidden;
        @media (min-width: 1025px) {
            font-size: 14px;
        }
    `,
    Salary: styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding-right: 6px;
        @media (min-width: 1025px) {
            justify-content: flex-end;
        }
    `,
    SalarySpan: styled.span`
        color: rgb(30, 198, 108);
        font-size: 13px;
        line-height: 23px;
        white-space: nowrap;
        align-self: center;
        text-overflow: ellipsis;
        overflow: hidden;
        @media (min-width: 1025px) {
            font-size: 14px;
        }
    `,
}