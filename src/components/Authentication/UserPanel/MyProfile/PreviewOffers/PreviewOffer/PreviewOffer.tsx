import React, {ReactElement} from 'react';
import {styles} from "./PreviewOffer.styles";
import {PreviewOfferProps} from "./PreviewOffer.interface";

export default function PreviewOffer({mainTech, currency, salaryFrom, salaryTo, title}: PreviewOfferProps): ReactElement {
    return (
        <styles.Offer>
            <styles.Colorbar color={mainTech} />
            <styles.Title>
                <styles.TitleSpan>
                    {title}
                </styles.TitleSpan>
            </styles.Title>
            <styles.Salary>
                <styles.SalarySpan>
                    {`${salaryFrom} - ${salaryTo} ${currency}`}
                </styles.SalarySpan>
            </styles.Salary>
        </styles.Offer>
    )
}