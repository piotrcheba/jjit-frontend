export const expYears: string[] = [
    "0-1",
    "1-2",
    "2-4",
    "4-6",
    "6-10",
    "10+",
]