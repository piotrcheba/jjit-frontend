import React, {ReactElement} from 'react';
import Dropzone from 'react-dropzone';
import {styles} from "./CVDropzone.styles";
import Icon from "../../../../Icon/Icon";
import {CVDropzoneProps} from "./CVDropzone.interface";

const NoFileLabel = (): ReactElement => (
    <styles.UploadLabel>
        {"Drag and Drop or "}
        <styles.ColorLabel>
            Browse
        </styles.ColorLabel>
    </styles.UploadLabel>
)

export default function CVDropzone({file, setTempFile}: CVDropzoneProps): ReactElement {
    const fileSizeDisplay = (fileSize: number): ReactElement => {
        const sizeInKb = fileSize / 1000;

        if (sizeInKb > 1000) {
            const sizeInMb = sizeInKb / 1000;

            return (
                <styles.FileSizeSpan>
                    {Math.round(sizeInMb * 100) / 100 + " mb"}
                </styles.FileSizeSpan>
            )
        }

        return (
            <styles.FileSizeSpan>
                {Math.round(sizeInKb * 100) / 100 + " kb"}
            </styles.FileSizeSpan>
        )
    }

    return (
        <Dropzone
            accept="image/*"
            onDrop={setTempFile}
        >
            {({getRootProps, getInputProps}) => (
                <styles.FileUploader isFile={Boolean(file)} {...getRootProps()}>
                    {file ? null : <input {...getInputProps()} />}
                    <styles.UploadBox>
                        <styles.UploadIcon>
                            {file ? <Icon type="uploaded"/> : <Icon type="upload"/>}
                        </styles.UploadIcon>
                        {file? (
                            <>
                                <styles.UploadedLabel>
                                    {file.name || file[0].name}
                                </styles.UploadedLabel>
                                {fileSizeDisplay(file.size || file[0].size)}
                            </>
                        ): <NoFileLabel />}
                    </styles.UploadBox>
                    {file ? (
                        <styles.DeleteBtnWrapper>
                            <styles.DeleteBtn onClick={(): void => setTempFile('')}>
                                Delete
                                <styles.TrashIcon/>
                            </styles.DeleteBtn>
                        </styles.DeleteBtnWrapper>
                    ) : null}
                </styles.FileUploader>
            )}
        </Dropzone>
    )
}