import {CVInterface} from "../../../../../interfaces/auth.interface";

export interface CVDropzoneProps {
    file: any;
    setTempFile:  React.Dispatch<React.SetStateAction<any>>;
}