import styled from 'styled-components';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

export const styles = {
    FileUploader: styled.div`
        display: flex;
        ${(props: {isFile: boolean}) => props.isFile ? 'position: relative' : null};
        align-items: center;
        flex-direction: column;
        height: 237px;
        width: 100%;
        ${(props: {isFile: boolean}) => props.isFile ? 'background-color: white' : null};
        margin-top: 20px;
        border-width: ${(props: {isFile: boolean}) => props.isFile ? '1px' : '1.5px'};
        border-style: ${(props: {isFile: boolean}) => props.isFile ? 'solid' : 'dashed'};
        border-color: ${(props: {isFile: boolean}) => props.isFile ? 'rgb(228, 232, 240)' : 'rgb(209, 196, 233)'};
        border-image: initial;
        border-radius: ${(props: {isFile: boolean}) => props.isFile ? '4px' : '6px'};
    `,
    FileUploaded: styled.div`
        display: flex;
        position: relative;
        align-items: center;
        flex-direction: column;
        height: 237px;
        width: 100%;
        background-color: white;
        margin-top: 20px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(228, 232, 240);
        border-image: initial;
        border-radius: 4px;
    `,
    UploadBox: styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        padding-top: 32px;
    `,
    UploadIcon: styled.div`
        height: 138px;
        width: 138px;
        background-color: rgb(250, 250, 250);
        border-radius: 50%;
        & svg {
            display: block;
            width: 110px;
            height: 130px;
            margin: 0px auto;
        }
    `,
    UploadLabel: styled.label`
        font-size: 16px;
        line-height: 22px;
        color: rgb(191, 197, 210);
        padding: 10px;
    `,
    UploadedLabel: styled.label`
        font-size: 16px;
        color: rgb(255, 64, 129);
        font-weight: 600;
        padding-bottom: 2px;
        display: inline-block;
        max-width: 270px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden !important;
    `,
    FileSizeSpan: styled.span`
        color: rgb(46, 56, 77);
        font-size: 16px;
        font-weight: 600;
        line-height: 22px;
    `,
    ColorLabel: styled.span`
        cursor: pointer;
        color: rgb(52, 124, 255);
    `,
    DeleteBtnWrapper: styled.div`
        text-align: right;
        position: absolute;
        bottom: 0px;
        right: 0px;
        margin-right: 32px;
        margin-bottom: 8px;
        width: 100%;
    `,
    DeleteBtn: styled.span`
        margin-right: 6%;
        color: rgb(153, 161, 171);
        font-size: 14px;
        font-weight: 600;
        &:hover {
            cursor: pointer;
        }
    `,
    TrashIcon: styled(DeleteOutlineIcon)`
        vertical-align: middle;
        font-size: 28px;
    `
}