import styled from "styled-components";

export const styles = {
    AvatarUploadWrapper: styled.div`
        margin-top: 40px;
        margin-bottom: 40px;
    `,
    AvatarUpload: styled.div`
        box-shadow: rgba(0, 0, 0, 0.08) 0px 6px 13px 0px;
        color: white;
        position: relative;
        text-align: center;
        flex-shrink: 0;
        display: flex;
        align-items: center;
        width: 130px;
        height: 130px;
        border-width: 3px;
        border-style: solid;
        border-color: white;
        border-image: initial;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        background: ${(props: {img: string | ArrayBuffer | null}) => `url(${props.img})`};
        background-size: 150px;
        background-repeat: no-repeat;
    `,
    ImgInput: styled.input`
        display: none;
    `,
    IconWrapper: styled.div`
        height: 34px;
        width: 100%;
        opacity: 0.63;
        background-color: rgb(51, 51, 51);
        position: absolute;
        text-align: center;
        cursor: pointer;
        bottom: 0px;
        left: 0px;
    `,
    Description: styled.p`
        font-weight: 400;
    `,
}

export const muiStyles = {
    header: {
        color: 'rgb(55, 71, 79)',
        margin: '30px 0px',
    },
    desc: {
        marginTop: '8px',
        fontSize: '23px',
        fontWeight: 400,
        color: 'rgb(153, 161, 171)',
    },
    section: {
        marginBottom: '22px',
        padding: '20px',
        background: 'white',
    },
    grid: {
        display: 'flex',
        alignItems: 'flex-start',
    },
    gridItem: {
        textAlign: 'center',
    },
    icon: {
        width: '32px',
        marginTop: '12px',
    },
    expBtn: {
        textAlign: 'center',
        minHeight: '75px',
        display: 'block',
        width: '100%',
        color: 'rgb(135, 144, 150)',
        background: 'white',
        borderWidth: '2px',
        borderStyle: 'solid',
        borderColor: 'rgb(228, 232, 240)',
        borderImage: 'initial',
        borderRadius: '4px',
        '&:hover': {
            background: 'rgba(0, 0, 0, 0.04)',
        },
        '& strong': {
            display: 'block',
            fontWeight: 400,
            fontSize: '22px',
        },
    },
    expBtnActive: {
        textAlign: 'center',
        minHeight: '75px',
        display: 'block',
        width: '100%',
        color: 'rgb(255, 64, 129)',
        background: 'rgba(255, 64, 129, 0.08)',
        borderWidth: '2px',
        borderStyle: 'solid',
        borderColor: 'rgb(255, 64, 129)',
        borderImage: 'initial',
        borderRadius: '4px',
        '&:hover': {
            background: 'rgba(0, 0, 0, 0.04)',
        },
        '& strong': {
            display: 'block',
            fontWeight: 400,
            fontSize: '22px',
        },
    },
    socialSection: {
        display: 'flex',
        alignItems: 'flex-start',
    },
    imgIcon: {
        color: 'white',
        fontSize: '21px',
        marginTop: '5px',
    }
}