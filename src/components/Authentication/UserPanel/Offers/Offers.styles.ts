import styled from "styled-components";
import {Link} from "react-router-dom";

export const styles = {
    Content: styled.div`
        display: flex;
        flex-direction: column;
        min-height: 50px;
        background-color: white;
        margin: 0 10px 12px;
        padding: 0;
        overflow: hidden;
        box-shadow: rgba(0,0,0,0.05) 0 2px 2px 0, rgba(0,0,0,0.04) 0 1px 5px 0;
        border-radius: 6px;
    `,
    TitlesRow: styled.div`
        display: flex;
        width: 100%;
        margin: 15px 0px;
    `,
    Title: styled.div`
        display: flex;
        width: 18%;
        font-weight: 600;
        justify-content: center;
        font-size: 13px;
    `,
    TitleDelete: styled.div`
        display: flex;
        width: 10%;
        align-items: center;
        font-weight: 600;
        font-size: 13px;
        justify-content: center;
    `,
    OfferRow: styled.div`
        display: flex;
        width: 100%;
        margin-bottom: 4px;
    `,
    OfferRowContent: styled.div`
        display: flex;
        width: 18%;
        justify-content: center;
        align-items: center;
        font-size: 13px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `,
    OfferRowDeleteBtn: styled.div`
        display: flex;
        width: 10%;
        justify-content: center;
        font-size: 13px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `,
    MsgWrapper: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin: 20px auto;
    `,
    StyledLink: styled(Link)`
        text-decoration: none;
        color: #ff4081;
    `,
}

export const muiStyles = {
    header: {
        color: 'rgb(55, 71, 79)',
        margin: '30px 0px',
    },
}