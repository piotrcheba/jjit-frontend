export interface AdminPanelOfferInterface {
    id: string | undefined;
    companyName: string;
    title: string;
    salary: string;
    createdById: string;
    link: string;
}