import React, {ReactElement, useEffect, useState} from 'react';
import axios, {AxiosResponse} from 'axios';
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import {styles, muiStyles} from "./Offers.styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from '@material-ui/core/Button';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import {OfferInterface} from "../../../../interfaces/offer.interface";
import {AdminPanelOfferInterface} from "./Offers.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

const LoadingSpinner = (): ReactElement => (
    <styles.MsgWrapper data-testid='myoffers-spinner'>
        <CircularProgress size={80} />
    </styles.MsgWrapper>
)

export default function Offers(): ReactElement {
    const classes = useStyles();

    const [offers, setOffers] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect((): void => {
        setLoading(true);
        axios.get("https://mern-jjit.herokuapp.com/api/offers/")
            .then((res: AxiosResponse<any>): void => {
                const offers = res.data;
                const offersToGo = offers.map((item: OfferInterface): AdminPanelOfferInterface => {
                    return {
                        id: item._id,
                        companyName: item.companyName,
                        title: item.title,
                        salary: `${item.salaryFrom} - ${item.salaryTo} ${item.currency}`,
                        createdById: item.createdBy,
                        link: `/offers/${item.lower_name}-${item.lower_title}`
                    }
                })
                setOffers(offersToGo);
                setLoading(false);
            });
    }, [])

    const offerRemover = (id: string): void => {
        axios.delete(`https://mern-jjit.herokuapp.com/api/offers/${id}`)
            .then((res: AxiosResponse<any>): void => {
                const indexToRemove = offers.findIndex((el: AdminPanelOfferInterface) => el.id === id);
                const offersToUpdate = [...offers];

                offersToUpdate.splice(indexToRemove, 1);

                setOffers(offersToUpdate);
            })
            .catch(err => {
                console.log(err);
            })
    }

    return (
        <>
            {loading ? <LoadingSpinner/> : <>
                    <Typography
                        className={classes.header}
                        variant="h1"
                    >
                        All offers
                    </Typography>
                    <styles.Content>
                        <styles.TitlesRow>
                            <styles.Title>
                                Company name
                            </styles.Title>
                            <styles.Title>
                                Title
                            </styles.Title>
                            <styles.Title>
                                Salary
                            </styles.Title>
                            <styles.Title>
                                Created by
                            </styles.Title>
                            <styles.Title>
                                Link
                            </styles.Title>
                            <styles.TitleDelete>
                                Actions
                            </styles.TitleDelete>
                        </styles.TitlesRow>
                        {offers.map((item: AdminPanelOfferInterface): ReactElement => {
                            return <styles.OfferRow>
                                <styles.OfferRowContent>
                                    {item.companyName}
                                </styles.OfferRowContent>
                                <styles.OfferRowContent>
                                    {item.title}
                                </styles.OfferRowContent>
                                <styles.OfferRowContent>
                                    {item.salary}
                                </styles.OfferRowContent>
                                <styles.OfferRowContent>
                                    {item.createdById}
                                </styles.OfferRowContent>
                                <styles.OfferRowContent>
                                    <styles.StyledLink to={item.link}>
                                        Link
                                    </styles.StyledLink>
                                </styles.OfferRowContent>
                                <styles.OfferRowDeleteBtn>
                                    <Button
                                        disableElevation
                                        disableFocusRipple
                                        disableRipple
                                        size='small'
                                        onClick={(): void => offerRemover(item.id!)}
                                    >
                                        <DeleteOutlineIcon/>
                                    </Button>
                                </styles.OfferRowDeleteBtn>
                            </styles.OfferRow>
                        })}
                    </styles.Content>
                </>
            }
        </>
    )
}