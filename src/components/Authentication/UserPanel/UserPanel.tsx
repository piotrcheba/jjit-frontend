import React, {ReactElement, useEffect} from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import {styles} from './UserPanel.styles';
import Sidebar from './Sidebar/Sidebar';
import MyProfile from "./MyProfile/MyProfile";
import MyOffers from "./MyOffers/MyOffers";
import Settings from "./Settings/Settings";
import {useDispatch, useSelector} from "react-redux";
import {fetchMyOffers} from "../../../redux/actions/authActions";
import Offers from "./Offers/Offers";
import {RootState} from "../../../redux/store";
import {UserInterface} from "../../../interfaces/auth.interface";

export default function UserPanel(): ReactElement {
    const user = useSelector((state: RootState): UserInterface | undefined => state.auth.user);

    const dispatch = useDispatch();

    useEffect((): void => {
        if (user?.accType === 'emp') dispatch(fetchMyOffers(user?.id!))
    }, [])

    return (
        <styles.Wrapper>
            <Sidebar/>
            <styles.MainWrapper>
                <styles.MainContent>
                    <Switch>
                        {user?.accType === 'emp' ? <Route path="/user_panel/offers" component={MyOffers}/> : null}
                        {user?.accType === 'admin' ? <Route path="/user_panel/alloffers" component={Offers}/> : null}
                        {/*<Route path="/user_panel/settings" component={Settings}/>*/}
                        <Route path="/user_panel/" component={MyProfile}/>
                        <Redirect to="/user_panel/"/>
                    </Switch>
                </styles.MainContent>
            </styles.MainWrapper>
        </styles.Wrapper>
    )
}