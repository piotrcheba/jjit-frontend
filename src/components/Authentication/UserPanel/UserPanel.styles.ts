import styled from "styled-components";

export const styles = {
    Wrapper: styled.div`
        display: flex;
        min-height: calc(100vh - 68px);
        background: rgb(243, 246, 248);
    `,
    MainWrapper: styled.main`
        flex-grow: 1;
        position: relative;
        max-width: calc(100% - 220px);
        padding: 20px;
        @media (max-width: 1919px) {
            padding-bottom: 100px;
        }
    `,
    MainContent: styled.div`
        max-width: 1280px;
        margin: 0px auto;
    `,
}