import styled from "styled-components";

export const styles = {
    AppRow: styled.div`
        display: flex;
        width: 100%;
        margin-bottom: 4px;
    `,
    AppRowContent: styled.div`
        display: flex;
        width: 25%;
        justify-content: center;
        font-size: 13px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `,
    StyledLink: styled.a`
        text-decoration: none;
        color: #ff4081;
    `,
}