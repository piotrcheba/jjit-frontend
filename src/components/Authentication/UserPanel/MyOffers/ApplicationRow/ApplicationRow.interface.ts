export interface ApplicationRowProps {
    name: string,
    email: string,
    about?: string,
    cv?: string,
}