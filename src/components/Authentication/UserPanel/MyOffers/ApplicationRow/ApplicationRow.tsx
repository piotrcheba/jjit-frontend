import React, {ReactElement} from 'react';
import {styles} from "./ApplicationRow.styles";
import {ApplicationRowProps} from "./ApplicationRow.interface";

export default function ApplicationRow({name, about, cv, email}: ApplicationRowProps): ReactElement {
    return (
        <styles.AppRow>
            <styles.AppRowContent>
                {name}
            </styles.AppRowContent>
            <styles.AppRowContent>
                {email}
            </styles.AppRowContent>
            <styles.AppRowContent data-testid='approw-aboutcol'>
                {about || '-'}
            </styles.AppRowContent>
            <styles.AppRowContent data-testid='approw-cvcol'>
                {cv ? (
                    <styles.StyledLink href={cv} target="_blank" data-testid='approw-cvlink'>
                        Link
                    </styles.StyledLink>
                ): '-'}
            </styles.AppRowContent>
        </styles.AppRow>
    )
}