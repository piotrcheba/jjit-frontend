import React, {ReactElement} from 'react';
import {ApplicationsProps} from "./Applications.interface";
import {styles} from "./Applications.styles";

export default function Applications({children}: ApplicationsProps): ReactElement {
    return (
        <styles.Applications data-testid='applications-wrapper'>
            <styles.AppHeaderContainer>
                <styles.AppHeader>
                    Applications
                </styles.AppHeader>
            </styles.AppHeaderContainer>
            <styles.AppContent>
                <styles.RowTitles>
                    <styles.RowTitle>
                        Name
                    </styles.RowTitle>
                    <styles.RowTitle>
                        Email
                    </styles.RowTitle>
                    <styles.RowTitle>
                        About
                    </styles.RowTitle>
                    <styles.RowTitle>
                        CV
                    </styles.RowTitle>
                </styles.RowTitles>
                {children}
            </styles.AppContent>
        </styles.Applications>
    )
}