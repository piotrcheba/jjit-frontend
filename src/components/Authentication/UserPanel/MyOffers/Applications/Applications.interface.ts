import {ReactElement} from "react";

export interface ApplicationsProps {
    children: ReactElement[]
}