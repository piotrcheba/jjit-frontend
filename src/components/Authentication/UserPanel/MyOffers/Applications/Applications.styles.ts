import styled from "styled-components";

export const styles = {
    Applications: styled.div`
        display: flex;
        flex-direction: column;
        min-height: 50px;
        background-color: white;
        margin: 0 20px 12px;
        padding: 0;
        overflow: hidden;
        box-shadow: rgba(0,0,0,0.05) 0 2px 2px 0, rgba(0,0,0,0.04) 0 1px 5px 0;
        border-radius: 6px;
    `,
    AppHeaderContainer: styled.div`
        display: flex;
        width: 100%;
        justify-content: flex-start;
        line-height: 20px;
        font-weight: 600;
        overflow: hidden;
        border-bottom: 1px solid rgba(0, 0, 0, 0.08)
    `,
    AppHeader: styled.div`
        display: flex;
        margin: 10px 30px;
    `,
    AppContent: styled.div`
        display: flex;
        flex-direction: column;
        margin: 6px 20px
    `,
    RowTitles: styled.div`
        display: flex;
        width: 100%;
        margin-bottom: 10px;
    `,
    RowTitle: styled.div`
        display: flex;
        width: 25%;
        font-weight: 600;
        justify-content: center;
        font-size: 13px;
    `,
}