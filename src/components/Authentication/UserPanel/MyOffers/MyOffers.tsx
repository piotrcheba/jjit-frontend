import React, {ReactElement} from 'react';
import {styles, muiStyles} from './MyOffers.styles';
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {Link} from "@material-ui/core";
import {useSelector} from "react-redux";
import ListItem from "../../../JobBoard/List/ListItems/ListItem/ListItem";
import CircularProgress from "@material-ui/core/CircularProgress";
import Applications from "./Applications/Applications";
import ApplicationRow from "./ApplicationRow/ApplicationRow";
import {RootState} from "../../../../redux/store";
import {AuthStateInterface} from "../../../../interfaces/auth.interface";
import {ApplicationInterface, OfferInterface} from "../../../../interfaces/offer.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

const LoadingSpinner = (): ReactElement => (
    <styles.MsgWrapper data-testid='myoffers-spinner'>
        <CircularProgress size={80} />
    </styles.MsgWrapper>
)

const NoOffersMsg = (): ReactElement => (
    <styles.NoOffers data-testid='myoffers-nooffers'>
        <styles.Icon/>
        <styles.Text>
            You have no active offers
        </styles.Text>
        <Typography
            variant="subtitle2"
            gutterBottom
        >
            <Link href='/addoffer' color='primary' underline="hover">
                Add one now!
            </Link>
        </Typography>
    </styles.NoOffers>
)

export default function MyOffers(): ReactElement {
    const classes = useStyles();

    const auth = useSelector((state: RootState): AuthStateInterface => state.auth)

    return (
        <>
            <Typography
                className={classes.header}
                variant="h1"
            >
                My job offers
            </Typography>
            {auth.loading ? <LoadingSpinner /> : auth.offers.length > 0 ? (
                auth.offers.map((item: OfferInterface): ReactElement => (
                        <>
                            <ListItem {...item} />
                            {item.applications!.length > 0 ? (
                                <Applications>
                                    {item.applications!.map((el: ApplicationInterface): ReactElement => (
                                        <ApplicationRow
                                            key={el.name}
                                            name={el.name}
                                            email={el.email}
                                            about={el.about}
                                            cv={el.cv.url}
                                        />
                                    ))}
                                </Applications>
                            ) : null}
                        </>
                    )
                )
            ) : <NoOffersMsg/>}
        </>
    )
}