import React, {ReactElement} from 'react';
import {styles} from './Sidebar.styles';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import ListItemText from "@material-ui/core/ListItemText";
import SettingsOutlinedIcon from "@material-ui/icons/SettingsOutlined";
import PowerSettingsNewOutlinedIcon from "@material-ui/icons/PowerSettingsNewOutlined";
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import {Link, NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../../../redux/actions/authActions";
import {RootState} from "../../../../redux/store";
import {UserInterface} from "../../../../interfaces/auth.interface";

export default function Sidebar(): ReactElement {
    const user = useSelector((state: RootState): UserInterface | undefined => state.auth.user);

    const dispatch = useDispatch();

    const logOutHandler = (): void => {
        dispatch(logoutUser());
    }

    return (
        <>
            <styles.Wrapper>
                <styles.SidePanel>
                    <styles.Avatar img={user?.avatarUrl!} />
                    <styles.Welcome>
                        Welcome
                        <span data-testid='sidebar-welcomespan'>
                            {user?.name}
                        </span>
                    </styles.Welcome>
                    <List component="nav">
                        <ListItem
                            button
                            component={NavLink}
                            to="/user_panel/profile"
                        >
                            <ListItemIcon>
                                <PersonOutlineIcon/>
                            </ListItemIcon>
                            <ListItemText primary="My profile"/>
                        </ListItem>
                        {user?.accType === 'emp' ?
                            <ListItem
                                data-testid='sidebar-myoffers'
                                button
                                component={NavLink}
                                to="/user_panel/offers"
                            >
                                <ListItemIcon>
                                    <AssignmentOutlinedIcon/>
                                </ListItemIcon>
                                <ListItemText primary="My job offers"/>
                            </ListItem>
                            : null
                        }
                        {user?.accType === 'admin' ?
                            <ListItem
                                button
                                component={NavLink}
                                to="/user_panel/alloffers"
                            >
                                <ListItemIcon>
                                    <AssignmentOutlinedIcon/>
                                </ListItemIcon>
                                <ListItemText primary="All offers"/>
                            </ListItem>
                            : null
                        }
                        {/*<ListItem*/}
                        {/*    button*/}
                        {/*    component={NavLink}*/}
                        {/*    to="/user_panel/settings"*/}
                        {/*>*/}
                        {/*    <ListItemIcon>*/}
                        {/*        <SettingsOutlinedIcon/>*/}
                        {/*    </ListItemIcon>*/}
                        {/*    <ListItemText primary="Settings"/>*/}
                        {/*</ListItem>*/}
                        <ListItem
                            button
                            component={Link}
                            to="/"
                            onClick={logOutHandler}
                        >
                            <ListItemIcon>
                                <PowerSettingsNewOutlinedIcon/>
                            </ListItemIcon>
                            <ListItemText primary="Log out"/>
                        </ListItem>
                    </List>
                </styles.SidePanel>
            </styles.Wrapper>
        </>
    )
}