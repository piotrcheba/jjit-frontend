import styled from "styled-components";

export const styles = {
    Wrapper: styled.aside`
        flex-shrink: 0;
        width: 220px;
        text-align: center;
        padding: 20px 0px;
        border-right: 1px solid rgb(229, 234, 243);
        background: white;
    `,
    SidePanel: styled.div`
        position: sticky;
        top: 88px;
    `,
    Avatar: styled.div`
        box-shadow: rgba(0, 0, 0, 0.08) 0px 6px 13px 0px;
        color: rgb(255, 255, 255);
        position: relative;
        text-align: center;
        flex-shrink: 0;
        display: flex;
        align-items: center;
        width: 100px;
        height: 100px;
        border-width: 3px;
        border-style: solid;
        border-color: white;
        border-image: initial;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        background: ${(props: {img: string}) => `url(${props.img})`};
        background-size: 120px;
        background-repeat: no-repeat;
        margin-bottom: 10px;
    `,
    Welcome: styled.div`
        font-size: 14px;
        margin-bottom: 16px;
        & span {
            display: block;
            color: rgb(153, 161, 171);
        }
    `,
}