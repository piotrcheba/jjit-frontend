import React, {ReactElement, useState} from 'react';
import {useDispatch} from "react-redux";
import {loginUser} from "../../../../redux/actions/authActions";
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import {muiStyles} from './EmpsLogin.styles';
import {checkValidity} from "../../../../utils/checkValidity";
import {EmpsLoginProps} from "./EmpsLogin.interface";
import {LoginFormInterface} from "../../../../interfaces/authForm.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function EmpsLogin({history}: EmpsLoginProps): ReactElement {
    const classes = useStyles();

    const [credentials, setCredentials] = useState<LoginFormInterface>({
        email: {
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password: {
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false
        }
    });

    const [formIsValid, setFormIsValid] = useState<boolean>(false);

    const dispatch = useDispatch();

    const inputChangedHandler = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, inputIdentifier: keyof typeof credentials): void => {
        const updatedForm = {
            ...credentials,
            [inputIdentifier]: {
                ...credentials[inputIdentifier],
                value: event.target.value,
                valid: checkValidity(event.target.value, credentials[inputIdentifier].validation),
                touched: true
            }
        }

        let formIsValid = true;

        for (let key in updatedForm) {
            // @ts-ignore
            formIsValid = credentials[key].valid && formIsValid
        }

        setFormIsValid(formIsValid);
        setCredentials(updatedForm);
    }

    const onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        const userData = {
            email: credentials.email.value,
            password: credentials.password.value
        }
        dispatch(loginUser(userData));
        history.push('/');
    }

    return (
        <>
            <form onSubmit={onSubmit}>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={2} className={classes.grid}>
                        <EmailIcon/>
                    </Grid>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Email"
                            type="email"
                            error={!credentials.email.valid && credentials.email.touched}
                            helperText={!credentials.email.valid && credentials.email.touched ? "Incorrect email" : " "}
                            color="secondary"
                            className={classes.label}
                            onChange={e => inputChangedHandler(e, 'email')}
                            value={credentials.email.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={2} className={classes.grid}>
                        <LockIcon/>
                    </Grid>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Password"
                            type="password"
                            error={!credentials.password.valid && credentials.password.touched}
                            helperText={!credentials.password.valid && credentials.password.touched ? "Field is required" : " "}
                            color="secondary"
                            className={classes.label}
                            onChange={e => inputChangedHandler(e, 'password')}
                            value={credentials.password.value}
                        />
                    </Grid>
                </Grid>
                <Fab
                    className={classes.btn}
                    disabled={!formIsValid}
                    color="primary"
                    variant="extended"
                    type="submit"
                >
                    SIGN IN
                </Fab>
            </form>
            <Typography
                variant="subtitle2"
                gutterBottom
            >
                {"New account? "}
                <Link href='/emps/register' color='primary' underline="hover">
                    Register
                </Link>
            </Typography>
        </>
    )
}