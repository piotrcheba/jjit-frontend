export const muiStyles = {
    btn: {
        width: '100%',
        height: '36px',
        margin: '20px 0 30px 0',
        fontWeight: 400,
    },
    grid: {
        '&&': {
            paddingBottom: '0',
            paddingRight: '0',
            marginRight: '-6px'
        }
    },
    label: {
        '& > label': {
            paddingBottom: '10px',
            paddingLeft: '6px'
        }
    }
}