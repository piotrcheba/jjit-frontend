import styled from "styled-components";

export const styles = {
    Wrapper: styled.div`
        height: calc(100vh - 68px);
        background-color: #eee;
    `,
    Container: styled.div`
        width: 300px;
        text-align: center;
        padding: 30px;
        margin: 0.5rem 0 1rem 0;
        background-color: #fff;
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translate(-50%, -50%);
        transition: box-shadow .25s;
        border-radius: 2px;
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    `,
    HeaderLogo: styled.div`
        margin: 10px 0 30px 0;
        text-align: center;
        & svg {
            fill: #37474f;
            height: 34px;
            width: 170px;
        }
    `,
}