import React, {ReactElement, useState} from 'react';
import {useDispatch} from "react-redux";
import {registerUser} from "../../../../redux/actions/authActions";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import {makeStyles} from "@material-ui/core/styles";
import {muiStyles} from "./EmpsRegister.styles";
import {checkValidity} from "../../../../utils/checkValidity";
import {EmpsRegisterProps} from "./EmpsRegister.interface";
import {RegisterFormInterface} from "../../../../interfaces/authForm.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles);

export default function EmpsRegister({history}: EmpsRegisterProps): ReactElement {
    const classes = useStyles();

    const [user, setUser] = useState<RegisterFormInterface>({
        name: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        },
        surname: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        },
        email: {
            value: '',
            validation: {
                required: true,
                isEmail: true
            },
            valid: false,
            touched: false
        },
        password: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        },
        password2: {
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false
        }
    });

    const dispatch = useDispatch();

    const inputChangedHandler = (event: React.ChangeEvent<HTMLInputElement>, inputIdentifier: keyof typeof user): void => {
        const updatedForm = {
            ...user,
            [inputIdentifier]: {
                ...user[inputIdentifier],
                value: event.target.value,
                valid: checkValidity(event.target.value, user[inputIdentifier].validation),
                touched: true
            }
        }

        setUser(updatedForm);
    }

    const onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        const userData = {
            accType: 'emp',
            name: user.name.value,
            surname: user.surname.value,
            email: user.email.value,
            password: user.password.value,
            password2: user.password2.value
        }

        dispatch(registerUser(userData));
        history.push('/emps')
    }

    return (
        <>
            <form onSubmit={onSubmit}>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Name"
                            type="text"
                            error={!user.name.valid && user.name.touched}
                            helperText={!user.name.valid && user.name.touched ? "Name is required" : " "}
                            color="secondary"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'name')}
                            value={user.name.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Surname"
                            type="text"
                            error={!user.surname.valid && user.surname.touched}
                            helperText={!user.surname.valid && user.surname.touched ? "Surname is required" : " "}
                            color="secondary"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'surname')}
                            value={user.surname.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Email"
                            type="email"
                            error={!user.email.valid && user.email.touched}
                            helperText={!user.email.valid && user.email.touched ? "Incorrect email" : " "}
                            color="secondary"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'email')}
                            value={user.email.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Password (min. 6 characters)"
                            type="password"
                            error={!user.password.valid && user.password.touched}
                            helperText={!user.password.valid && user.password.touched ? "Password is required & has to be min. 6 characters" : " "}
                            color="secondary"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'password')}
                            value={user.password.value}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2} alignItems='center'>
                    <Grid item xs={true}>
                        <TextField
                            fullWidth
                            label="Confirm password"
                            type="password"
                            error={!user.password2.valid && user.password2.touched}
                            helperText={!user.password2.valid && user.password2.touched ? "Field is required" : " "}
                            color="secondary"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => inputChangedHandler(e, 'password2')}
                            value={user.password2.value}
                        />
                    </Grid>
                </Grid>
                <Fab
                    className={classes.btn}
                    // disabled={!user.}
                    color="primary"
                    variant="extended"
                    type="submit"
                >
                    SIGN UP
                </Fab>
            </form>
            <Typography
                variant="subtitle2"
                gutterBottom
            >
                {"Already registered? "}
                <Link href='/emps' color='primary' underline="hover">
                    Sign in
                </Link>
            </Typography>
        </>
    )
}