import React, {ReactElement} from 'react';
import {Route, Switch} from "react-router-dom";
import EmpsLogin from "./EmpsLogin/EmpsLogin";
import EmpsRegister from "./EmpsRegister/EmpsRegister";
import Logo from "../../Logo/Logo";
import {styles} from './EmpsAuth.styles';

export default function EmpsAuth(): ReactElement {
    return (
        <styles.Wrapper>
            <styles.Container>
                <styles.HeaderLogo>
                    <Logo/>
                </styles.HeaderLogo>
                <Switch>
                    <Switch>
                        <Route
                            path="/emps/register"
                            component={EmpsRegister}
                        />
                        <Route
                            path="/emps"
                            component={EmpsLogin}
                            exact
                        />
                    </Switch>
                </Switch>
            </styles.Container>
        </styles.Wrapper>
    )
}