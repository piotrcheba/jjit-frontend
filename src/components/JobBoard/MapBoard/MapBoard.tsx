import React, {ReactElement} from 'react';
import {useSelector} from "react-redux";
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';
import {apiConfig} from "../../../config/api";
import {MapBoardProps} from "./MapBoard.interface";
import {RootState} from "../../../redux/store";
import {OfferInterface} from "../../../interfaces/offer.interface";

const mapStyles = {
    width: '100%',
    height: '100%',
};

const MapBoard = React.memo(({google}: MapBoardProps): ReactElement => {
    const offers = useSelector((state: RootState): OfferInterface[] => state.offers.offers)

    const markers = offers.map((item: OfferInterface): ReactElement => (
        <Marker
            position={item.geoLoc}
            icon={{
                url: require(`../../../assets/markers/png/${item.mainTech}.png`),
                scaledSize: new google.maps.Size(40,40),
                anchor: new google.maps.Point(20, 20)}}
        />
    ));

    return (
        <Map
            google={google}
            zoom={6}
            mapTypeControl={false}
            fullscreenControl={false}
            // @ts-ignore
            style={mapStyles}
            initialCenter={{
                lat: 51.1,
                lng: 18.9488
            }}
        >
            {markers}
        </Map>
    )
})

export default GoogleApiWrapper({
    apiKey: apiConfig.GOOGLE_API_KEY
})(MapBoard);