import React, {ReactElement} from 'react';
import Sortbar from "./Sortbar/Sortbar";
import ListItems from "./ListItems/ListItems";
import {styles} from './List.styles';

export default function List(): ReactElement {
    return(
        <styles.ListInner>
            {/*<Sortbar/>*/}
            <ListItems/>
        </styles.ListInner>
    )
};