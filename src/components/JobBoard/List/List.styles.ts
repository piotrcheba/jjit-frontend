import styled from "styled-components";

export const styles = {
    ListInner: styled.div`
        display: flex;
        flex-direction: column;
        flex: 1 1 0%;
    `
}