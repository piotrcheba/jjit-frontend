import styled from "styled-components";
import {Link} from "react-router-dom";

export const styles = {
    StyledWrapper: styled.div`
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;
        background-color: white;
        height: 40px;
        margin-bottom: 8px;
        overflow: hidden;
        @media (min-width: 1025px) {
            height: 40px;
            margin-bottom: 8px;
        }
    `,
    SalaryFilter: styled.div`
        display: flex;
        height: 40px;
        padding-left: 20px;
        flex: 1 1 0%;
        overflow: hidden;
        @media (min-width: 1025px) {
            height: 40px;
            padding-left: 30px;
            a:not(:first-of-type) {
                margin-left: 40px;
            }
        }
    `,
    WithSalaryLink: styled(Link)`
        display: block;
        max-width: 50%;
        height: 100%;
        position: relative;
        text-decoration: none;
        background: white;
        border-radius: 20px 20px 0px 0px;
        @media (min-width: 1025px) {
            border-radius: 20px 20px 0px 0px;
        }
    `,
    AllOffersLink: styled(Link)`
        display: block;
        max-width: 50%;
        height: 100%;
        position: relative;
        z-index: 3;
        text-decoration: none;
        border-radius: 20px 20px 0px 0px;
        background: rgb(243, 246, 248);
        @media (min-width: 1025px) {
            border-radius: 20px 20px 0px 0px;
        }
    `,
    TextWrapper: styled.div`
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        color: rgb(119, 119, 119);
        height: 100%;
        font-weight: 800;
        z-index: 2;Filter
        padding: 0 20px;
        @media (min-width: 1025px) {
            padding: 0 30px;
        }
    `,
    StyledText: styled.div`
        white-space: nowrap;
        text-overflow: ellipsis;
        margin-top: 4px;
        overflow: hidden;
        @media (min-width: 1025px) {
            margin-top: 4px;
        }
    `,
    DateFilter: styled.div`
        display: none;
        @media (min-width: 1025px) {
            display: block;
        }
    `,
    StyledLightSpan: styled.span`
        margin-right: 8px;
        color: rgb(153, 161, 171);
    `,
    StyledSpan: styled.span`
        text-transform: lowercase;
    `,
    SalarySort: styled.div`
        padding: 6px 0px;
    `,
}

export const muiStyles = {
    expandBtn: {
        textTransform: 'none',
        fontSize: '14px',
        fontWeight: 600,
        marginRight: '16px',
        color: 'rgb(119, 119, 119)',
        '&:hover': {
            backgroundColor: 'transparent',
        }
    },
    popoverBtn: {
        display: 'block',
        textTransform: 'lowercase',
        textAlign: 'left',
        fontSize: '14px',
        fontWeight: 600,
        color: 'rgb(119, 119, 119)',
        padding: '6px 16px',
        borderRadius: '0px',
    }
}