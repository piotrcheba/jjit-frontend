import React, {ReactElement, useState} from 'react';
import Button from "@material-ui/core/Button";
import Popover from '@material-ui/core/Popover';
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {makeStyles} from "@material-ui/core/styles";
import {styles, muiStyles} from './Sortbar.styles';
import {sortByHighestSalary, sortByLatest, sortByLowestSalary} from "../../../../redux/actions/offerActions";
import {useDispatch} from "react-redux";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

export default function Sortbar(): ReactElement {
    const classes = useStyles();
    const [showSortPop, toggleSortPop] = useState(false);

    const [displayValue, setDisplayValue] = useState('latest')

    const dispatch = useDispatch();

    const openSortPopHandler = (): void => toggleSortPop(true);
    const closeSortPopHandler = (): void => toggleSortPop(false);

    return (
        <>
            <styles.StyledWrapper>
                <styles.DateFilter>
                    <Button
                        data-testid='sortbar-btn'
                        className={classes.expandBtn}
                        onClick={openSortPopHandler}
                        disableElevation
                        endIcon={<ExpandMoreIcon/>}
                    >
                        <styles.StyledLightSpan>
                            Sort by:
                        </styles.StyledLightSpan>
                        <styles.StyledSpan data-testid='sortbar-displayvalue'>
                            {displayValue}
                        </styles.StyledSpan>
                    </Button>
                </styles.DateFilter>
            </styles.StyledWrapper>
            <Popover
                data-testid='sortbar-popover'
                open={showSortPop}
                onClose={closeSortPopHandler}
                anchorReference="anchorPosition"
                anchorPosition={{ top: 172, left: 722 }}
                transformOrigin={{ vertical: 'top', horizontal: 'left'}}
            >
                <styles.SalarySort>
                    <Button
                        data-testid='sortbar-latestbtn'
                        className={classes.popoverBtn}
                        fullWidth={true}
                        onClick={(): void => {
                            dispatch(sortByLatest());
                            setDisplayValue('latest');
                            closeSortPopHandler();
                        }}
                    >
                        latest
                    </Button>
                    <Button
                        data-testid='sortbar-highestbtn'
                        className={classes.popoverBtn}
                        fullWidth={true}
                        onClick={(): void => {
                            dispatch(sortByHighestSalary());
                            setDisplayValue('highest salary');
                            closeSortPopHandler();
                        }}
                    >
                        highest salary
                    </Button>
                    <Button
                        data-testid='sortbar-lowestbtn'
                        className={classes.popoverBtn}
                        fullWidth={true}
                        onClick={(): void => {
                            dispatch(sortByLowestSalary());
                            setDisplayValue('lowest salary');
                            closeSortPopHandler();
                        }}
                    >
                        lowest salary
                    </Button>
                </styles.SalarySort>
            </Popover>
        </>
    )
};