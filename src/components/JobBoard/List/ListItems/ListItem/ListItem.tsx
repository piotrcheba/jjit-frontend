import React, {ReactElement} from 'react';
import BusinessOutlinedIcon from '@material-ui/icons/BusinessOutlined';
import RoomIcon from '@material-ui/icons/Room';
import {styles} from './ListItem.styles';
import {ListItemProps} from "./ListItem.interface";
import {SkillInterface} from "../../../../../interfaces/offer.interface";

export default function ListItem({date, lower_name, lower_title, mainTech, companyLogo, title, companyName, onlineInterview, currency, fullyRemote, officeCity, salaryFrom, salaryTo, skills}: ListItemProps): ReactElement {
    const dateAdded = Date.parse(date!)

    const diffInDays = (Date.now() - dateAdded) / 86400000

    const offerAdded = () => {
        if (diffInDays < 1) {
            return 'New'
        } else {
            return `${Math.floor(diffInDays)}d ago`
        }
    }

    const isNew = diffInDays < 1;

    return (
        <styles.StyledLink
            data-testid='listitem-linkwrapper'
            to={'/offers/' + lower_name + '-' + lower_title}
        >
            <styles.Colorbar color={mainTech}/>
            <styles.LogoWrapper>
                <styles.Logo
                    alt='CompanyLogo'
                    src={companyLogo}
                />
            </styles.LogoWrapper>
            <styles.InfoWrapper>
                <styles.BoldDetails>
                    <styles.Position>
                        <styles.PositionTitle>
                            {title}
                        </styles.PositionTitle>
                    </styles.Position>
                    <styles.Salary>
                        <styles.SalarySpan>
                            {salaryFrom} - {salaryTo} {currency}
                        </styles.SalarySpan>
                        <styles.DateAdded isNew={isNew} data-testid='listitem-dateadded'>
                            {offerAdded()}
                        </styles.DateAdded>
                    </styles.Salary>
                </styles.BoldDetails>
                <styles.TinyDetails>
                    <styles.CompanyInfo>
                        <styles.CompanyName>
                            <BusinessOutlinedIcon/>
                            {companyName}
                        </styles.CompanyName>
                        <styles.CompanyLoc>
                            <RoomIcon/>
                            {officeCity}
                        </styles.CompanyLoc>
                        {fullyRemote ? (
                            <styles.Remote data-testid='listitem-remotechip'>
                                Remote
                            </styles.Remote>
                        ) : onlineInterview ? <styles.OnlineInterview data-testid='listitem-onlinechip'>
                            Online interview
                        </styles.OnlineInterview> : null}
                    </styles.CompanyInfo>
                    <styles.TechInfo>
                        {skills.slice(0, 3).map((el: SkillInterface): ReactElement => (
                            <styles.TechType>
                                {el.name}
                            </styles.TechType>
                        ))}
                    </styles.TechInfo>
                </styles.TinyDetails>
            </styles.InfoWrapper>
        </styles.StyledLink>
    );
};