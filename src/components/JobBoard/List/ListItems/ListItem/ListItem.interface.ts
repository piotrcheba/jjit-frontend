import {OfferInterface} from "../../../../../interfaces/offer.interface";

export type ListItemProps = Omit<OfferInterface, '_id' | 'applications' | 'companyWebsite' | 'companySize' | 'companyType' | 'companyIndustry' | 'expLevel' | 'empType' | 'jobDesc' | 'officeStreet' | 'geoLoc' | 'contact' | 'createdBy'>