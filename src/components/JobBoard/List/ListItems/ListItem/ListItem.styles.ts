import styled from "styled-components";
import {Link} from "react-router-dom";

export const styles = {
    StyledLink: styled(Link)`
        display: flex;
        box-shadow: rgba(0, 0, 0, 0.05) 0 2px 2px 0, rgba(0, 0, 0, 0.04) 0 1px 5px 0;
        height: 77px;
        background-color: white;
        color: rgba(0, 0, 0, 0.87);
        border-radius: 6px;
        margin: 0 10px 12px;
        padding: 0;
        overflow: hidden;
        text-decoration: none;
        &:hover {
        box-shadow: rgba(0, 0, 0, 0.07) 0 4px 4px 0, rgba(0, 0, 0, 0.06) 0 3px 7px 0;
        transition: box-shadow 0.9s cubic-bezier(0.25, 0.8, 0.25, 1) 0s;
    `,
    Colorbar: styled.div`
        flex-basis: 5px;
        flex-shrink: 0;
        background: ${(props: {color: string}) =>
            props.color === 'javascript' && 'rgb(255, 199, 6)' ||
            props.color === 'html' && 'rgb(255, 125, 80)' ||
            props.color === 'php' && 'rgb(65, 173, 250)' ||
            props.color === 'ruby' && 'rgb(239, 83, 80)' ||
            props.color === 'python' && 'rgb(31, 94, 182)' ||
            props.color === 'java' && 'rgb(250, 101, 107)' ||
            props.color === 'net' && 'rgb(103, 37, 114)' ||
            props.color === 'scala' && 'rgb(248, 100, 104)' ||
            props.color === 'c' && 'rgb(47, 207, 187)' ||
            props.color === 'mobile' && 'rgb(224, 79, 134)' ||
            props.color === 'testing' && 'rgb(0, 150, 136)' ||
            props.color === 'devops' && 'rgb(82, 102, 225)' ||
            props.color === 'ux' && 'rgb(255, 183, 77)' ||
            props.color === 'pm' && 'rgb(128, 203, 196)' ||
            props.color === 'game' && 'rgb(255, 64, 129)' ||
            props.color === 'analytics' && 'rgb(59, 89, 152)' ||
            props.color === 'security' && 'rgb(83, 109, 254)' ||
            props.color === 'data' && 'rgb(137, 219, 84)' ||
            props.color === 'go' && 'rgb(106, 215, 229)' ||
            props.color === 'sap' && 'rgb(2, 175, 235)' ||
            props.color === 'support' && 'rgb(212, 104, 222)' ||
            props.color === 'other' && 'rgb(236, 76, 182)'
        };
    `,
    LogoWrapper: styled.div`
        display: flex;
        flex-basis: 100px;
        flex-shrink: 0;
        justify-content: center;
        align-items: center;
        @media (min-width: 1025px) {
            flex-basis: 125px;
        }
    `,
    Logo: styled.img`
        max-height: 40px;
        max-width: 72px;
        @media (min-width: 1025px) {
            max-width: 85px;
        }
    `,
    InfoWrapper: styled.div`
        display: flex;
        flex-grow: 1;
        min-width: 0;
        flex-direction: column;
        padding: 12px 10px 10px 0;
        @media (min-width: 1025px) {
            padding: 8px 20px 8px 0px;
        }
    `,
    BoldDetails: styled.div`
        color: rgb(55, 71, 79);
        display: flex;
        flex-direction: column;
        align-items: stretch;
        min-width: 0;
        justify-content: space-between;
        flex-grow: 1;
        @media (min-width: 1025px) {
            flex-direction: row;
            align-items: center
        }
    `,
    Position: styled.div`
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
        flex-grow: 1;
        min-width: 0;
    `,
    PositionTitle: styled.div`
        display: -webkit-box;
        font-size: 15px;
        padding-right: 8px;
        line-height: 20px;
        font-weight: 600;
        overflow: hidden;
        @media (min-width: 1025px) {
            display: block;
            margin-top: 0;
            padding-right: 5px;
            font-size: 16px;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
    `,
    Salary: styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
        @media (min-width: 1025px) {
            justify-content: flex-end;
        }
    `,
    SalarySpan: styled.span`
        color: rgb(30, 198, 108);
        font-size: 15px;
        line-height: 23px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        @media (min-width: 1025px) {
            font-size: 16px;
        }
    `,
    DateAdded: styled.div`
        height: 22px;
        margin-left: 16px;
        font-size: 11px;
        line-height: 20px;
        white-space: nowrap;
        color: ${(props: {isNew: boolean}) => props.isNew ? 'rgb(66, 86, 239)' : 'rgb(55, 71, 79)'};
        background-color: ${(props: {isNew: boolean}) => props.isNew ? 'rgb(217, 221, 252)' : 'rgb(255, 255, 255)'};
        padding: 0 7px;
        border-radius: 11px;
        border-width: 1px;
        border-style: solid;
        border-color: ${(props: {isNew: boolean}) => props.isNew ? 'rgb(217, 221, 252)' : 'rgb(224, 224, 224)'};
        border-image: initial;
    `,
    TinyDetails: styled.div`
        display: none;
        justify-content: space-between;
        @media (min-width: 1025px) {
            display: flex;
        }
    `,
    CompanyInfo: styled.div`
        display: flex;
        align-items: center;
        color: rgb(153, 161, 171);
        font-size: 11px;
        & svg {
            width: 14px;
        }
    `,
    CompanyName: styled.div`
        display: flex;
        align-items: center;
        & svg {
            margin-right: 6px;
        }
    `,
    CompanyLoc: styled.div`
        display: flex;
        align-items: center;
        margin: 0 12px;
        & svg {
            margin-right: 4px;
        }
    `,
    OnlineInterview: styled.span`
        display: block;
        text-align: center;
        font-size: 10px;
        line-height: 14px;
        background-color: rgb(225, 245, 254);
        color: rgb(66, 165, 245);
        margin: 0;
        padding: 4px 12px;
        border-radius: 4px;
    `,
    Remote: styled.span`
        display: inline-block;
        height: 21px;
        font-size: 10px;
        line-height: 21px;
        white-space: nowrap;
        background-color: rgb(255, 245, 248);
        color: rgb(255, 64, 129);
        margin: 0px;
        padding: 0px 12px;
        border-radius: 11px;
    `,
    TechInfo: styled.div`
        display: flex;
        white-space: nowrap;
        justify-content: flex-end;
        align-items: flex-end;
        flex: 1 1 0%;
        overflow: hidden;
    `,
    TechType: styled.div`
        height: 22px;
        font-size: 11px;
        line-height: 20px;
        white-space: nowrap;
        background-color: rgb(255, 255, 255);
        color: rgb(153, 161, 171);
        display: inline-block;
        margin-left: 6px;
        text-transform: lowercase;
        text-overflow: ellipsis;
        min-width: 1ch;
        padding: 0 7px;
        border-radius: 11px;
        border-width: 1px;
        border-style: solid;
        border-color: rgb(224, 224, 224);
        border-image: initial;
        overflow: hidden;
    `,
}