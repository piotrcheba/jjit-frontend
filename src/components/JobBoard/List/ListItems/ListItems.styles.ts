import styled from "styled-components";
import {FixedSizeList} from "react-window";
import SentimentDissatisfiedOutlinedIcon from "@material-ui/icons/SentimentDissatisfiedOutlined";

export const styles = {
    Wrapper: styled.div`
        display: flex;
        flex-direction: column;
        flex: 1 1 0%;
    `,
    MsgWrapper: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin: 20px auto;
    `,
    NoOffers: styled.div`
        display: flex; 
        flex-direction: column;
        justifyContent: center;
        align-items: center;
    `,
    Icon: styled(SentimentDissatisfiedOutlinedIcon)`
        width: 100px;
        height: 100px;
        font-size: 80px;
        fill: rgb(255, 64, 129);
    `,
    Text: styled.span`
        padding-top: 10px;
        color: #2E384D;
        font-size: 20px;
        font-family: Open Sans, sans-serif;
        font-weight: 300;
        line-height: 1.167;
        margin-bottom: 10px;
    `,
    StyledFixedSizeList: styled(FixedSizeList)`
        @media (min-width: 1025px) {
            &::-webkit-scrollbar-track {
                width: 12px;
                border-radius: 4px;
                background: rgb(243, 246, 248);
            }
    
            &::-webkit-scrollbar-thumb {
                height: 56px;
                background: rgb(193, 193, 193);
                border-radius: 10px;
            }
    
            &::-webkit-scrollbar {
                width: 12px;
            }
        }
    `,
}