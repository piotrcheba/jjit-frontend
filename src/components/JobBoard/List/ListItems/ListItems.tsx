import React, {ReactElement} from 'react';
import {useSelector} from "react-redux";
import {AutoSizer} from "react-virtualized";
import ListItem from "./ListItem/ListItem";
import {styles} from './ListItems.styles';
import {RootState} from "../../../../redux/store";
import CircularProgress from "@material-ui/core/CircularProgress";
import {OfferInterface} from "../../../../interfaces/offer.interface";

const Row = ({data, index, style}: any): ReactElement => {
    return (
        <div style={style}>
            <ListItem
                {...data.offers[index]}
            />
        </div>
    )
};

const LoadingSpinner = (): ReactElement => (
    <styles.MsgWrapper>
        <CircularProgress size={80} />
    </styles.MsgWrapper>
);

const NoOffersMsg = (): ReactElement => (
    <styles.NoOffers>
        <styles.Icon/>
        <styles.Text>
            Sorry, currently there are no offers with specified criteria
        </styles.Text>
    </styles.NoOffers>
)

export default function ListItems(): ReactElement {
    const offers = useSelector((state: RootState): OfferInterface[] => state.offers.offers);
    const isLoading = useSelector((state: RootState): boolean => state.offers.loading);

    return (
        <>
            {isLoading ? <LoadingSpinner/> : offers.length > 0 ? (
                <styles.Wrapper>
                    <AutoSizer>
                        {({ height, width }) => (
                            <styles.StyledFixedSizeList
                                height={height}
                                itemCount={offers.length}
                                itemSize={89}
                                width={width}
                                itemData={{offers: offers}}
                            >
                                {Row}
                            </styles.StyledFixedSizeList>
                        )}
                    </AutoSizer>
                </styles.Wrapper>
            ) : <NoOffersMsg />}
        </>
    )
};