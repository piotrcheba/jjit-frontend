import styled from "styled-components";

export const styles = {
    StyledWrapper: styled.div`
        display: flex;
        flex: 1 1 0%;
    `,
    ListWrapper: styled.div`
        display: flex;
        flex-direction: column;
        padding-top: 0px;
        flex: 0 0 100%;
        background: rgb(243, 246, 248);
        @media (min-width: 1025px) {
            display: flex;
            flex: 0 0 60%;
        };
        @media (min-width: 1500px) {
            display: flex;
            flex: 0 0 50%;
        };
    `,
    MapWrapper: styled.div`
        display: none;
        position: relative;
        background: rgb(243, 246, 248);
        flex: 0 0 100%;
        @media (min-width: 1025px) {
            display: flex;
            flex: 0 0 40%
        };
        @media (min-width: 1500px) {
            flex: 0 0 50%
        };
    `,
}