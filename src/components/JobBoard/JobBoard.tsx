import React, {ReactElement, useEffect} from 'react';
import { Route, Switch } from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {fetchOffers} from "../../redux/actions/offerActions";
import List from './List/List';
import MapBoard from './MapBoard/MapBoard';
import Filterbar from "../Filterbar/Filterbar";
import Offer from "../Offer/Offer";
import {styles} from './JobBoard.styles';
import {RootState} from "../../redux/store";
import {FilterStateInterface} from "../../interfaces/filter.interface";

export default function JobBoard(): ReactElement {
    const filters = useSelector((state: RootState): FilterStateInterface => state.filter);

    const dispatch = useDispatch();

    useEffect((): void => {
        const filtersToFetch = {
            location: filters.location,
            tech: filters.tech,
            salaryFrom: filters.salary[0],
            salaryTo: filters.salary[1],
            seniority: filters.seniority
        }
        dispatch(fetchOffers(filtersToFetch));
    }, [filters])

    return (
        <>
            <Filterbar />
            <styles.StyledWrapper id='jobboard'>
                <styles.ListWrapper>
                    <Switch>
                        <Route
                            path='/offers/:companyName?-:title?'
                            component={Offer}
                        />
                        <Route
                            path='/'
                            component={List}
                        />
                    </Switch>
                </styles.ListWrapper>
                <styles.MapWrapper>
                    <MapBoard/>
                </styles.MapWrapper>
            </styles.StyledWrapper>
        </>
    )
};