import React, {ReactElement} from 'react';
import {IconProps} from "./Icon.interface";
import {ReactComponent as Analytics} from '../../assets/icons/analytics.svg';
import {ReactComponent as C} from '../../assets/icons/c.svg';
import {ReactComponent as Data} from '../../assets/icons/data.svg';
import {ReactComponent as Devops} from '../../assets/icons/devops.svg';
import {ReactComponent as Game} from '../../assets/icons/game.svg';
import {ReactComponent as Go} from '../../assets/icons/go.svg';
import {ReactComponent as Html} from '../../assets/icons/html.svg';
import {ReactComponent as Java} from '../../assets/icons/java.svg';
import {ReactComponent as Javascript} from '../../assets/icons/javascript.svg';
import {ReactComponent as Mobile} from '../../assets/icons/mobile.svg';
import {ReactComponent as Net} from '../../assets/icons/net.svg';
import {ReactComponent as Other} from '../../assets/icons/other.svg';
import {ReactComponent as Php} from '../../assets/icons/php.svg';
import {ReactComponent as Pm} from '../../assets/icons/pm.svg';
import {ReactComponent as Python} from '../../assets/icons/python.svg';
import {ReactComponent as Ruby} from '../../assets/icons/ruby.svg';
import {ReactComponent as Sap} from '../../assets/icons/sap.svg';
import {ReactComponent as Scala} from '../../assets/icons/scala.svg';
import {ReactComponent as Security} from '../../assets/icons/security.svg';
import {ReactComponent as Support} from '../../assets/icons/support.svg';
import {ReactComponent as Testing} from '../../assets/icons/testing.svg';
import {ReactComponent as Ux} from '../../assets/icons/ux.svg';
import {ReactComponent as Google} from "../../assets/google.svg";
import {ReactComponent as LinkedIn} from "../../assets/linkedin.svg";
import {ReactComponent as Github} from "../../assets/github-logo.svg";
import {ReactComponent as Facebook} from "../../assets/facebook-logo.svg";
import {ReactComponent as Upload} from "../../assets/upload.svg";
import {ReactComponent as Uploaded} from "../../assets/uploaded.svg";

const Icon = ({type}: IconProps): ReactElement | null => {
    switch (type) {
        case 'javascript':
            return (
                <Javascript/>
            );
        case 'html':
            return (
                <Html/>
            );
        case 'php':
            return (
                <Php/>
            );
        case 'ruby':
            return (
                <Ruby/>
            );
        case 'python':
            return (
                <Python/>
            );
        case 'java':
            return (
                <Java/>
            );
        case 'net':
            return (
                <Net/>
            );
        case 'scala':
            return (
                <Scala/>
            );
        case 'c':
            return (
                <C/>
            );
        case 'mobile':
            return (
                <Mobile/>
            );
        case 'testing':
            return (
                <Testing/>
            );
        case 'devops':
            return (
                <Devops/>
            );
        case 'ux':
            return (
                <Ux/>
            );
        case 'pm':
            return (
                <Pm/>
            );
        case 'game':
            return (
                <Game/>
            );
        case 'analytics':
            return (
                <Analytics/>
            );
        case 'security':
            return (
                <Security/>
            );
        case 'data':
            return (
                <Data/>
            );
        case 'go':
            return (
                <Go/>
            );
        case 'other':
            return (
                <Other/>
            );
        case 'sap':
            return (
                <Sap/>
            );
        case 'support':
            return (
                <Support/>
            );
        case 'google':
            return (
                <Google/>
            );
        case 'github':
            return (
                <Github/>
            );
        case 'facebook':
            return (
                <Facebook/>
            );
        case 'linkedin':
            return (
                <LinkedIn/>
            );
        case 'upload':
            return (
                <Upload/>
            );
        case 'uploaded':
            return (
                <Uploaded/>
            )
        default:
            return null;
    }
};

export default Icon;