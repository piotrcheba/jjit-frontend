import React, {ReactElement, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {applyOffer, applyOfferStart, fetchSingleOffer} from "../../redux/actions/offerActions";
import axios from "axios";
import parse from 'html-react-parser';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import SendIcon from '@material-ui/icons/Send';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import MailIcon from '@material-ui/icons/Mail';
import CreateIcon from '@material-ui/icons/Create';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import CircularProgress from "@material-ui/core/CircularProgress";
import OfferHeader from "../OfferHeader/OfferHeader";
import OfferSection from "../OfferSection/OfferSection";
import Skill from "../Skill/Skill";
import Icon from '../Icon/Icon';
import {makeStyles} from "@material-ui/core/styles";
import {styles, muiStyles} from './Offer.styles';
import {apiConfig} from "../../config/api";
import {OfferProps} from "./Offer.interface";
import {RootState} from "../../redux/store";
import {SkillInterface} from "../../interfaces/offer.interface";

// @ts-ignore
const useStyles = makeStyles(muiStyles)

const CLOUDINARY_UPLOAD_PRESET = 'jjit_cv';

const LoadingSpinner = (): ReactElement => (
    <styles.MsgWrapper>
        <CircularProgress size={80} data-testid='offer-spinner' />
    </styles.MsgWrapper>
);

const SubmitSuccessMsg = (): ReactElement => (
    <styles.MsgWrapper>
        <styles.SuccessIcon/>
        <styles.SubmittedText>
            Thank you! Your application has been submitted!
        </styles.SubmittedText>
    </styles.MsgWrapper>
)

export default function Offer({match}: OfferProps): ReactElement {
    const classes = useStyles();

    const activeOffer = useSelector((state: RootState) => state.offers.activeOffer);
    const offers = useSelector((state: RootState) => state.offers);
    const auth = useSelector((state: RootState) => state.auth);

    const isLoading = offers.fetchLoading;

    const dispatch = useDispatch();

    useEffect((): void => {
        const params = {
            name: match.params.companyName,
            title: match.params.title,
        }

        dispatch(fetchSingleOffer(params))
    }, [])

    const [name, setName] = useState<string>(auth.user?.name ? `${auth.user.name} ${auth.user.surname}` : '');
    const [email, setEmail] = useState<string>(auth.user?.email || '');
    const [about, setAbout] = useState<string>(auth.user?.liProfile || '')
    const [file, setFile] = useState<any>(auth.user?.cv || '');

    const [submitted, setSubmitted] = useState<boolean>(false);

    const fileInputRef: React.MutableRefObject<HTMLInputElement | null> = useRef(null);

    const handleClick = (e: React.MouseEvent<HTMLDivElement>): void => {
        fileInputRef.current?.click();
    }

    //-----------FILE INPUT DEPENDENCIES--------------

    let inputElement: ReactElement | null = (
        <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>): void => (
                (e.target.files instanceof FileList) ?
                setFile(e.target.files[0]) : undefined
            )}
            accept="image/*"
            type="file"
            tabIndex={-1}
            autoComplete="off"
            style={{display: 'none'}}
            ref={fileInputRef}
        />
    );

    let uploadIcon: ReactElement = (
        <Icon type='upload'/>
    );

    let uploadLabel: ReactElement = (
        <styles.UploadLabel>
            Upload CV (.pdf)
        </styles.UploadLabel>
    );

    let deleteFileBtn: ReactElement | null = null;

    if (file) {
        inputElement = null;
        uploadIcon = (
            <Icon type='uploaded'/>
        );
        uploadLabel = (
            <styles.UploadedLabel>
                {file.name}
            </styles.UploadedLabel>
        )
        deleteFileBtn = (
            <Button variant="text" className={classes.deleteBtn} onClick={() => setFile('')}>
                <styles.DeleteBtnSpan>
                    Delete
                </styles.DeleteBtnSpan>
                <DeleteOutlineIcon/>
            </Button>
        )
    }

    //-----------AUTH DEPENDENCIES--------------

    const applyForm: ReactElement = (
        <styles.ApplyForm>
            {auth.isAuthenticated ? (
                <styles.ApplyHeader>
                    <styles.UserInfoContainer>
                        <styles.AvatarBox img={auth.user?.avatarUrl} />
                        <styles.UserInfo>
                            <Typography
                                className={classes.name}
                                variant="body1"
                            >
                                {auth.user?.name + " " + auth.user?.surname}
                            </Typography>
                            <Typography
                                className={classes.email}
                                variant="body1"
                            >
                                {auth.user?.email}
                            </Typography>
                        </styles.UserInfo>
                    </styles.UserInfoContainer>
                    <styles.BtnWrapper>
                        <Button
                            className={classes.largeBtn}
                            variant='contained'
                            color='primary'
                            onClick={() => submitApplication()}
                        >
                            1-click Apply
                            <SendIcon/>
                        </Button>
                    </styles.BtnWrapper>
                </styles.ApplyHeader>
            ): null}
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={6}>
                    <TextField
                        fullWidth
                        value={name}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>): void => setName(e.target.value)}
                        color="primary"
                        label="First & last name"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <PersonOutlineIcon className={classes.inputIcon} />
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                    <TextField
                        fullWidth
                        value={email}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>): void => setEmail(e.target.value)}
                        color="primary"
                        label="Email"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <MailIcon className={classes.inputIcon} />
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                    <TextField
                        fullWidth
                        value={about}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>): void => setAbout(e.target.value)}
                        className={classes.inputHeight}
                        multiline
                        color="primary"
                        label="Introduce yourself (linkedin/github links)"
                        variant="outlined"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <CreateIcon className={classes.inputIcon} />
                                </InputAdornment>
                            )
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                    <styles.UploadCVWrapper tabIndex={0} onClick={file ? undefined : handleClick} cursor={Boolean(file)}>
                        {inputElement}
                        <styles.UploadCV>
                            <styles.IconWrapper>
                                {uploadIcon}
                            </styles.IconWrapper>
                            {uploadLabel}
                            {deleteFileBtn}
                        </styles.UploadCV>
                    </styles.UploadCVWrapper>
                </Grid>
            </Grid>
            {auth.isAuthenticated ? null : (
                <styles.ApplyBtn>
                    <styles.BtnWrapper>
                        <Button
                            className={classes.btn}
                            variant='contained'
                            color='primary'
                            onClick={() => submitApplication()}
                        >
                            Apply
                            <SendIcon/>
                        </Button>
                    </styles.BtnWrapper>
                </styles.ApplyBtn>
            )}
        </styles.ApplyForm>
    )

    const submitApplication = async () => {
        dispatch(applyOfferStart());

        const offerId = activeOffer?._id!;
        let cv = auth.user?.cv || {name: '', size: '', url: ''};

        // Upload file to cloudinary to get url before storing in redux
        if (file && file !== auth.user?.cv) {
            const formData = new FormData();
            formData.append('file', file);
            formData.append('upload_preset', CLOUDINARY_UPLOAD_PRESET);
            delete axios.defaults.headers.common["Authorization"]
            const res = await axios.post(apiConfig.CLOUDINARY_API_URL, formData);
            cv = {
                name: res.data.original_filename,
                size: res.data.bytes,
                url: res.data.secure_url
            };
        }

        const appData = {
            name: name,
            email: email,
            about: about,
            cv: cv
        }

        dispatch(applyOffer(offerId, appData))
        setSubmitted(true);
    }

    return (
        <styles.Container>
            {isLoading ? <LoadingSpinner/> :
                <styles.Wrapper>
                    <OfferHeader headerCtrls />
                    <Paper className={classes.paper}>
                        <OfferSection title='Tech stack'>
                            <styles.SkillsContainer>
                                {activeOffer?.skills?.map((el: SkillInterface): ReactElement => (
                                    <Skill
                                        label={el.name}
                                        level={el.level}
                                    />
                                ))}
                            </styles.SkillsContainer>
                        </OfferSection>
                    </Paper>
                    <Paper className={classes.paper}>
                        <OfferSection title='Description'>
                            {parse(activeOffer?.jobDesc || '')}
                        </OfferSection>
                    </Paper>
                    <Paper className={classes.paper}>
                        <OfferSection title='Apply for this job'>
                            {offers.loading ? <LoadingSpinner/> : submitted ? <SubmitSuccessMsg/> : applyForm}
                        </OfferSection>
                    </Paper>
                </styles.Wrapper>
            }
        </styles.Container>
    )
}