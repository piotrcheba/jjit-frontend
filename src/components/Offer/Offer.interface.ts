import {RouteComponentProps} from "react-router-dom";

interface MatchParams {
    companyName: string;
    title: string;
}

export interface OfferProps extends RouteComponentProps<MatchParams> {
}