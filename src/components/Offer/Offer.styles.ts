import styled from "styled-components";
import CheckCircleOutlinedIcon from "@material-ui/icons/CheckCircleOutlined";

export const styles = {
    Wrapper: styled.div`
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        padding: 0;
        overflow: auto;
        @media (min-width: 1025px) {
            padding: 0 15px 15px;
        }
    `,
    Container: styled.div`
        position: relative;
        display: flex;
        flex: 1 1 0%;
    `,
    SkillsContainer: styled.div`
        display: flex;
        flex-wrap: wrap;
        margin: 0 0 -20px 0;
    `,
    ApplyForm: styled.form`
        padding: 12px 20px;
    `,
    ApplyHeader: styled.div`
        display: flex;
        flex-wrap: nowrap;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 20px;
    `,
    UserInfoContainer: styled.div`
        display: flex;
        align-items: center;
        flex-flow: row nowrap;
    `,
    AvatarBox: styled.div`
        box-shadow: rgba(0, 0, 0, 0.08) 0px 6px 13px 0px;
        color: white;
        position: relative;
        text-align: center;
        flex-shrink: 0;
        display: flex;
        align-items: center;
        width: 77px;
        height: 77px;
        border-width: 3px;
        border-style: solid
        border-color: white;
        border-image: initial;
        border-radius: 50%;
        margin: 0px auto;
        overflow: hidden;
        background: ${(props: {img: string | undefined}) => `url(${props.img})`};
        background-repeat: no-repeat;
    `,
    UserInfo: styled.div`
        display: flex;
        justify-content: center;
        flex-flow: column nowrap;
        padding: 20px;
    `,
    BtnWrapper: styled.div`
        position: relative;
        display: inline-block;
        order: 1;
    `,
    UploadCVWrapper: styled.div`
        display: flex;
        align-items: center;
        flex-direction: row;
        height: calc(((100% - 12px) - 6px) - 8px);
        position: relative;
        width: 100%;
        cursor: ${(props: {cursor: boolean}) => props.cursor ? 'default' : 'pointer'};
        min-height: 100px;
        box-sizing: border-box;
        padding: 20px;
        border-width: 1px;
        border-style: solid;
        border-color: rgba(0, 0, 0, 0.25);
        border-image: initial;
        border-radius: 6px;
        transition: border-color 300ms ease 0s;
        outline: none !important;
    `,
    UploadCV: styled.div`
        display: flex;
        width: 100%;
        flex-direction: row;
        align-items: center;
    `,
    IconWrapper: styled.div`
        & svg {
            width: 50px;
            height: 50px;
        }
    `,
    UploadLabel: styled.label`
        font-size: 16px;
        line-height: 22px;
        color: rgb(153, 161, 171);
        padding: 10px;
    `,
    UploadedLabel: styled.label`
        font-size: 14px;
        color: rgb(255, 64, 129);
        font-weight: 600;
        max-width: 80%;
        padding: 10px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden !important;
    `,
    Filename: styled.div`
        display: inline-block;
        max-width: 100%;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    `,
    DeleteBtnSpan: styled.span`
        margin-right: 5px;
        text-transform: capitalize;
    `,
    ApplyBtn: styled.div`
        margin-top: 20px;
        display: flex;
        justify-content: flex-end;
    `,
    MsgWrapper: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin: 20px auto;
    `,
    SuccessIcon: styled(CheckCircleOutlinedIcon)`
        width: 100px;
        height: 100px;
        font-size: 80px;
        fill: rgb(255, 64, 129);
    `,
    SubmittedText: styled.span`
        padding-top: 10px;
        color: #2E384D;
        font-size: 22px;
        font-family: Open Sans, sans-serif;
        font-weight: 300;
        line-height: 1.167;
        margin-bottom: 10px;
    `
}

export const muiStyles = {
    paper: {
        boxShadow: 'rgba(225, 232, 240, 0.7) 0px 2px 18px 10px;',
        marginTop: '40px',
        background: 'rgb(255, 255, 255)',
        borderRadius: '5px',
        overflow: 'visible',
    },
    name: {
        fontSize: '18px',
        color: 'rgb(55, 71, 79)',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden'
    },
    email: {
        color: 'rgb(153, 161, 171)'
    },
    btn: {
        color: 'rgb(255, 255, 255)',
        fontWeight: 600,
        fontSize: '14px',
        position: 'relative',
        paddingRight: '70px',
        paddingLeft: '34px',
        textTransform: 'none',
        boxShadow: 'rgba(255, 64, 129, 0.31) 0px 23px 26px -13px',
        background: 'rgb(255, 64, 129)',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(255, 64, 129)',
        borderImage: 'initial',
        borderRadius: '100px',
        overflow: 'hidden',
        transition: 'all 300ms ease-out 0s, background 0s ease 0s',
        '&:hover': {
            background: 'rgb(178, 44, 90)',
            borderWidth: '1px',
            borderStyle: 'solid',
            borderColor: 'rgb(178, 44, 90)',
            borderImage: 'initial'
        },
        '&:hover svg': {
            fill: 'rgb(178, 44, 90)',
            background: 'linear-gradient(110deg, rgb(178, 44, 90) 30%, rgb(255, 255, 255) 32%)'
        },
        '& svg': {
            position: 'absolute',
            top: '0px',
            right: '0px',
            fill: 'rgb(255, 64, 129)',
            width: '55px',
            height: '100%',
            background: 'linear-gradient(110deg, rgb(255, 64, 129) 30%, rgb(255, 255, 255) 32%)',
            padding: '8px 8px 8px 24px'
        }
    },
    largeBtn: {
        color: 'rgb(255, 255, 255)',
        fontWeight: 600,
        fontSize: '18px',
        position: 'relative',
        paddingRight: '70px',
        paddingLeft: '34px',
        textTransform: 'none',
        boxShadow: 'rgba(255, 64, 129, 0.31) 0px 23px 26px -13px',
        background: 'rgb(255, 64, 129)',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: 'rgb(255, 64, 129)',
        borderImage: 'initial',
        borderRadius: '100px',
        overflow: 'hidden',
        transition: 'all 300ms ease-out 0s, background 0s ease 0s',
        '&:hover': {
            background: 'rgb(178, 44, 90)',
            borderWidth: '1px',
            borderStyle: 'solid',
            borderColor: 'rgb(178, 44, 90)',
            borderImage: 'initial'
        },
        '&:hover svg': {
            fill: 'rgb(178, 44, 90)',
            background: 'linear-gradient(110deg, rgb(178, 44, 90) 30%, rgb(255, 255, 255) 32%)'
        },
        '& svg': {
            position: 'absolute',
            top: '0px',
            right: '0px',
            fill: 'rgb(255, 64, 129)',
            width: '55px',
            height: '100%',
            background: 'linear-gradient(110deg, rgb(255, 64, 129) 30%, rgb(255, 255, 255) 32%)',
            padding: '8px 8px 8px 24px'
        }
    },
    formGrid: {
        display: 'flex'
    },
    inputIcon: {
        color: 'rgb(191, 197, 210)'
    },
    inputHeight: {
        '& .MuiInputBase-root': {
            minHeight: '100px'
        }
    },
    deleteBtn: {
        position: 'absolute',
        bottom: '0px',
        right: '0px',
        display: 'flex',
        alignItems: 'center',
        color: 'rgb(153, 161, 171)',
        fontSize: '14px',
        fontWeight: 600,
        flexFlow: 'row nowrap'
    }
}