export interface SkillProps {
    editable?: boolean;
    label: string;
    level: number;
    skillRemover?: (skillIdentifier: string) => void;
    skillChange?: (value: number, skillIdentifier: string) => void
}