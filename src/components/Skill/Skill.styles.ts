import styled from 'styled-components';

export const styles = {
    SkillWrapper: styled.div`
        display: flex;
        flex: 1 0 20%;
        padding: 0 5px;
        margin: 0 0 20px 0;
        max-width: 20%;
        justify-content: center;
    `,
    SkillEditable: styled.div`
        position: relative;
        &:hover {
            & > i {
                opacity: 1;
            }
        }
    `,
    SkillCloseIcon: styled.i`
        cursor: pointer;
        position: absolute;
        right: -25px;
        top: -3px;
        color: #64407a;
        font-size: 15px;
        background: #fff;
        border-radius: 50%;
        border: 1px solid #64407a;
        opacity: 0;
        z-index: 10;
        &:hover {
            background: #64407a;
            color: #fff;
            transition: .2s linear all;
        }
    `,
    SkillBar: styled.div`
        display: flex;
        flex-direction: row-reverse;
        justify-content: flex-end;
        margin-bottom: 4px;
    `,
    SkillDot: styled.div`
        background-color: ${(props: {fill: boolean}) => props.fill ? '#ff4081' : '#c7ced6'};
        display: block;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        margin: 0 0 0 10px;
        cursor: default;
        &:last-child {
            margin: 0;
        }
    `,
    SkillDotEditable: styled.div`
        background-color: ${(props: {fill: boolean}) => props.fill ? '#ff4081' : '#c7ced6'};
        display: block;
        width: 10px;
        height: 10px;
        border-radius: 50%;
        margin: 0 0 0 10px;
        cursor: pointer;
        &:last-child {
            margin: 0;
        }
        &:hover {
            background-color: #9c27b0;
            border-color: #e6b5ee;
            & ~ div {
                background-color: #9c27b0;
                border-color: #e6b5ee;
            }
        }
    `,
    SkillName: styled.div`
        font-weight: 600;
        font-size: 16px;
        word-break: break-word;
    `,
    LevelTitle: styled.div`
        color: #99a1ab;
    `
}