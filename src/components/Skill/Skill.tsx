import React, {ReactElement} from 'react';
import {styles} from "./Skill.styles";
import {SkillProps} from "./Skill.interface";
import {levelDisplayer} from "../../utils/levelDisplayer";

export default function Skill({level, editable, label, skillChange, skillRemover}: SkillProps): ReactElement {
    let skillCloseIcon: ReactElement | null = null;

    let skillDots: ReactElement = (
        <>
            <styles.SkillDot fill={level >= 5} />
            <styles.SkillDot fill={level >= 4} />
            <styles.SkillDot fill={level >= 3} />
            <styles.SkillDot fill={level >= 2} />
            <styles.SkillDot fill={level >= 1} />
        </>
    );

    if (editable) {
        skillCloseIcon = (
            <styles.SkillCloseIcon
                data-testid='skill-closeicon'
                className="material-icons close"
                onClick={() => skillRemover && skillRemover(label)}
            >
                close
            </styles.SkillCloseIcon>
        );
        skillDots = (
            <>
                <styles.SkillDotEditable
                    fill={level >= 5}
                    onClick={() => skillChange && skillChange(5, label)}/>
                <styles.SkillDotEditable
                    fill={level >= 4}
                    onClick={() => skillChange && skillChange(4, label)}/>
                <styles.SkillDotEditable
                    fill={level >= 3}
                    onClick={() => skillChange && skillChange(3, label)}/>
                <styles.SkillDotEditable
                    fill={level >= 2}
                    onClick={() => skillChange && skillChange(2, label)}/>
                <styles.SkillDotEditable
                    fill={level >= 1}
                    onClick={() => skillChange && skillChange(1, label)}/>
            </>
        )
    }

    return (
        <styles.SkillWrapper data-testid='skill-wrapper'>
            <styles.SkillEditable>
                {skillCloseIcon}
                <styles.SkillBar>
                    {skillDots}
                </styles.SkillBar>
                <styles.SkillName data-testid='skill-label'>
                    {label}
                </styles.SkillName>
                <styles.LevelTitle data-testid='skill-leveltitle'>
                    {levelDisplayer(level)}
                </styles.LevelTitle>
            </styles.SkillEditable>
        </styles.SkillWrapper>
    )
}