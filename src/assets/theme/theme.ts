import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: 'rgb(255, 64, 129)',
        },
        secondary: {
            main: 'rgb(171, 71, 188)',
        },
    },
    typography: {
        fontFamily: [
            "Open Sans",
            'sans-serif'
        ].join(','),
        fontWeightBold: 800,
        h1: {
            fontSize: '34px',
            marginBottom: '20px',
            color: '#2E384D',
        },
        h2: {
            color: '#37474f',
            fontSize: '24px',
            fontWeight: 600,
        },
        h3: {
            fontSize: '13px',
            fontWeight: 600,
            marginBottom: '20px',
            color: 'rgb(153, 161, 171)',
        },
        body1: {
            fontSize: '14px',
            color: 'rgb(55, 71, 79)',
        },
        body2: {
            fontFamily: [
                "Open Sans",
                'sans-serif'
            ].join(','),
        },
        subtitle2: {
            color: '#2E384D',
            fontWeight: 600,
        }
    },
    overrides: {
        MuiFab: {
            root: {
                boxShadow: 'none',
                fontWeight: 600,
                textTransform: 'none',
            },
            extended: {
                "&$sizeMedium": {
                    height: '40px',
                    padding: '0 16px',
                }
            }
        },
        MuiPaper: {
            root: {
                color: '#99a1ab',
            }
        },
        MuiListItem: {
            button: {
                '&.active': {
                    borderLeft: '#ff4081 solid 4px',
                    paddingLeft: '12px',
                },
            },
        },
        MuiFormLabel: {
            root: {
                color: '#9e9e9e',
            }
        },
    }
});

export default theme;