export const apiConfig = {
    CLOUDINARY_API_URL: process.env.REACT_APP_CLOUDINARY_API_URL as string,
    GOOGLE_API_KEY: process.env.REACT_APP_GOOGLE_API_KEY as string,
    GEOCODE_API_KEY: process.env.REACT_APP_GEOCODE_API_KEY as string,
    EDITOR_API_KEY: process.env.REACT_APP_EDITOR_API_KEY as string
}