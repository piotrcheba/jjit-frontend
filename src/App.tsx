import React, {ReactElement, useEffect, useState} from 'react';

import { Route, Switch, Redirect } from 'react-router-dom';

import {useDispatch, useSelector} from "react-redux";
import {verifyAuth} from "./redux/actions/authActions";

import AddOffer from "./components/AddOffer/AddOffer";
import JobBoard from "./components/JobBoard/JobBoard";
import UserPanel from "./components/Authentication/UserPanel/UserPanel";
import Navbar from "./components/Navigation/Navbar/Navbar";
import SideDrawer from "./components/Navigation/SideDrawer/SideDrawer";
import DevsAuth from "./components/Authentication/DevsAuth/DevsAuth";
import EmpsAuth from "./components/Authentication/EmpsAuth/EmpsAuth";

import {ThemeProvider} from "@material-ui/core/styles";
import theme from "./assets/theme/theme";
import {RootState} from "./redux/store";
import {AuthStateInterface} from "./interfaces/auth.interface";

let routes = [
    {path: "/devs", component: DevsAuth},
    {path: "/emps", component: EmpsAuth},
    {path: "/", component: JobBoard}
]

export default function App(): ReactElement {
    const [showDrawer, toggleDrawer] = useState(false);

    const closeDrawerHandler = () => toggleDrawer(false);

    const auth = useSelector((state: RootState): AuthStateInterface => state.auth)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(verifyAuth());
    }, [])

    if (auth.isAuthenticated) {
        routes = [
            {path: "/addoffer", component: AddOffer},
            {path: "/devs", component: DevsAuth},
            {path: "/emps", component: EmpsAuth},
            {path: "/user_panel", component: UserPanel},
            {path: "/", component: JobBoard}
        ]
    }

    return (
        <ThemeProvider theme={theme}>
            <Navbar toggleDrawer={toggleDrawer}/>
            <SideDrawer
                showDrawer={showDrawer}
                closeHandler={closeDrawerHandler}
            />
            <Switch>
                {routes.map((route, i) => (
                    <Route {...route}/>
                    ))
                }
                    <Redirect to="/"/>
            </Switch>
        </ThemeProvider>
    );
}