import {OfferInterface} from "./offer.interface";

export interface RegisterUserInterface {
    accType: string;
    name: string;
    surname: string;
    email: string;
    password: string;
    password2: string;
}

export interface LoginUserInterface {
    email: string;
    password: string;
}

export interface CVInterface {
    name: string;
    size: number | string;
    url: string;
}

export interface UserInterface {
    id: string;
    accType: string;
    name: string;
    surname: string;
    email: string;
    avatarUrl: string;
    city?: string;
    description?: string;
    liProfile?: string;
    githubProfile?: string;
    cv?: CVInterface;
    expYears?: string;
}

export interface AuthStateInterface {
    isAuthenticated: boolean;
    user: UserInterface | undefined;
    offers: OfferInterface[];
    loading: boolean;
}

