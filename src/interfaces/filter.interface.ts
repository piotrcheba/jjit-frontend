export type SelectFilterValue = string | number | number[];

export interface FetchFilterInterface {
    location: string;
    tech: string;
    salaryFrom: number;
    salaryTo: number;
    seniority: string;
}

export interface FilterStateInterface {
    location: string;
    tech: string;
    salary: number[];
    seniority: string;
}