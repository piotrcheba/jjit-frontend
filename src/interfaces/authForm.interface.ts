export interface RegisterFormInterface {
    name: {
        value: string;
        validation: {
            required: true;
        },
        valid: boolean;
        touched: boolean;
    },
    surname: {
        value: string;
        validation: {
            required: true,
        },
        valid: boolean;
        touched: boolean;
    },
    email: {
        value: string;
        validation: {
            required: true,
            isEmail: true
        },
        valid: boolean;
        touched: boolean;
    },
    password: {
        value: string;
        validation: {
            required: true
        };
        valid: boolean;
        touched: boolean;
    },
    password2: {
        value: string;
        validation: {
            required: true
        },
        valid: boolean;
        touched: boolean;
    }
}

export interface LoginFormInterface {
    email: {
        value: string;
        validation: {
            required: true,
            isEmail: true
        };
        valid: boolean;
        touched: boolean;
    },
    password: {
        value: string;
        validation: {
            required: true
        };
        valid: boolean;
        touched: boolean;
    }
}