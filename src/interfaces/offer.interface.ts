import {CVInterface} from "./auth.interface";

export interface GeoLocInterface {
    lat: number;
    lng: number;
}

export interface SkillInterface {
    name: string;
    level: number;
}

export interface ApplicationInterface {
    name: string;
    email: string;
    about: string;
    cv: CVInterface;
}

export interface FetchSingleOfferParams {
    name: string;
    title: string;
}

export interface OfferInterface {
    _id?: string;
    skills: SkillInterface[];
    applications: ApplicationInterface[] | undefined;
    companyLogo: string;
    companyName: string;
    lower_name: string;
    companyWebsite: string;
    companySize: string;
    companyType: string;
    companyIndustry: string;
    title: string;
    lower_title: string;
    expLevel: string;
    lower_exp: string;
    empType: string;
    salaryFrom: number;
    salaryTo: number;
    currency: string;
    onlineInterview: boolean;
    jobDesc: string;
    officeCity: string;
    lower_city: string;
    officeStreet: string;
    fullyRemote: boolean;
    geoLoc: GeoLocInterface;
    mainTech: string;
    contact: string;
    createdBy: string;
    date?: string;
}

export interface OfferStateInterface {
    fetchLoading: boolean;
    loading: boolean;
    offers: OfferInterface[];
    activeOffer: OfferInterface | undefined;
    error: {}
}