import React from 'react';
import {render} from '@testing-library/react';
import Publish from "../../../../components/AddOffer/Publish/Publish";
import {RenderWithStore} from "../../../helpers/render-component-with-store";

describe('Publish', (): void => {
    it('should render loading spinner if offer hasnt been added yet', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{loading: true}}>
                <Publish/>
            </RenderWithStore>
        )
        const spinner = queryByTestId('publish-spinner');
        const addedText = queryByTestId('publish-offeraddedtext');

        expect(spinner).toBeInTheDocument();
        expect(addedText).not.toBeInTheDocument();
    })
    it('should render success msg if offer has been added', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <Publish/>
            </RenderWithStore>
        )
        const spinner = queryByTestId('publish-spinner');
        const addedText = queryByTestId('publish-offeraddedtext');

        expect(spinner).not.toBeInTheDocument();
        expect(addedText).toBeInTheDocument();
    })
})