import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import AddOffer from "../../../components/AddOffer/AddOffer";
import {RenderWithStore} from "../../helpers/render-component-with-store";

jest.mock('react-router-dom', () => ({
    useHistory: () => ({
        push: jest.fn(),
    }),
}));

describe('AddOffer', (): void => {
    it('should not render back button if active step is Publish component', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{activeOffer: {skills: [], jobDesc: ''}}}>
                <AddOffer history={[]}/>
            </RenderWithStore>
        );

        const button = queryByTestId('addoffer-button')
        const form = queryByTestId('create-form');
        expect(button).toBeInTheDocument();
        // @ts-ignore
        fireEvent.submit(form);
        const verifyNextButton = queryByTestId('verify-nextbutton');
        expect(button).toBeInTheDocument();
        // @ts-ignore
        fireEvent.click(verifyNextButton);
        expect(button).not.toBeInTheDocument();
    })
    it('stepper shows correct values', () => {
        const steps = ['Create', 'Verify', 'Publish'];

        const {queryByTestId} = render(
            <RenderWithStore offers={{activeOffer: {skills: [], jobDesc: ''}}}>
                <AddOffer history={[]}/>
            </RenderWithStore>
        );

        steps.forEach(label => {
            const labelDOM = queryByTestId(label);
            expect(labelDOM).toHaveTextContent(label);
        })
    })
})
