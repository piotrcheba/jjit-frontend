import React from 'react';
import {render, fireEvent, waitForElementToBeRemoved} from '@testing-library/react';
import Create from '../../../../components/AddOffer/Create/Create';
import {RenderWithStore} from "../../../helpers/render-component-with-store";

const imgBlob = jest.fn();
const next = jest.fn();

describe('Create', (): void => {
    it('submit button should be disabled if any input is empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{}}>
                <Create next={next}/>
            </RenderWithStore>
        )


    })
    it('should display upload img input if img hasnt been provided', async () => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{}}>
                <Create next={next}/>
            </RenderWithStore>
        )
        const imgInput = queryByTestId('create-imginput');

        const fileMock = new File(['(⌐□_□)'], 'chucknorris.png', { type: 'image/png' })

        fireEvent.change(imgInput, {target: {files: [fileMock]}});
        await waitForElementToBeRemoved(() => imgInput)

        expect(imgInput).not.toBeInTheDocument();
        // const thumbContainer = queryByTestId('create-thumbcontainer')
        // expect(thumbContainer).not.toBeInTheDocument();
    })
    it('should display thumb if img has been provided', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <Create next={next}/>
            </RenderWithStore>
        )
        const input = queryByTestId('create-imginput');
        const thumb = queryByTestId('create-thumb');
        // @ts-ignore
        thumb.src = 'asdfxd'

        // fireEvent.change(input, {target: {files: [fileMock]}});
        expect(input).not.toBeInTheDocument();
        expect(thumb).toBeInTheDocument();
    })
})