import React from 'react';
import {render} from '@testing-library/react';
import SideDrawer from "../../../../components/Navigation/SideDrawer/SideDrawer";

describe('SideDrawer', (): void => {
    it('should map through provided array and return list items with its values', (): void => {
        const listItems = [
            {label: 'Employer Panel', icon: 'work'},
            {label: 'Event', icon: 'mic'},
            {label: 'About us', icon: 'group'},
            {label: 'RSS', icon: 'rss_feed'},
            {label: 'Terms', icon: 'picture_as_pdf'},
            {label: 'Policy', icon: 'picture_as_pdf'},
        ];

        const {queryByTestId} = render(
            <SideDrawer showDrawer={true} closeHandler={jest.fn} />
        )

        listItems.forEach((item, index) => {
            const icon = queryByTestId(`sidedrawer-icon-${index}`);
            const label = queryByTestId(`sidedrawer-label-${index}`);
            expect(icon).toHaveTextContent(item.icon);
            expect(label).toHaveTextContent(item.label);
        })
    })
})