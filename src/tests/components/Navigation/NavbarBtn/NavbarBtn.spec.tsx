import React from 'react';
import {render} from '@testing-library/react';
import NavbarBtn from "../../../../components/Navigation/NavbarBtn/NavbarBtn";

describe('NavbarBtn', (): void => {
    it('should render link btn with given url and label', (): void => {
        const test = {url: '/test', label: 'Test'}

        const {queryByTestId} = render(
            <NavbarBtn type='post' url={test.url} label={test.label} />
        )
        const postBtn = queryByTestId('navbarbtn-post');

        expect(postBtn).toHaveAttribute('href', test.url);
        expect(postBtn).toHaveTextContent(test.label);

    })
})