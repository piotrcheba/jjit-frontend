import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {render, fireEvent} from '@testing-library/react';
import UserBox from '../../../../components/Navigation/UserBox/UserBox';
import {RenderWithStore} from "../../../helpers/render-component-with-store";

describe('UserBox', (): void => {
    it('should display provided user name', (): void => {
        const testname = 'Testname'

        const {queryByTestId} = render(
            <RenderWithStore auth={{user: {name: testname}}}>
                <UserBox />
            </RenderWithStore>
        );
        const usernameContainer = queryByTestId('userbox-usernamecontainer');
        expect(usernameContainer).toHaveTextContent(testname);
    })
    it('should not render popover', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <UserBox />
            </RenderWithStore>
            , {wrapper: MemoryRouter});
        const popover = queryByTestId('userbox-popover');
        expect(popover).not.toBeInTheDocument();
    })
    it('should render popover on wrapper click', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <UserBox />
            </RenderWithStore>
            , {wrapper: MemoryRouter});
        const wrapper = queryByTestId('userbox-wrapper');
        // @ts-ignore
        fireEvent.click(wrapper);
        const popover = queryByTestId('userbox-popover');
        expect(popover).toBeInTheDocument();
    })
    it('should remove popover on backdrop click', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <UserBox />
            </RenderWithStore>
            , {wrapper: MemoryRouter});
        const backdrop = queryByTestId('userbox-backdrop');
        // @ts-ignore
        fireEvent.click(backdrop);
        const popover = queryByTestId('userbox-popover');
        expect(popover).not.toBeInTheDocument();
    })
    it('should not render myoffers link if account type is not emp', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{isAuthenticated: true, user: {accType: 'dev'}}}>
                <UserBox />
            </RenderWithStore>
        , {wrapper: MemoryRouter});
        const myOffersBtn = queryByTestId('userbox-myoffersbtn');
        expect(myOffersBtn).not.toBeInTheDocument();
    })
    it('should render myoffers link if account type is emp', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{isAuthenticated: true, user: {accType: 'emp'}}}>
                <UserBox />
            </RenderWithStore>
        , {wrapper: MemoryRouter});
        const wrapper = queryByTestId('userbox-wrapper');
        // @ts-ignore
        fireEvent.click(wrapper);
        const myOffersBtn = queryByTestId('userbox-myoffersbtn');
        expect(myOffersBtn).toBeInTheDocument();
    })

})