import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {render} from '@testing-library/react';
import NavbarLink from "../../../../components/Navigation/NavbarLink/NavbarLink";

describe('NavbarLink', (): void => {
    it('should render button and span with given props', (): void => {
        const test = {url: '/test', label: 'Test'}

        const {queryByTestId} = render(
            <NavbarLink url={test.url} label={test.label} />
        , {wrapper: MemoryRouter})

        const btn = queryByTestId('navbarlink-btn');
        const span = queryByTestId('navbarlink-span');

        expect(btn).toHaveAttribute('href', test.url);
        expect(span).toHaveTextContent(test.label)
    })
})