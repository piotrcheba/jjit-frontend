import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {render} from '@testing-library/react';
import Navbar from "../../../../components/Navigation/Navbar/Navbar";
import {RenderWithStore} from "../../../helpers/render-component-with-store";

describe('Navbar', (): void => {
    it('should render post a job button and userbox if user is authenticated & account type is emp', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{isAuthenticated: true, user: {accType: 'emp'}}}>
                <Navbar clicked={jest.fn} />
            </RenderWithStore>
        , {wrapper: MemoryRouter})
        const postBtn = queryByTestId('navbarbtn-post');
        const userBox = queryByTestId('userbox-wrapper');
        const signBtn = queryByTestId('navbarbtn-sign');

        expect(postBtn).toBeInTheDocument();
        expect(userBox).toBeInTheDocument();
        expect(signBtn).not.toBeInTheDocument();
    })
    it('should render only userbox if user is authenticated & account type is not emp', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{isAuthenticated: true}}>
                <Navbar clicked={jest.fn} />
            </RenderWithStore>
            , {wrapper: MemoryRouter})
        const postBtn = queryByTestId('navbarbtn-post');
        const userBox = queryByTestId('userbox-wrapper');
        const signBtn = queryByTestId('navbarbtn-sign');

        expect(postBtn).not.toBeInTheDocument();
        expect(userBox).toBeInTheDocument();
        expect(signBtn).not.toBeInTheDocument();
    })
    it('should render only sign btn if user is not authenticated', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{isAuthenticated: false}}>
                <Navbar clicked={jest.fn} />
            </RenderWithStore>
            , {wrapper: MemoryRouter})
        const postBtn = queryByTestId('navbarbtn-post');
        const userBox = queryByTestId('userbox-wrapper');
        const signBtn = queryByTestId('navbarbtn-sign');

        expect(postBtn).not.toBeInTheDocument();
        expect(userBox).not.toBeInTheDocument();
        expect(signBtn).toBeInTheDocument();
    })
})