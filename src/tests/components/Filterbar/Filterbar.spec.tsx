import React from 'react';
import {render, fireEvent, waitFor} from '@testing-library/react';
import Filterbar from "../../../components/Filterbar/Filterbar";
import {RenderWithStore} from "../../helpers/render-component-with-store";

describe('Filterbar', (): void => {
    it('should open location popover on btn click', async () => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <Filterbar />
            </RenderWithStore>
        )
        const locPopBtn = queryByTestId('filterbar-locpopbtn');
        // @ts-ignore
        fireEvent.click(locPopBtn);
        await waitFor(() => {
            expect(queryByTestId('locationpopover-popover')).toBeInTheDocument();
        })
    })
    it('should open more filters dialog on btn click', async () => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <Filterbar />
            </RenderWithStore>
        )
        const moreFiltersBtn = queryByTestId('filterbar-morebtn');
        // @ts-ignore
        fireEvent.click(moreFiltersBtn);
        await waitFor(() => {
            expect(queryByTestId('morefiltersdialog-dialog')).toBeInTheDocument();
        })
    })
})