import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {render} from '@testing-library/react';
import ListItem from "../../../../../../components/JobBoard/List/ListItems/ListItem/ListItem";

describe('ListItem', (): void => {
    it('checks if link generates url dynamically based on props', (): void => {
        const {queryByTestId} = render(
            <ListItem skills={[]} lower_name='testname' lower_title='testtitle' />
        , {wrapper: MemoryRouter})

        const link = queryByTestId('listitem-linkwrapper');
        expect(link).toHaveAttribute('href', '/offers/testname-testtitle')
    })
    it('checks if function returns correct date of adding offer in days ago if its more than one', (): void => {
        const {queryByTestId} = render(
            <ListItem skills={[]} date='2020-09-15T12:22:12.553+00:00' />
            , {wrapper: MemoryRouter})

        const dateAdded = queryByTestId('listitem-dateadded');
        expect(dateAdded).toHaveTextContent('12d ago')
    })
    it('checks if function returns correct date of adding offer if its less than one day', (): void => {
        const {queryByTestId} = render(
            <ListItem skills={[]} date='2020-09-27T12:22:12.553+00:00' />
            , {wrapper: MemoryRouter})

        const dateAdded = queryByTestId('listitem-dateadded');
        expect(dateAdded).toHaveTextContent('New')
    })
    it('checks if fully remote chip is rendered based on given prop', (): void => {
        const {queryByTestId} = render(
            <ListItem skills={[]} fullyRemote onlineInterview />
            , {wrapper: MemoryRouter})

        const remoteChip = queryByTestId('listitem-remotechip');
        const onlineChip = queryByTestId('listitem-onlinechip');

        expect(remoteChip).toBeInTheDocument();
        expect(onlineChip).not.toBeInTheDocument();
    })
    it('checks if online interview chip is rendered based on given prop', (): void => {
        const {queryByTestId} = render(
            <ListItem skills={[]} onlineInterview />
            , {wrapper: MemoryRouter})

        const remoteChip = queryByTestId('listitem-remotechip');
        const onlineChip = queryByTestId('listitem-onlinechip');

        expect(remoteChip).not.toBeInTheDocument();
        expect(onlineChip).toBeInTheDocument();
    })
})