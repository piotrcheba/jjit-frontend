import React from 'react';
import {render, fireEvent, waitFor} from '@testing-library/react';
import Sortbar from "../../../../../components/JobBoard/List/Sortbar/Sortbar";
import {RenderWithStore} from "../../../../helpers/render-component-with-store";

describe('Sortbar', (): void => {
    it('should open popover on button click', async () => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <Sortbar />
            </RenderWithStore>
        )

        const button = queryByTestId('sortbar-btn');

        fireEvent.click(button);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).toBeInTheDocument();
        })
    })
    it('button should change display value of current filter', async () => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <Sortbar />
            </RenderWithStore>
        )

        const openPopBtn = queryByTestId('sortbar-btn');
        fireEvent.click(openPopBtn);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).toBeInTheDocument();
        })

        const highestBtn = queryByTestId('sortbar-highestbtn');
        fireEvent.click(highestBtn);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).not.toBeInTheDocument();
        })

        expect(queryByTestId('sortbar-displayvalue')).toHaveTextContent('highest salary');

        fireEvent.click(openPopBtn);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).toBeInTheDocument();
        })

        const lowestBtn = queryByTestId('sortbar-lowestbtn');
        fireEvent.click(lowestBtn);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).not.toBeInTheDocument();
        })

        expect(queryByTestId('sortbar-displayvalue')).toHaveTextContent('lowest salary');

        fireEvent.click(openPopBtn);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).toBeInTheDocument();
        })

        const latestBtn = queryByTestId('sortbar-latestbtn');
        fireEvent.click(latestBtn);

        await waitFor(() => {
            expect(queryByTestId('sortbar-popover')).not.toBeInTheDocument();
        })

        expect(queryByTestId('sortbar-displayvalue')).toHaveTextContent('latest');
    })
})