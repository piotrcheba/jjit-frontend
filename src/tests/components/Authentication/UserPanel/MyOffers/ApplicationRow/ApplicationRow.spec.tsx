import React from 'react';
import {render} from '@testing-library/react';
import ApplicationRow
    from "../../../../../../components/Authentication/UserPanel/MyOffers/ApplicationRow/ApplicationRow";

describe('ApplicationRow', (): void => {
    it('should render none in about column if props about is not provided', (): void => {
        const {queryByTestId} = render(
            <ApplicationRow name='test' email='test' />
        )
        const aboutCol = queryByTestId('approw-aboutcol');

        expect(aboutCol).toHaveTextContent('-');
    })
    it('should render given text in about column if props about is provided', (): void => {
        const {queryByTestId} = render(
            <ApplicationRow name='test' email='test' about='testtest' />
        )
        const aboutCol = queryByTestId('approw-aboutcol');

        expect(aboutCol).toHaveTextContent('testtest');
    })
    it('should render none in cv column if props cv is not provided', (): void => {
        const {queryByTestId} = render(
            <ApplicationRow name='test' email='test' />
        )
        const cvCol = queryByTestId('approw-cvcol');

        expect(cvCol).toHaveTextContent('-');
    })
    it('should render link in cv column if props cv is provided', (): void => {
        const {queryByTestId} = render(
            <ApplicationRow name='test' email='test' cv='http://test.com' />
        )
        const cvLink = queryByTestId('approw-cvlink');

        expect(cvLink).toBeInTheDocument();
        expect(cvLink).toHaveAttribute('href', 'http://test.com');
    })
})