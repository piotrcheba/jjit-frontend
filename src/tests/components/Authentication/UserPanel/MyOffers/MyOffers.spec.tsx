import React from 'react';
import {render} from '@testing-library/react';
import {MemoryRouter} from 'react-router-dom';
import MyOffers from "../../../../../components/Authentication/UserPanel/MyOffers/MyOffers";
import {RenderWithStore} from "../../../../helpers/render-component-with-store";

describe('MyOffers', (): void => {
    it('should render loading spinner if offers not fetched', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{user: {loading: true}}}>
                <MyOffers/>
            </RenderWithStore>
        )

        const loadingSpinner = queryByTestId('myoffers-spinner');
        const noOffersMsg = queryByTestId('myoffers-nooffers');
        const offerItem = queryByTestId('listitem-linkwrapper');

        expect(loadingSpinner).toBeInTheDocument();
        expect(noOffersMsg).not.toBeInTheDocument();
        expect(offerItem).not.toBeInTheDocument();
    })
    it('should render no active offers message if offers array is empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{offers: []}}>
                <MyOffers/>
            </RenderWithStore>
        )

        const loadingSpinner = queryByTestId('myoffers-spinner');
        const noOffersMsg = queryByTestId('myoffers-nooffers');
        const offerItem = queryByTestId('listitem-linkwrapper');

        expect(loadingSpinner).not.toBeInTheDocument();
        expect(noOffersMsg).toBeInTheDocument();
        expect(offerItem).not.toBeInTheDocument();
    })
    it('should render offer item if offers array is not empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{offers: [{applications: [], skills: []}]}}>
                <MyOffers/>
            </RenderWithStore>
        , {wrapper: MemoryRouter})

        const loadingSpinner = queryByTestId('myoffers-spinner');
        const noOffersMsg = queryByTestId('myoffers-nooffers');
        const offerItem = queryByTestId('listitem-linkwrapper');
        const apps = queryByTestId('applications-wrapper');

        expect(loadingSpinner).not.toBeInTheDocument();
        expect(noOffersMsg).not.toBeInTheDocument();
        expect(offerItem).toBeInTheDocument();
        expect(apps).not.toBeInTheDocument()
    })
    it('should render offer item with applications if offers and application arrays are not empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{offers: [{applications: [{name: 'test', email: 'test'}], skills: []}]}}>
                <MyOffers/>
            </RenderWithStore>
            , {wrapper: MemoryRouter})

        const loadingSpinner = queryByTestId('myoffers-spinner');
        const noOffersMsg = queryByTestId('myoffers-nooffers');
        const offerItem = queryByTestId('listitem-linkwrapper');
        const apps = queryByTestId('applications-wrapper');

        expect(loadingSpinner).not.toBeInTheDocument();
        expect(noOffersMsg).not.toBeInTheDocument();
        expect(offerItem).toBeInTheDocument();
        expect(apps).toBeInTheDocument();
    })
})