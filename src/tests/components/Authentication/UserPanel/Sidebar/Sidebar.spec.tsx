import React from 'react';
import {render} from '@testing-library/react';
import {MemoryRouter} from 'react-router-dom';
import Sidebar from "../../../../../components/Authentication/UserPanel/Sidebar/Sidebar";
import {RenderWithStore} from "../../../../helpers/render-component-with-store";

describe('Sidebar', (): void => {
    it('welcome span should contain correct users name provided by redux', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{user: {name: 'Testname'}}}>
                <Sidebar/>
            </RenderWithStore>
        , {wrapper: MemoryRouter})

        const welcomeSpan = queryByTestId('sidebar-welcomespan');

        expect(welcomeSpan).toHaveTextContent('Testname');
    })
    it('should not display my offers link if account type is not emp', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{user: {accType: 'dev'}}}>
                <Sidebar/>
            </RenderWithStore>
            , {wrapper: MemoryRouter})

        const myOffersLink = queryByTestId('sidebar-myoffers');

        expect(myOffersLink).not.toBeInTheDocument();
    })
    it('should display my offers link if account type is emp', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore auth={{user: {accType: 'emp'}}}>
                <Sidebar/>
            </RenderWithStore>
            , {wrapper: MemoryRouter})

        const myOffersLink = queryByTestId('sidebar-myoffers');

        expect(myOffersLink).toBeInTheDocument();
    })
})