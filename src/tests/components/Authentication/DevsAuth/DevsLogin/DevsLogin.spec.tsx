import React from 'react';
import {render, fireEvent, waitFor} from '@testing-library/react';
import DevsLogin from "../../../../../components/Authentication/DevsAuth/DevsLogin/DevsLogin";
import {RenderWithStore} from "../../../../helpers/render-component-with-store";

describe('DevsLogin', (): void => {
    it('button should be disabled if both fields are left empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <DevsLogin />
            </RenderWithStore>
        )

        const emailInput = queryByTestId('devslogin-emailinput');
        const pwInput = queryByTestId('devslogin-pwinput');
        const submitBtn = queryByTestId('devslogin-btn');

        fireEvent.change(emailInput, {target: {value: ''}});
        fireEvent.change(pwInput, {target: {value: ''}});

        expect(submitBtn).toBeDisabled();
    })
    it('button should be disabled if one of the fields is left empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <DevsLogin />
            </RenderWithStore>
        )

        const emailInput = queryByTestId('devslogin-emailinput');
        const pwInput = queryByTestId('devslogin-pwinput');
        const submitBtn = queryByTestId('devslogin-btn');

        fireEvent.change(emailInput, {target: {value: ''}});
        fireEvent.change(pwInput, {target: {value: 'testpw'}});

        expect(submitBtn).toBeDisabled();

        fireEvent.change(emailInput, {target: {value: 'test@test.com'}});
        fireEvent.change(pwInput, {target: {value: ''}});

        expect(submitBtn).toBeDisabled();
    })
    it('button should be disabled if both fields have values but email doesnt match regex', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <DevsLogin />
            </RenderWithStore>
        )

        const emailInput = queryByTestId('devslogin-emailinput');
        const pwInput = queryByTestId('devslogin-pwinput');
        const submitBtn = queryByTestId('devslogin-btn');

        fireEvent.change(emailInput, {target: {value: 'test@test'}});
        fireEvent.change(pwInput, {target: {value: 'testpw'}});

        expect(submitBtn).toBeDisabled();

        fireEvent.change(emailInput, {target: {value: 'test.com'}});
        expect(submitBtn).toBeDisabled();
    })
    it('button should be enabled if both fields have values and email matches regex', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <DevsLogin />
            </RenderWithStore>
        )

        const emailInput = queryByTestId('devslogin-emailinput');
        const pwInput = queryByTestId('devslogin-pwinput');
        const submitBtn = queryByTestId('devslogin-btn');

        fireEvent.change(emailInput, {target: {value: 'test@test.com'}});
        fireEvent.change(pwInput, {target: {value: 'testpw'}});

        expect(submitBtn).not.toBeDisabled();
    })
})