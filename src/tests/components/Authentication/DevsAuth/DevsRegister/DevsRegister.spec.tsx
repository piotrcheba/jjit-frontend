import React from 'react';
import {render, fireEvent, waitFor} from '@testing-library/react';
import DevsRegister from "../../../../../components/Authentication/DevsAuth/DevsRegister/DevsRegister";
import {RenderWithStore} from "../../../../helpers/render-component-with-store";

describe('DevsRegister', (): void => {
    it('submit button should be disabled if any of the inputs is left empty', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <DevsRegister />
            </RenderWithStore>
        )
        const nameInput = queryByTestId('devsregister-nameinput');
        const surnameInput = queryByTestId('devsregister-surnameinput');
        const emailInput = queryByTestId('devsregister-emailinput');
        const pwInput = queryByTestId('devsregister-pwinput');
        const confirmPwInput = queryByTestId('devsregister-pw2input');
        const submitBtn = queryByTestId('devsregister-submitbtn');

        fireEvent.change(nameInput, {target: {value: ''}});
        fireEvent.change(surnameInput, {target: {value: 'test'}});
        fireEvent.change(emailInput, {target: {value: 'test@test.com'}});
        fireEvent.change(pwInput, {target: {value: 'test'}});
        fireEvent.change(confirmPwInput, {target: {value: 'test'}});

        expect(submitBtn).toBeDisabled();

        fireEvent.change(nameInput, {target: {value: 'test'}});
        fireEvent.change(surnameInput, {target: {value: ''}});

        expect(submitBtn).toBeDisabled();

        fireEvent.change(surnameInput, {target: {value: 'test'}});
        fireEvent.change(emailInput, {target: {value: ''}});

        expect(submitBtn).toBeDisabled();

        fireEvent.change(emailInput, {target: {value: 'test'}});
        fireEvent.change(pwInput, {target: {value: ''}});

        expect(submitBtn).toBeDisabled();

        fireEvent.change(pwInput, {target: {value: 'test'}});
        fireEvent.change(confirmPwInput, {target: {value: ''}});

        expect(submitBtn).toBeDisabled();
    });
    it('submit button should be enabled if every input has value and email regex is matched', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <DevsRegister />
            </RenderWithStore>
        )
        const nameInput = queryByTestId('devsregister-nameinput');
        const surnameInput = queryByTestId('devsregister-surnameinput');
        const emailInput = queryByTestId('devsregister-emailinput');
        const pwInput = queryByTestId('devsregister-pwinput');
        const confirmPwInput = queryByTestId('devsregister-pw2input');
        const submitBtn = queryByTestId('devsregister-submitbtn');

        fireEvent.change(nameInput, {target: {value: 'test'}});
        fireEvent.change(surnameInput, {target: {value: 'test'}});
        fireEvent.change(emailInput, {target: {value: 'test@test.com'}});
        fireEvent.change(pwInput, {target: {value: 'test'}});
        fireEvent.change(confirmPwInput, {target: {value: 'test'}});

        expect(submitBtn).not.toBeDisabled();
    })
})