import React from 'react';
import {render} from '@testing-library/react';
import Skill from "../../../components/Skill/Skill";

describe('Skill', (): void => {
    const test = {label: 'TestSkill', level: 5, expectedTitle: 'senior'}
    it('should render given label', (): void => {
        const {queryByTestId} = render(
            <Skill label={test.label} level={test.level} />
        )
        const labelDOM = queryByTestId('skill-label')
        expect(labelDOM).toHaveTextContent(test.label)
    })
    it('should display correct level description', (): void => {
        const {queryByTestId} = render(
            <Skill label={test.label} level={test.level} />
        )
        const levelTitle = queryByTestId('skill-leveltitle');
        expect(levelTitle).toHaveTextContent(test.expectedTitle)
    })
})