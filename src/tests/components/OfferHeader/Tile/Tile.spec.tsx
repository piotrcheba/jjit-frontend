import React from 'react';
import {render} from '@testing-library/react';
import Tile from '../../../../components/OfferHeader/Tile/Tile';

describe('Tile', (): void => {
    it('checks if value and title values are correct based on props', (): void => {
        const test = {type: 'companyName', title: 'Test', value: 'test'}
        const {queryByTestId} = render(
            <Tile type={test.type} title={test.title} value={test.value} />
        )
        const link = queryByTestId('tile-link');
        const value = queryByTestId('tile-value');
        const title = queryByTestId('tile-title');

        expect(link).not.toBeInTheDocument();
        expect(value).toHaveTextContent('test');
        expect(title).toHaveTextContent('Test');
    })
    it('renders link if type equals companyName', (): void => {
        const test = {type: 'companyName', title: 'Test', value: 'test', link: 'test'}

        const {queryByTestId} = render(
            <Tile type={test.type} title={test.title} value={test.value} link={test.link} />
        );

        const link = queryByTestId('tile-link');
        const value = queryByTestId('tile-value');
        const title = queryByTestId('tile-title');

        expect(link).toBeInTheDocument();
        expect(link).toHaveAttribute('href', `http://${test.link}`)
    })
})