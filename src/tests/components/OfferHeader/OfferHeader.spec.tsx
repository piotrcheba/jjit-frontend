import React from 'react';
import {render} from '@testing-library/react';
import OfferHeader from "../../../components/OfferHeader/OfferHeader";
import {RenderWithStore} from "../../helpers/render-component-with-store";

jest.mock('react-router-dom', () => ({
    useHistory: () => ({
        push: jest.fn(),
    }),
}));

describe('OfferHeader', (): void => {
    it('should render back button if given prop headerCtrls is true', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <OfferHeader headerCtrls />
            </RenderWithStore>
        )
        const backBtn = queryByTestId('offerheader-backbtn');
        expect(backBtn).toBeInTheDocument();
    })
    it('should not render back button if prop headerCtrls is not true', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore>
                <OfferHeader />
            </RenderWithStore>
        )
        const backBtn = queryByTestId('offerheader-backbtn');
        expect(backBtn).not.toBeInTheDocument();
    })
    it('should render remote chip if offer is fully remote', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{activeOffer: {fullyRemote: true}}}>
                <OfferHeader />
            </RenderWithStore>
        )
        const remoteChip = queryByTestId('offerheader-remotechip');
        const onlineChip = queryByTestId('offerheader-onlinechip');

        expect(remoteChip).toBeInTheDocument();
        expect(onlineChip).not.toBeInTheDocument();
    })
    it('should render online interview chip if offer isnt fully remote and has online interview option', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{activeOffer: {fullyRemote: false, onlineInterview: true}}}>
                <OfferHeader />
            </RenderWithStore>
        )
        const remoteChip = queryByTestId('offerheader-remotechip');
        const onlineChip = queryByTestId('offerheader-onlinechip');

        expect(remoteChip).not.toBeInTheDocument();
        expect(onlineChip).toBeInTheDocument();
    })
    it('should not render remote nor online interview chip if offer isnt fully remote and has no online interview option', (): void => {
        const {queryByTestId} = render(
            <RenderWithStore offers={{activeOffer: {fullyRemote: false, onlineInterview: false}}}>
                <OfferHeader />
            </RenderWithStore>
        )
        const remoteChip = queryByTestId('offerheader-remotechip');
        const onlineChip = queryByTestId('offerheader-onlinechip');

        expect(remoteChip).not.toBeInTheDocument();
        expect(onlineChip).not.toBeInTheDocument();
    })
})