import React from 'react';
import {render} from '@testing-library/react';
import OfferSection from "../../../components/OfferSection/OfferSection";

describe('OfferSection', (): void => {
    it('should render title and children based on given props', (): void => {
        const {queryByTestId} = render(
            <OfferSection title='Test'>
                <div data-testId='offersection-testdiv' />
            </OfferSection>
        )
        const title = queryByTestId('offersection-title');
        const testDiv = queryByTestId('offersection-testdiv');

        expect(title).toHaveTextContent('Test');
        expect(testDiv).toBeInTheDocument();
    })
})