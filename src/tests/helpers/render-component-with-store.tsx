import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import React, {PropsWithChildren, ReactElement} from 'react';
import authReducer from "../../redux/reducers/authReducer";
import filterReducer from "../../redux/reducers/filterReducer";
import {defaultFilters} from "../../components/Filterbar/Filterbar.constants";

export const RenderWithStore = ({children, auth, filter, offers}: PropsWithChildren<any>): ReactElement => {
    const middlewares = [thunk];
    const mockStore = configureMockStore(middlewares);
    const store = mockStore({
        auth: {
            isAuthenticated: false,
            user: {},
            offers: [],
            loading: false,
            ...auth
        },
        filter: {
            location: defaultFilters.location,
            tech: defaultFilters.tech,
            salary: defaultFilters.salary,
            seniority: defaultFilters.seniority,
            ...filter
        },
        offers: {
            fetchLoading: false,
            loading: false,
            offers: [],
            activeOffer: {},
            error: {},
            ...offers
        }
    });
    return <Provider store={store}>{children}</Provider>;
};