export const levelDisplayer = (skillLevel: number): string => {
    switch (skillLevel) {
        case 1:
            return 'nice to have'
        case 2:
            return 'junior'
        case 3:
            return 'regular'
        case 4:
            return 'advanced'
        case 5:
            return 'senior'
        default:
            return ''
    }
}